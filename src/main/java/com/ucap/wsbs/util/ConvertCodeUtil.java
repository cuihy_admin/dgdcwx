package com.ucap.wsbs.util;

import com.ucap.spyxmp.model.SpyxmpApplyItem;
import com.ucap.util.DateTime;

public class ConvertCodeUtil {

    public static String getOrderCode(int code){
        if(code > 99){
            return "" + code;
        }else if(code > 9){
            return "0" + code;
        }else {
            return "00" + code;
        }
    }

    /**
     * 生成自动注销的申办流水号
     * @param tableNameCode
     * @param typeCode
     * @return
     */
    public static String getZdzxSblsh(String tableNameCode, String typeCode){
        StringBuffer sblshStb = new StringBuffer("Z");
        String datePrefix = new DateTime().getDateTimeNYR();
        sblshStb.append(datePrefix);
        sblshStb.append(ConvertCodeUtil.getOrderCode(Integer.valueOf(tableNameCode)))
                .append(ConvertCodeUtil.getOrderCode(Integer.valueOf(typeCode)));
        int code = SpyxmpApplyItem.me.findLikeSblshData(sblshStb.toString()).size() + 1;
        sblshStb.append(ConvertCodeUtil.getOrderCode(code));
        return sblshStb.toString();
    }
}
