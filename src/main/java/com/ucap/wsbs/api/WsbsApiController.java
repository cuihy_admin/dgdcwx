package com.ucap.wsbs.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.ucap.util.Conts;
import com.ucap.util.DateTime;
import com.ucap.util.StringUtils;
import com.ucap.wsbs.model.WxWsbsFrxx;
import com.ucap.wsbs.model.WxWsbsQyxx;
import com.ucap.wsbs.model.WxWsbsSbxx;
import com.ucap.wsbs.model.YwSgsxzgljKydjxx;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.List;


/**
 * Created by Administrator on 2018/5/10.
 */
public class WsbsApiController extends ApiController {
   // private final static String ZTYT = "[{\"title\":\"食品销售经营者\",\"name\":\"spxsjyz\"},{\"title\":\"商场超市\",\"name\":\"sccs\"},{\"title\":\"便利店 \",\"name\":\"bld\",\"value\":\"bld\"},{\"title\":\"食杂店\",\"name\":\"zsd\"},{\"title\":\"食品批发商(食品贸易商)\",\"name\":\"sppfs\"},{\"title\":\"食品代理销售商(食品贸易商)\",\"name\":\"spdlxss\"},{\"title\":\"食品自动售货销售商  \",\"name\":\"spzdslxss\"},{\"title\":\"网络食品销售商\",\"name\":\"wlspxxs\"},{\"title\":\"药店兼营 \",\"name\":\"ydjy\"},{\"title\":\"食品销售连锁企业总部 \",\"name\":\"spxslsqyzb\"},{\"title\":\"专卖店\",\"name\":\"zmd\"},{\"title\":\"餐饮服务经营者\",\"name\":\"cyfwjyz\"},{\"title\":\"大型餐馆\",\"name\":\"dxcg\"},{\"title\":\"中型餐馆\",\"name\":\"zxcg\"},{\"title\":\"小型餐馆\",\"name\":\"xxcg\"},{\"title\":\"中央厨房\",\"name\":\"zycf\"},{\"title\":\"集体用餐配送单位\",\"name\":\"jtycpsdw\"},{\"title\":\"小餐饮\",\"name\":\"xcy\"},{\"title\":\"饮品店\",\"name\":\"ypd\"},{\"title\":\"糕点店\",\"name\":\"gdd\"},{\"title\":\"餐饮服务连锁企业总部\",\"name\":\"cyfwlsqyzb\"},{\"title\":\"餐饮管理企业\",\"name\":\"cyglqy\"},{\"title\":\"单位食堂\",\"name\":\"dwst\"},{\"title\":\"学校食堂\",\"name\":\"xxst\"},{\"title\":\"托幼机构食堂\",\"name\":\"tejgst\"},{\"title\":\"职工食堂\",\"name\":\"zgst\"},{\"title\":\"养老机构食堂\",\"name\":\"yljgst\"},{\"title\":\"工地食堂\",\"name\":\"gdst\"},{\"title\":\"其他食堂\",\"name\":\"qtst\"}]";
   // private final static String JYXM = "[{\"title\":\"预包装食品销售\",\"name\":\"ybzspxs\"},{\"title\":\"含预包装冷藏冷冻食品\",\"name\":\"hybzlcldsp\"},{\"title\":\"不含预包装冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"散装食品销售\",\"name\":\"szspxs\"},{\"title\":\"含冷藏冷冻食品\",\"name\":\"hlcldsp\"},{\"title\":\"不含冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"含散装熟食\",\"name\":\"hszss\"},{\"title\":\"不含散装熟食\",\"name\":\"bhszss\"},{\"title\":\"特殊食品销售\",\"name\":\"tsspxs\"},{\"title\":\"保健食品(批发)\",\"name\":\"bjsppf\"},{\"title\":\"保健食品(零售)\",\"name\":\"bjspls\"},{\"title\":\"特殊医学用途配方食品\",\"name\":\"tsyxytpfsp\"},{\"title\":\"婴幼儿配方乳粉\",\"name\":\"yyepfrf\"},{\"title\":\"其他婴幼儿配方食品\",\"name\":\"qtyyepfsp\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"其他类食品销售\",\"name\":\"qtlspxx\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"热食类食品制售\",\"name\":\"rslspzs\"},{\"title\":\"冷食类食品制售\",\"name\":\"lslspzs\"},{\"title\":\"含烧卤熟肉\",\"name\":\"hslsr\"},{\"title\":\"不含烧卤熟肉\",\"name\":\"bhslsr\"},{\"title\":\"生食类食品制售\",\"name\":\"shlspzs\"},{\"title\":\"糕点类食品制售\",\"name\":\"gdlspzs\"},{\"title\":\"含裱花蛋糕\",\"name\":\"hbhdg\"},{\"title\":\"不含裱花蛋糕\",\"name\":\"bhbhdg\"},{\"title\":\"自制饮品制售\",\"name\":\"zzypzzs\"},{\"title\":\"含自酿酒\",\"name\":\"hznj\"},{\"title\":\"不含自酿酒\",\"name\":\"bhznj\"},{\"title\":\"其他类食品制售\",\"name\":\"qtlspzs\"},{\"title\":\"具体品种\",\"name\":\"qtlspzs_jtpz\"},{\"title\":\"食品经营管理\",\"name\":\"spjygl\"}]";
    private final static String ZTYT = "[{\"title\":\"食品销售经营者\",\"name\":\"spxsjyz\"},{\"title\":\"商场超市\",\"name\":\"sccs\"},{\"title\":\"便利店 \",\"name\":\"bld\"},{\"title\":\"食杂店\",\"name\":\"zsd\"},{\"title\":\"食品贸易商\",\"name\":\"spmys\"},{\"title\":\"食品自动售货销售商\",\"name\":\"spzdshxss\"},{\"title\":\"网络食品销售商\",\"name\":\"wlspxss\"},{\"title\":\"药店兼营 \",\"name\":\"ydjy\"},{\"title\":\"食品销售连锁企业总部 \",\"name\":\"spxslsqyzb\"},{\"title\":\"专卖店\",\"name\":\"zmd\"},{\"title\":\"餐饮服务经营者\",\"name\":\"cyfwjyz\"},{\"title\":\"大型餐馆\",\"name\":\"dxcg\"},{\"title\":\"中型餐馆\",\"name\":\"zxcg\"},{\"title\":\"小型餐馆\",\"name\":\"xxcg\"},{\"title\":\"中央厨房\",\"name\":\"zycf\"},{\"title\":\"集体用餐配送单位\",\"name\":\"jtycpsdw\"},{\"title\":\"小餐饮\",\"name\":\"xcy\"},{\"title\":\"饮品店\",\"name\":\"ypd\"},{\"title\":\"糕点店\",\"name\":\"gdd\"},{\"title\":\"餐饮服务连锁企业总部\",\"name\":\"cyfwlsqyzb\"},{\"title\":\"餐饮管理企业\",\"name\":\"cyglqy\"},{\"title\":\"单位食堂\",\"name\":\"dwst\"},{\"title\":\"学校食堂\",\"name\":\"xxst\"},{\"title\":\"托幼机构食堂\",\"name\":\"tejgst\"},{\"title\":\"职工食堂\",\"name\":\"zgst\"},{\"title\":\"养老机构食堂\",\"name\":\"yljgst\"},{\"title\":\"工地食堂\",\"name\":\"gdst\"},{\"title\":\"其他食堂\",\"name\":\"qtst\"}]";
    private final static String JYXM = "[{\"title\":\"预包装食品销售\",\"name\":\"ybzspxs\"},{\"title\":\"含预包装冷藏冷冻食品\",\"name\":\"hybzlcldsp\"},{\"title\":\"不含预包装冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"散装食品销售\",\"name\":\"szspxs\"},{\"title\":\"含冷藏冷冻食品\",\"name\":\"hlcldsp\"},{\"title\":\"不含冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"含散装熟食\",\"name\":\"hszss\"},{\"title\":\"不含散装熟食\",\"name\":\"bhszss\"},{\"title\":\"含散装酒\",\"name\":\"hszj\"},{\"title\":\"不含散装酒\",\"name\":\"bhszj\"},{\"title\":\"特殊食品销售\",\"name\":\"tsspxs\"},{\"title\":\"保健食品\",\"name\":\"bjsp\"},{\"title\":\"特殊医学用途配方食品\",\"name\":\"tsyxytpfsp\"},{\"title\":\"婴幼儿配方乳粉\",\"name\":\"yyepfrf\"},{\"title\":\"其他婴幼儿配方食品\",\"name\":\"qtyyepfsp\"},{\"title\":\"热食类食品制售\",\"name\":\"rslspzs\"},{\"title\":\"冷食类食品制售\",\"name\":\"lslspzs\"},{\"title\":\"含烧卤熟肉\",\"name\":\"hslsr\"},{\"title\":\"不含烧卤熟肉\",\"name\":\"bhslsr\"},{\"title\":\"生食类食品制售\",\"name\":\"shlspzs\"},{\"title\":\"糕点类食品制售\",\"name\":\"gdlspzs\"},{\"title\":\"含裱花类糕点\",\"name\":\"hbhlgd\"},{\"title\":\"不含裱花类糕点\",\"name\":\"bhbhlgd\"},{\"title\":\"自制饮品制售\",\"name\":\"zzypzzs\"},{\"title\":\"含自酿酒\",\"name\":\"hznj\"},{\"title\":\"不含自酿酒\",\"name\":\"bhznj\"},{\"title\":\"食品经营管理\",\"name\":\"spjygl\"}]";

    public void getSblsh() {

        JSONObject msg = new JSONObject();
        boolean boo = jrpzVerify();
        if (!boo) {
            msg.put("code", "10010");
            msg.put("msg", "接口凭证校验失败！");
        } else {
            JSONObject data = new JSONObject();
            WxWsbsSbxx sbxx = new WxWsbsSbxx();
            sbxx.set("id", StringUtils.getUUID());
            List<WxWsbsSbxx> sbxxList = WxWsbsSbxx.me.findLikeData();
            NumberFormat f = new DecimalFormat("00000");
            if (sbxxList == null || sbxxList.size() == 0) {
                sbxx.set("sblsh", new DateTime().getDateTimeNYR() + f.format(1));
            } else {
                sbxx.set("sblsh", new DateTime().getDateTimeNYR() + f.format(sbxxList.size() + 1));
            }


            sbxx.set("userid", data.getString("appid"));
            sbxx.set("addtime", new DateTime().getTimestamp());
            sbxx.set("adduser", data.getString("appid"));
            sbxx.set("updatetime", new DateTime().getTimestamp());
            sbxx.set("updateuser", data.getString("appid"));
            sbxx.set("datastate", "I");
            sbxx.set("sbxx", "0");
            try {
                sbxx.save();
                data.put("sblsh", sbxx.get("sblsh"));
                msg.put("code", "10000");
                msg.put("msg", "获取申报流水号成功");
                msg.put("data", data);
            } catch (Exception e) {
                msg.put("code", "10001");
                msg.put("msg", "获取申报流水号失败");
                e.printStackTrace();
            }


        }


        renderJson(msg);
    }

    public void saveQyxx() {
        JSONObject msg = new JSONObject();

        try {
            JSONObject data = JSONObject.parseObject(HttpKit.readData(getRequest()).toString());
            System.out.println("参数：" + data);

            boolean jyboo = jrpzVerify(data);
            if (!jyboo) {
                msg.put("code", "10010");
                msg.put("msg", "接口凭证校验失败！");
            } else {

                WxWsbsQyxx qyxx = null;
                if (!StringUtils.isEmpty(data.getString("sblsh") + "")) {
                    qyxx = WxWsbsQyxx.me.findBySblsh(data.getString("sblsh"));
                }
                if (qyxx == null) {
                    qyxx = new WxWsbsQyxx();
//2018090200003
                }
                qyxx.set("qyxz", data.getString("qyxz"));
                // qyxx.set("qyxzt", data.getString("qyxzt"));
                qyxx.set("jyzmc", data.getString("jyzmc"));
                qyxx.set("shtyxym", data.getString("shtyxym"));
                //添加联系人信息
                qyxx.set("lxr", data.getString("lxr"));
                qyxx.set("lxrdh", data.getString("lxrdh"));
                qyxx.set("jycsdzlx", data.getString("jycsdzlx"));

                if (data.getString("ckdzlx").equals("2")) {
                    qyxx.set("ckdz",data.getString("jycsdz"));
                } else {
                    qyxx.set("ckdz", data.getString("ckdz"));
                }
                qyxx.set("ckdzlx", data.getString("ckdzlx"));
                qyxx.set("jycsmj", data.getString("jycsmj"));
                qyxx.set("jycsdz", data.getString("jycsdz"));
                qyxx.set("sblsh", data.getString("sblsh"));
                boolean boo = false;
                if (qyxx.get("id") == null) {
                    qyxx.set("id", StringUtils.getUUID());
                    qyxx.set("addtime", new DateTime().getTimestamp());
                    qyxx.set("adduser", data.getString("sblsh"));
                    qyxx.set("updatetime", new DateTime().getTimestamp());
                    qyxx.set("updateuser", data.getString("sblsh"));
                    qyxx.set("datastate", "I");
                    boo = qyxx.save();
                } else {

                    qyxx.set("updatetime", new DateTime().getTimestamp());
//                qyxx.set("updateuser", data.getString("sblsh"));
                    qyxx.set("datastate", "U");
                    boo = qyxx.update();

                }


                if (boo) {
                    msg.put("code", "10000");
                    msg.put("msg", "保存企业信息成功");
                    WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(data.getString("sblsh"));
                    sb.set("QYXX", "1");
                    sb.update();
                } else {
                    msg.put("code", "10011");
                    msg.put("msg", "保存企业信息失败");
                }

            }
        } catch (Exception e) {
            msg.put("code", "10011");
            msg.put("msg", "保存企业信息失败");
            e.printStackTrace();
        }
        renderJson(msg);
    }


    public void saveZzxx() {
        JSONObject msg = new JSONObject();
        JSONObject data = JSONObject.parseObject(HttpKit.readData(getRequest()).toString());
        boolean jyboo = jrpzVerify(data);
        if (!jyboo) {
            msg.put("code", "10010");
            msg.put("msg", "接口凭证校验失败！");
        } else {
            WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(data.getString("sblsh"));
            sb.set("lyb", data.getString("qksm"));
            sb.set("ZZXX", "1");
            try {
                boolean boo = sb.update();

                if (boo) {
                    msg.put("code", "10000");
                    msg.put("msg", "保存材料信息成功");
                } else {
                    msg.put("code", "10012");
                    msg.put("msg", "保存材料信息失败");
                }
            } catch (Exception e) {
                msg.put("code", "10012");
                msg.put("msg", "保存材料信息失败");
                e.printStackTrace();
            }
        }
        renderJson(msg);
    }

    public void saveFrxx() {
        JSONObject msg = new JSONObject();



            try {
                JSONObject data = JSONObject.parseObject(HttpKit.readData(getRequest()).toString());
                System.out.println();
                boolean jyboo = jrpzVerify(data);
                if (!jyboo) {
                    msg.put("code", "10010");
                    msg.put("msg", "接口凭证校验失败！");
                } else {

                    WxWsbsFrxx frxx = null;
                    if (!StringUtils.isEmpty(data.getString("sblsh"))) {
                        frxx = WxWsbsFrxx.me.findBySblsh(data.getString("sblsh"));
                    }
                    if (frxx == null) {
                        frxx = new WxWsbsFrxx();
                    }
                    frxx.set("userid", data.getString("sblsh"));
                    frxx.set("name", data.getString("name"));
                    frxx.set("sex", data.getString("sex"));
                    frxx.set("nation", data.getString("nation"));
                    frxx.set("post", data.getString("post"));
                    // frxx.set("place", data.getString("place"));
                    frxx.set("detailedPlace", data.getString("detailedPlace"));
                    frxx.set("live", data.getString("live"));
                    frxx.set("detailedLive", data.getString("detailedLive"));
                    frxx.set("idType", data.getString("idType"));
                    frxx.set("tel", data.getString("tel"));
                    frxx.set("zjh", data.getString("idCard"));
                    frxx.set("phone", data.getString("phone"));
                    frxx.set("sblsh", data.getString("sblsh"));
                boolean boo = false;
                if (frxx.get("id") == null) {
                    frxx.set("id", StringUtils.getUUID());
                    frxx.set("addtime", new DateTime().getTimestamp());
                    frxx.set("adduser", data.getString("sblsh"));
                    frxx.set("updatetime", new DateTime().getTimestamp());
                    frxx.set("updateuser", data.getString("sblsh"));
                    frxx.set("datastate", "I");
                    boo = frxx.save();
                } else {
                    frxx.set("updatetime", new DateTime().getTimestamp());
                    frxx.set("updateuser", data.getString("sblsh"));
                    frxx.set("datastate", "U");
                    boo = frxx.update();

                }

                if (boo) {
                    msg.put("code", "10000");
                    msg.put("msg", "保存法人信息成功");
                    WxWsbsQyxx qyxx = WxWsbsQyxx.me.findBySblsh(data.getString("sblsh"));
                    System.out.println(qyxx.toString());
                    if (qyxx.getStr("jycsdzlx").equals("1")) {
                        qyxx.set("jycsdz", frxx.getStr("live") + frxx.getStr("detailedLive"));
                        if (qyxx.getStr("ckdzlx").equals("2")) {
                            qyxx.set("ckdz", qyxx.getStr("jycsdz"));
                        }
                        qyxx.update();
                    }
                    WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(data.getString("sblsh"));
                    sb.set("frxx", "1");
                    sb.update();
                    sb.update();
                } else {
                    msg.put("code", "10013");
                    msg.put("msg", "保存法人信息失败");
                }  }
            } catch (Exception e) {
                msg.put("code", "10013");
                msg.put("msg", "保存法人信息失败");
                e.printStackTrace();


        }
        renderJson(msg);
    }

    public void saveSbxx() {

        JSONObject msg = new JSONObject();

        try {
            JSONObject data = JSONObject.parseObject(HttpKit.readData(getRequest()).toString());


            WxWsbsSbxx sbxx = null;


            if (!StringUtils.isEmpty(data.getString("sblsh"))) {
                sbxx = WxWsbsSbxx.me.findBySblsh(data.getString("sblsh"));
            } else {
                sbxx = new WxWsbsSbxx();
                sbxx.set("id", StringUtils.getUUID());

            }

            sbxx.set("sbxx", "1");
            sbxx.set("ztytlx", data.getString("ztytlx"));
            JSONArray ytlx = foreachJson(JSONArray.parseArray(ZTYT),sbxx.getStr("ztytlx"),data);
            System.out.println("ytlx=" + ytlx.toJSONString());

            sbxx.set("ytlx", ytlx.toString());
            sbxx.set("jyxmlx", data.getString("jyxmlx"));
            JSONArray jyxm = foreachJson(JSONArray.parseArray(JYXM),sbxx.getStr("jyxmlx"),data);
            System.out.println("jyxm=" + jyxm.toJSONString());
            sbxx.set("jyxm", jyxm.toString());
            sbxx.set("wljy", data.getString("wljy"));
            System.out.println("wljy=" + data.getString("sfwljy"));
            System.out.println("stmd=" + data.getString("stmd"));
            sbxx.set("stmd",data.getString("stmd"));
            sbxx.set("wzlx", data.getString("wzlx"));
            sbxx.set("wzdz", data.getString("wzdz"));
            sbxx.set("isfrxx", data.getString("isfrxx"));
            sbxx.set("sp_name", data.getString("sp_name"));
            sbxx.set("sp_sex", data.getString("sp_sex"));
            sbxx.set("sp_idtype", data.getString("sp_idtype"));
            sbxx.set("sp_idcard", data.getString("sp_idcard"));
            sbxx.set("sp_post", data.getString("sp_post"));
            sbxx.set("sp_tel", data.getString("sp_tel"));
            sbxx.set("bz", data.getString("bz"));
            sbxx.set("sfrlsb","2");
            sbxx.set("sblx","1");
/*
*  SBXX.SET("ISFRXX",data.getString("ISFRXX"));
            SBXX.SET("SP_NAME",data.getString("SP_NAME"));
            SBXX.SET("SP_SEX",data.getString("SP_SEX"));
            SBXX.SET("SP_IDTYPE",data.getString("SP_IDTYPE"));
            SBXX.SET("SP_IDCARD",data.getString("SP_IDCARD"));
            SBXX.SET("SP_POST",data.getString("SP_POST"));
            SBXX.SET("SP_TEL",data.getString("SP_TEL"));
* */
            sbxx.set("updatetime", new DateTime().getTimestamp());
            sbxx.set("updateuser", data.getString("appid"));
            sbxx.set("datastate", "U");
            sbxx.set("sbxx", "1");
            System.out.println(sbxx);
            boolean boo = sbxx.update();

            if (boo) {
                msg.put("code", "10000");
                msg.put("msg", "保存申报信息成功");
            } else {
                msg.put("code", "10016");
                msg.put("msg", "保存申报信息失败");
            }
        } catch (Exception e) {
            msg.put("code", "10016");
            msg.put("msg", "保存申报信息失败");
            e.printStackTrace();
        }
        renderJson(msg);
    }




    public void findFrxx() {
        WxWsbsFrxx frxx = WxWsbsFrxx.me.findByUser(Conts.getSessionUser(getSession()).getStr("id"));
        renderJson(frxx);
    }
    private JSONArray foreachJson(JSONArray items, String lx,JSONObject data) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < items.size(); i++) {
            JSONObject json = JSONObject.parseObject(items.get(i).toString());
            json.put("value", "0");
            for (int j = 0; j < items.size(); j++) {
                JSONObject itemjson = JSONObject.parseObject(items.get(j).toString());
               String val=data.getString(itemjson.getString("name"));
               if(!StringUtils.isEmpty(val)){
                   String[] vals=val.split(",");
                   for  (int k = 0; k < vals.length; k++) {
                       if (json.getString("title").equals(vals[k])) {
                           System.out.println(i + "条数据" + json.getString("title") + "=" + data.getString(itemjson.getString("name")));
                           json.put("value", "1");
                       }
                   }
               }

            }
            jsonArray.add(json);
        }
        JSONArray jsonValue = new JSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject json = JSONObject.parseObject(jsonArray.get(i).toString());
            if (lx.contains(json.getString("title"))) {
                json.put("value", "1");
                System.out.println(json.toString());
            }
            jsonValue.add(json);
        }

        return jsonValue;
    }
    private void checkPara(String[] values,JSONArray items,JSONArray jsonValue) {
        for (int i=0;i<values.length;i++){
            for (int j=0;j<items.size();j++){
                JSONObject json = JSONObject.parseObject(items.get(j).toString());
                System.out.println(values[0]+"="+json.getString("title")+"="+values[0].equals(json.getString("title")));
                if(!StringUtils.isEmpty(values[0])&&values[0].equals(json.getString("title"))){
                    json.put("value","1");
                    jsonValue.add(json);
                }
            }
        }
    }


    public void queryQyxx() {
        JSONObject msg = new JSONObject();
        boolean jyboo = jrpzVerify();
        if (!jyboo) {
            msg.put("code", "10010");
            msg.put("msg", "接口凭证校验失败！");
        } else {
            List<YwSgsxzgljKydjxx> qyxx = null;
            if (!StringUtils.isEmpty(getPara("frsfzh"))) {
                qyxx = YwSgsxzgljKydjxx.me.findByShtyxym(getPara("frsfzh"));
            } else {
                msg.put("code", "10018");
                msg.put("msg", "法人代表证件号码不能为空！");
            }

            if (qyxx != null) {
                msg.put("code", "10000");
                msg.put("msg", "查询企业信息成功！");
                msg.put("data", qyxx);
            } else {
                msg.put("code", "10017");
                msg.put("msg", "查询企业信息失败！");
            }

        }
        renderJson(msg);
    }
}
