package com.ucap.wsbs.api;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;

/**
 * Created by Administrator on 2018/9/2.
 */
public abstract class ApiController extends Controller {
    public boolean jrpzVerify(JSONObject data) {
        if (getPara("appid") != null && !"".equals(getPara("appid")) && getPara("secret") != null && !"".equals(getPara("secret")))
            return JrpzVerify.verify(getPara("appid"), getPara("secret"));
        else if (data.getString("appid") != null && !"".equals(data.getString("appid")) && data.getString("secret") != null && !"".equals(data.getString("secret")))
            return JrpzVerify.verify(data.getString("appid"), data.getString("secret"));
        else
            return false;
    }
    public boolean jrpzVerify() {
        getPara("appid");
        getPara("secret");
        if (getPara("appid") != null && !"".equals(getPara("appid")) && getPara("secret") != null && !"".equals(getPara("secret")))
            return JrpzVerify.verify(getPara("appid"), getPara("secret"));
        else
            return false;
    }
}
