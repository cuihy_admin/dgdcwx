package com.ucap.wsbs.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.CustomServiceApi;
import com.ucap.util.Conts;
import com.ucap.util.DateTime;
import com.ucap.util.StringFormat;
import com.ucap.util.StringUtils;
import com.ucap.weixin.model.WxWsbsUser;
import com.ucap.wsbs.api.ApiController;
import com.ucap.wsbs.model.WxWsbsFile;
import com.ucap.wsbs.model.WxWsbsFrxx;
import com.ucap.wsbs.model.WxWsbsQyxx;
import com.ucap.wsbs.model.WxWsbsSbxx;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Administrator on 2018/5/10.
 */
public class WsbsControllerbak extends ApiController {
    private final static String ZTYT = "[{\"title\":\"食品销售经营者\",\"name\":\"spxsjyz\"},{\"title\":\"商场超市\",\"name\":\"sccs\"},{\"title\":\"便利店 \",\"name\":\"bld\",\"value\":\"bld\"},{\"title\":\"食杂店\",\"name\":\"zsd\"},{\"title\":\"食品批发商(食品贸易商)\",\"name\":\"sppfs\"},{\"title\":\"食品代理销售商(食品贸易商)\",\"name\":\"spdlxss\"},{\"title\":\"食品自动售货销售商  \",\"name\":\"spzdslxss\"},{\"title\":\"网络食品销售商\",\"name\":\"wlspxxs\"},{\"title\":\"药店兼营 \",\"name\":\"ydjy\"},{\"title\":\"食品销售连锁企业总部 \",\"name\":\"spxslsqyzb\"},{\"title\":\"专卖店\",\"name\":\"zmd\"},{\"title\":\"餐饮服务经营者\",\"name\":\"cyfwjyz\"},{\"title\":\"大型餐馆\",\"name\":\"dxcg\"},{\"title\":\"中型餐馆\",\"name\":\"zxcg\"},{\"title\":\"小型餐馆\",\"name\":\"xxcg\"},{\"title\":\"中央厨房\",\"name\":\"zycf\"},{\"title\":\"集体用餐配送单位\",\"name\":\"jtycpsdw\"},{\"title\":\"小餐饮\",\"name\":\"xcy\"},{\"title\":\"饮品店\",\"name\":\"ypd\"},{\"title\":\"糕点店\",\"name\":\"gdd\"},{\"title\":\"餐饮服务连锁企业总部\",\"name\":\"cyfwlsqyzb\"},{\"title\":\"餐饮管理企业\",\"name\":\"cyglqy\"},{\"title\":\"单位食堂\",\"name\":\"dwst\"},{\"title\":\"学校食堂\",\"name\":\"xxst\"},{\"title\":\"托幼机构食堂\",\"name\":\"tejgst\"},{\"title\":\"职工食堂\",\"name\":\"zgst\"},{\"title\":\"养老机构食堂\",\"name\":\"yljgst\"},{\"title\":\"工地食堂\",\"name\":\"gdst\"},{\"title\":\"其他食堂\",\"name\":\"qtst\"}]";
    private final static String JYXM = "[{\"title\":\"预包装食品销售\",\"name\":\"ybzspxs\"},{\"title\":\"含预包装冷藏冷冻食品\",\"name\":\"hybzlcldsp\"},{\"title\":\"不含预包装冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"散装食品销售\",\"name\":\"szspxs\"},{\"title\":\"含冷藏冷冻食品\",\"name\":\"hlcldsp\"},{\"title\":\"不含冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"含散装熟食\",\"name\":\"hszss\"},{\"title\":\"不含散装熟食\",\"name\":\"bhszss\"},{\"title\":\"特殊食品销售\",\"name\":\"tsspxs\"},{\"title\":\"保健食品(批发)\",\"name\":\"bjsppf\"},{\"title\":\"保健食品(零售)\",\"name\":\"bjspls\"},{\"title\":\"特殊医学用途配方食品\",\"name\":\"tsyxytpfsp\"},{\"title\":\"婴幼儿配方乳粉\",\"name\":\"yyepfrf\"},{\"title\":\"其他婴幼儿配方食品\",\"name\":\"qtyyepfsp\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"其他类食品销售\",\"name\":\"qtlspxx\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"热食类食品制售\",\"name\":\"rslspzs\"},{\"title\":\"冷食类食品制售\",\"name\":\"lslspzs\"},{\"title\":\"含烧卤熟肉\",\"name\":\"hslsr\"},{\"title\":\"不含烧卤熟肉\",\"name\":\"bhslsr\"},{\"title\":\"生食类食品制售\",\"name\":\"shlspzs\"},{\"title\":\"糕点类食品制售\",\"name\":\"gdlspzs\"},{\"title\":\"含裱花蛋糕\",\"name\":\"hbhdg\"},{\"title\":\"不含裱花蛋糕\",\"name\":\"bhbhdg\"},{\"title\":\"自制饮品制售\",\"name\":\"zzypzzs\"},{\"title\":\"含自酿酒\",\"name\":\"hznj\"},{\"title\":\"不含自酿酒\",\"name\":\"bhznj\"},{\"title\":\"其他类食品制售\",\"name\":\"qtlspzs\"},{\"title\":\"具体品种\",\"name\":\"qtlspzs_jtpz\"},{\"title\":\"食品经营管理\",\"name\":\"spjygl\"}]";

    public void saveQyxx() {
        JSONObject msg = new JSONObject();
        String step = getPara("step");
        String id = getPara("id");
        WxWsbsQyxx qyxx = null;
        if (!StringUtils.isEmpty(id)) {
            qyxx = WxWsbsQyxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
        } else {
            qyxx = new WxWsbsQyxx();
            qyxx.set("id", StringUtils.getUUID());

        }
        qyxx.set("qyxz", getPara("qyxz"));
        qyxx.set("qyxzt", getPara("qyxzT"));
        qyxx.set("jyzmc", getPara("jyzmc"));
        qyxx.set("shtyxym", getPara("shtyxym"));
        qyxx.set("jycsdz", getPara("jycsdz"));
        qyxx.set("sblsh", Conts.getWsbsSblsh(getSession()));
        //添加联系人信息
        qyxx.set("lxr", getPara("lxr"));
        qyxx.set("lxrdh", getPara("lxrdh"));
        qyxx.set("jycsdzlx", getPara("jycsdzlx"));
        qyxx.set("ckdzlx", getPara("ckdzlx"));

        try {
            boolean boo = false;
            if (StringUtils.isEmpty(id)) {
                qyxx.set("addtime", new DateTime().getTimestamp());
                qyxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("updatetime", new DateTime().getTimestamp());
                qyxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("datastate", "I");
                boo = qyxx.save();
            } else {

                qyxx.set("updatetime", new DateTime().getTimestamp());
//                qyxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("datastate", "U");
                boo = qyxx.update();

            }

            msg.put("success", boo);
            if (boo) {
                msg.put("data", qyxx);
                msg.put("msg", "保存企业信息成功");
                WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
                sb.set("qyxx", "1");
                sb.update();
            } else {
                msg.put("msg", "保存企业信息失败");
            }
        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存企业信息失败");
            e.printStackTrace();
        }

        renderJson(msg);
    }


    public void saveZzxx() {
        JSONObject msg = new JSONObject();
        String step = getPara("step");
        WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
        sb.set("lyb", getPara("lyb"));
        sb.set("zzxx", "1");
        try {
            boolean boo = sb.update();
            msg.put("success", boo);
            if (boo) {
                msg.put("msg", "保存材料信息成功");
            } else {

                msg.put("msg", "保存材料信息失败");
            }
        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存材料信息失败");
            e.printStackTrace();
        }

        renderJson(msg);
    }

    public void saveFrxx() {
        JSONObject msg = new JSONObject();
        String id = getPara("id");
        WxWsbsFrxx frxx = null;
        if (!StringUtils.isEmpty(id)) {
            frxx = WxWsbsFrxx.me.findByUser(Conts.getSessionUser(getSession()).getStr("id"));
            ;
        } else {
            frxx = new WxWsbsFrxx();
            frxx.set("id", StringUtils.getUUID());

        }
        frxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
        frxx.set("name", getPara("name"));
        frxx.set("sex", getPara("sex"));
        frxx.set("nation", getPara("nation"));
        frxx.set("post", getPara("post"));
        // frxx.set("place", getPara("place"));
        frxx.set("detailedPlace", getPara("detailedPlace"));
        frxx.set("live", getPara("live"));
        frxx.set("detailedLive", getPara("detailedLive"));
        frxx.set("idType", getPara("idType"));
        frxx.set("tel", getPara("tel"));
        frxx.set("zjh", getPara("zjh"));
        frxx.set("phone", getPara("phone"));
        frxx.set("sblsh", Conts.getWsbsSblsh(getSession()));


        try {
            boolean boo = false;
            if (StringUtils.isEmpty(id)) {
                frxx.set("addtime", new DateTime().getTimestamp());
                frxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("updatetime", new DateTime().getTimestamp());
                frxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("datastate", "I");
                boo = frxx.save();
            } else {
                frxx.set("updatetime", new DateTime().getTimestamp());
                frxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("datastate", "U");
                boo = frxx.update();

            }

            msg.put("success", boo);
            if (boo) {
                msg.put("data", frxx);
                msg.put("msg", "保存法人信息成功");
                WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
                sb.set("FRXX", "1");
                sb.update();
            } else {
                msg.put("msg", "保存法人信息失败");
            }
        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存法人信息失败");
            e.printStackTrace();
        }


        renderJson(msg);
    }

    public void saveSbxx() {
        JSONObject msg = new JSONObject();
        String sblx = getPara("sblx");
        String sblsh = getPara("sblsh");
        if (sblx == null) {
            sblx = "1";
        }
        String id = getPara("id");
        WxWsbsSbxx sbxx = null;
        boolean boo=false;
        if (!StringUtils.isEmpty(sblsh)) {
            sbxx = WxWsbsSbxx.me.findBySblsh(sblsh);
        } else {
            sbxx = new WxWsbsSbxx();
            sbxx.set("id", StringUtils.getUUID());
        }
        try {
            switch (sblx) {
                case "1":
                    sbxx.set("ztytlx", getPara("ztytlx"));
                    JSONArray ytlx = foreachJson(JSONArray.parseArray(ZTYT));
                    sbxx.set("ytlx", ytlx.toString());
                    sbxx.set("jyxmlx", getPara("jyxmlx"));
                    JSONArray jyxm = foreachJson(JSONArray.parseArray(JYXM));
                    sbxx.set("jyxm", jyxm.toString());
                    sbxx.set("wljy", getPara("sfwljy"));
                    sbxx.set("stmd", getPara("stmd"));
                    sbxx.set("wzlx", getPara("wzlx"));
                    sbxx.set("wzdz", getPara("wzdz"));
                    sbxx.set("isfrxx", getPara("isfrxx"));
                    sbxx.set("sp_name", getPara("sp_name"));
                    sbxx.set("sp_sex", getPara("sp_sex"));
                    sbxx.set("sp_idtype", getPara("sp_idtype"));
                    sbxx.set("sp_idcard", getPara("sp_idcard"));
                    sbxx.set("sp_post", getPara("sp_post"));
                    sbxx.set("sp_tel", getPara("sp_tel"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "U");
                    sbxx.set("sbxx", "1");
                     boo = sbxx.update();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }

                    break;
                case "2":
                    sbxx.set("id", StringUtils.getUUID());
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "U");
                    sbxx.set("sbxx", "1");
                    boo = sbxx.save();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("msg", "保存申报信息成功");
                        msg.put("data",sbxx);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "3":
                    sbxx.set("id", StringUtils.getUUID());
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "U");
                    sbxx.set("sbxx", "1");
                    boo = sbxx.save();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data",sbxx);
                        msg.put("msg", "保存申报信息成功");
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "4":
                    sbxx.set("id", StringUtils.getUUID());

                    sbxx.set("sbsm", getPara("sbsm"));
                    sbxx.set("sblx", sblx);
                    sbxx.set("ysblsh", sbxx.getStr("sblsh"));
                    sbxx.set("sblsh", createSblsh());
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "I");
                    sbxx.set("sbxx", "0");
                     boo = sbxx.save();
                    System.out.println(sbxx.toString());
                    msg.put("data", sbxx);

                    if (boo) {
                        msg.put("code", "10000");
                        msg.put("data",sbxx);
                        msg.put("msg", "保存申报信息成功");
                    } else {
                        msg.put("code", "10020");
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "5":

                    sbxx.set("id", StringUtils.getUUID());
                    sbxx.set("sbsm", getPara("sbsm"));
                    sbxx.set("sblx", getPara("sblx"));
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "U");
                    sbxx.set("sbxx", "1");
                    boo = sbxx.save();
                    msg.put("success", boo);
                    msg.put("data", boo);
                    if (boo) {
                        msg.put("msg", "保存申报信息成功");
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存申报信息失败");
            e.printStackTrace();
        }
        renderJson(msg);
    }

    private Object createSblsh() {
        List<WxWsbsSbxx> sbxxList = WxWsbsSbxx.me.findLikeData();
        NumberFormat f = new DecimalFormat("00000");
        String sblsh=null;
        if (sbxxList == null || sbxxList.size() == 0) {
            sblsh= new DateTime().getDateTimeNYR() + f.format(1);
        } else {
            sblsh= new DateTime().getDateTimeNYR() + f.format(sbxxList.size() + 1);
        }
        return sblsh;
    }


    public void findFrxx() {
        WxWsbsFrxx frxx = WxWsbsFrxx.me.findByUser(Conts.getSessionUser(getSession()).getStr("id"));
        renderJson(frxx);
    }

    private JSONArray foreachJson(JSONArray items) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < items.size(); i++) {
            JSONObject json = JSONObject.parseObject(items.get(i).toString());
            json.put("value", getPara(json.get("name").toString()));
            jsonArray.add(json);
        }
        return jsonArray;
    }

    public void toBszn() {
        String values = getPara("values");
        System.out.println(values);
        setAttr("values", values);
        render("/WEB-INF/wsbs/bszn/index.html");
    }
}
