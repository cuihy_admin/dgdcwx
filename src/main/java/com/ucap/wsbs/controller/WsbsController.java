package com.ucap.wsbs.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ucap.util.Conts;
import com.ucap.util.DateTime;
import com.ucap.util.StringUtils;
import com.ucap.wsbs.api.ApiController;
import com.ucap.wsbs.model.WxWsbsFile;
import com.ucap.wsbs.model.WxWsbsFrxx;
import com.ucap.wsbs.model.WxWsbsQyxx;
import com.ucap.wsbs.model.WxWsbsSbxx;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;


/**
 * Created by Administrator on 2018/5/10.
 */
public class WsbsController extends ApiController {
    //private final static String ZTYT = "[{\"title\":\"食品销售经营者\",\"name\":\"spxsjyz\"},{\"title\":\"商场超市\",\"name\":\"sccs\"},{\"title\":\"便利店 \",\"name\":\"bld\",\"value\":\"bld\"},{\"title\":\"食杂店\",\"name\":\"zsd\"},{\"title\":\"食品批发商(食品贸易商)\",\"name\":\"sppfs\"},{\"title\":\"食品代理销售商(食品贸易商)\",\"name\":\"spdlxss\"},{\"title\":\"食品自动售货销售商  \",\"name\":\"spzdslxss\"},{\"title\":\"网络食品销售商\",\"name\":\"wlspxxs\"},{\"title\":\"药店兼营 \",\"name\":\"ydjy\"},{\"title\":\"食品销售连锁企业总部 \",\"name\":\"spxslsqyzb\"},{\"title\":\"专卖店\",\"name\":\"zmd\"},{\"title\":\"餐饮服务经营者\",\"name\":\"cyfwjyz\"},{\"title\":\"大型餐馆\",\"name\":\"dxcg\"},{\"title\":\"中型餐馆\",\"name\":\"zxcg\"},{\"title\":\"小型餐馆\",\"name\":\"xxcg\"},{\"title\":\"中央厨房\",\"name\":\"zycf\"},{\"title\":\"集体用餐配送单位\",\"name\":\"jtycpsdw\"},{\"title\":\"小餐饮\",\"name\":\"xcy\"},{\"title\":\"饮品店\",\"name\":\"ypd\"},{\"title\":\"糕点店\",\"name\":\"gdd\"},{\"title\":\"餐饮服务连锁企业总部\",\"name\":\"cyfwlsqyzb\"},{\"title\":\"餐饮管理企业\",\"name\":\"cyglqy\"},{\"title\":\"单位食堂\",\"name\":\"dwst\"},{\"title\":\"学校食堂\",\"name\":\"xxst\"},{\"title\":\"托幼机构食堂\",\"name\":\"tejgst\"},{\"title\":\"职工食堂\",\"name\":\"zgst\"},{\"title\":\"养老机构食堂\",\"name\":\"yljgst\"},{\"title\":\"工地食堂\",\"name\":\"gdst\"},{\"title\":\"其他食堂\",\"name\":\"qtst\"}]";
    //private final static String JYXM = "[{\"title\":\"预包装食品销售\",\"name\":\"ybzspxs\"},{\"title\":\"含预包装冷藏冷冻食品\",\"name\":\"hybzlcldsp\"},{\"title\":\"不含预包装冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"散装食品销售\",\"name\":\"szspxs\"},{\"title\":\"含冷藏冷冻食品\",\"name\":\"hlcldsp\"},{\"title\":\"不含冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"含散装熟食\",\"name\":\"hszss\"},{\"title\":\"不含散装熟食\",\"name\":\"bhszss\"},{\"title\":\"特殊食品销售\",\"name\":\"tsspxs\"},{\"title\":\"保健食品(批发)\",\"name\":\"bjsppf\"},{\"title\":\"保健食品(零售)\",\"name\":\"bjspls\"},{\"title\":\"特殊医学用途配方食品\",\"name\":\"tsyxytpfsp\"},{\"title\":\"婴幼儿配方乳粉\",\"name\":\"yyepfrf\"},{\"title\":\"其他婴幼儿配方食品\",\"name\":\"qtyyepfsp\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"其他类食品销售\",\"name\":\"qtlspxx\"},{\"title\":\"具体品种\",\"name\":\"qtlspxx_jtpz\"},{\"title\":\"热食类食品制售\",\"name\":\"rslspzs\"},{\"title\":\"冷食类食品制售\",\"name\":\"lslspzs\"},{\"title\":\"含烧卤熟肉\",\"name\":\"hslsr\"},{\"title\":\"不含烧卤熟肉\",\"name\":\"bhslsr\"},{\"title\":\"生食类食品制售\",\"name\":\"shlspzs\"},{\"title\":\"糕点类食品制售\",\"name\":\"gdlspzs\"},{\"title\":\"含裱花蛋糕\",\"name\":\"hbhdg\"},{\"title\":\"不含裱花蛋糕\",\"name\":\"bhbhdg\"},{\"title\":\"自制饮品制售\",\"name\":\"zzypzzs\"},{\"title\":\"含自酿酒\",\"name\":\"hznj\"},{\"title\":\"不含自酿酒\",\"name\":\"bhznj\"},{\"title\":\"其他类食品制售\",\"name\":\"qtlspzs\"},{\"title\":\"具体品种\",\"name\":\"qtlspzs_jtpz\"},{\"title\":\"食品经营管理\",\"name\":\"spjygl\"}]";
    private final static String ZTYT = "[{\"title\":\"食品销售经营者\",\"name\":\"spxsjyz\"},{\"title\":\"商场超市\",\"name\":\"sccs\"},{\"title\":\"便利店 \",\"name\":\"bld\"},{\"title\":\"食杂店\",\"name\":\"zsd\"},{\"title\":\"食品贸易商\",\"name\":\"spmys\"},{\"title\":\"食品自动售货销售商\",\"name\":\"spzdshxss\"},{\"title\":\"网络食品销售商\",\"name\":\"wlspxss\"},{\"title\":\"药店兼营 \",\"name\":\"ydjy\"},{\"title\":\"食品销售连锁企业总部 \",\"name\":\"spxslsqyzb\"},{\"title\":\"专卖店\",\"name\":\"zmd\"},{\"title\":\"餐饮服务经营者\",\"name\":\"cyfwjyz\"},{\"title\":\"大型餐馆\",\"name\":\"dxcg\"},{\"title\":\"中型餐馆\",\"name\":\"zxcg\"},{\"title\":\"小型餐馆\",\"name\":\"xxcg\"},{\"title\":\"中央厨房\",\"name\":\"zycf\"},{\"title\":\"集体用餐配送单位\",\"name\":\"jtycpsdw\"},{\"title\":\"小餐饮\",\"name\":\"xcy\"},{\"title\":\"饮品店\",\"name\":\"ypd\"},{\"title\":\"糕点店\",\"name\":\"gdd\"},{\"title\":\"餐饮服务连锁企业总部\",\"name\":\"cyfwlsqyzb\"},{\"title\":\"餐饮管理企业\",\"name\":\"cyglqy\"},{\"title\":\"单位食堂\",\"name\":\"dwst\"},{\"title\":\"学校食堂\",\"name\":\"xxst\"},{\"title\":\"托幼机构食堂\",\"name\":\"tejgst\"},{\"title\":\"职工食堂\",\"name\":\"zgst\"},{\"title\":\"养老机构食堂\",\"name\":\"yljgst\"},{\"title\":\"工地食堂\",\"name\":\"gdst\"},{\"title\":\"其他食堂\",\"name\":\"qtst\"}]";
    private final static String JYXM = "[{\"title\":\"预包装食品销售\",\"name\":\"ybzspxs\"},{\"title\":\"含预包装冷藏冷冻食品\",\"name\":\"hybzlcldsp\"},{\"title\":\"不含预包装冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"散装食品销售\",\"name\":\"szspxs\"},{\"title\":\"含冷藏冷冻食品\",\"name\":\"hlcldsp\"},{\"title\":\"不含冷藏冷冻食品\",\"name\":\"bhybzlcldsp\"},{\"title\":\"含散装熟食\",\"name\":\"hszss\"},{\"title\":\"不含散装熟食\",\"name\":\"bhszss\"},{\"title\":\"含散装酒\",\"name\":\"hszj\"},{\"title\":\"不含散装酒\",\"name\":\"bhszj\"},{\"title\":\"特殊食品销售\",\"name\":\"tsspxs\"},{\"title\":\"保健食品\",\"name\":\"bjsp\"},{\"title\":\"特殊医学用途配方食品\",\"name\":\"tsyxytpfsp\"},{\"title\":\"婴幼儿配方乳粉\",\"name\":\"yyepfrf\"},{\"title\":\"其他婴幼儿配方食品\",\"name\":\"qtyyepfsp\"},{\"title\":\"热食类食品制售\",\"name\":\"rslspzs\"},{\"title\":\"冷食类食品制售\",\"name\":\"lslspzs\"},{\"title\":\"含烧卤熟肉\",\"name\":\"hslsr\"},{\"title\":\"不含烧卤熟肉\",\"name\":\"bhslsr\"},{\"title\":\"生食类食品制售\",\"name\":\"shlspzs\"},{\"title\":\"糕点类食品制售\",\"name\":\"gdlspzs\"},{\"title\":\"含裱花类糕点\",\"name\":\"hbhlgd\"},{\"title\":\"不含裱花类糕点\",\"name\":\"bhbhlgd\"},{\"title\":\"自制饮品制售\",\"name\":\"zzypzzs\"},{\"title\":\"含自酿酒\",\"name\":\"hznj\"},{\"title\":\"不含自酿酒\",\"name\":\"bhznj\"},{\"title\":\"食品经营管理\",\"name\":\"spjygl\"}]";

    public void saveQyxx() {
        JSONObject msg = new JSONObject();
        boolean isNull=false;
        WxWsbsQyxx qyxx = WxWsbsQyxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));;
        if (qyxx==null) {
            qyxx = new WxWsbsQyxx();
            qyxx.set("id", StringUtils.getUUID());
            isNull=true;
        }
        qyxx.set("qyxz", getPara("qyxz"));
        qyxx.set("qyxzt", getPara("qyxzT"));
        qyxx.set("jyzmc", getPara("jyzmc"));
        qyxx.set("shtyxym", getPara("shtyxym"));
        qyxx.set("jycsdz", getPara("jycsdz"));
        qyxx.set("sblsh", Conts.getWsbsSblsh(getSession()));

        //添加联系人信息
        qyxx.set("lxr", getPara("lxr"));
        qyxx.set("lxrdh", getPara("lxrdh"));
        qyxx.set("jycsdzlx", getPara("jycsdzlx"));

        if (getPara("ckdzlx").equals("2")) {
            qyxx.set("ckdz", getPara("jycsdz"));
        } else {
            qyxx.set("ckdz", getPara("ckdz"));
        }
        qyxx.set("ckdzlx", getPara("ckdzlx"));
        qyxx.set("jycsmj", getPara("jycsmj"));
        try {
            boolean boo = false;
            if (isNull) {
                qyxx.set("addtime", new DateTime().getTimestamp());
                qyxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("updatetime", new DateTime().getTimestamp());
                qyxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("datastate", "I");
                boo = qyxx.save();
            } else {

                qyxx.set("updatetime", new DateTime().getTimestamp());
//                qyxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                qyxx.set("datastate", "U");
                boo = qyxx.update();

            }

            msg.put("success", boo);
            if (boo) {
                msg.put("data", qyxx);
                msg.put("msg", "保存企业信息成功");
                WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
                sb.set("qyxx", "1");
                sb.update();
            } else {
                msg.put("msg", "保存企业信息失败");
            }
        } catch (Exception e) {
            msg.put("data", qyxx);
            msg.put("success", false);
            msg.put("msg", "保存企业信息失败");
            e.printStackTrace();
        }

        renderJson(msg);
    }


    public void saveZzxx() {
        JSONObject msg = new JSONObject();
        String step = getPara("step");
        WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
        sb.set("lyb", getPara("lyb"));
        sb.set("zzxx", "1");
        try {
            boolean boo = sb.update();
            msg.put("success", boo);
            if (boo) {
                msg.put("msg", "保存材料信息成功");
            } else {

                msg.put("msg", "保存材料信息失败");
            }
        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存材料信息失败");
            e.printStackTrace();
        }

        renderJson(msg);
    }

    public void saveFrxx() {
        JSONObject msg = new JSONObject();
        WxWsbsFrxx frxx = WxWsbsFrxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
        boolean isNull=false;
        if (frxx==null) {
             frxx = new WxWsbsFrxx();
            frxx.set("id", StringUtils.getUUID());
            isNull=true;

        }
        frxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
        frxx.set("name", getPara("name"));
        frxx.set("sex", getPara("sex"));
        frxx.set("nation", getPara("nation"));
        frxx.set("post", getPara("post"));
        // frxx.set("place", getPara("place"));
        frxx.set("detailedPlace", getPara("detailedPlace"));
        frxx.set("live", getPara("live"));
        frxx.set("detailedLive", getPara("detailedLive"));
        frxx.set("idType", getPara("idType"));
        frxx.set("tel", getPara("tel"));
        frxx.set("zjh", getPara("zjh"));
        frxx.set("phone", getPara("phone"));
        frxx.set("sblsh", Conts.getWsbsSblsh(getSession()));


        try {
            boolean boo = false;
            if (isNull) {
                frxx.set("addtime", new DateTime().getTimestamp());
                frxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("updatetime", new DateTime().getTimestamp());
                frxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("datastate", "I");
                boo = frxx.save();
            } else {
                frxx.set("updatetime", new DateTime().getTimestamp());
                frxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                frxx.set("datastate", "U");
                boo = frxx.update();

            }

            msg.put("success", boo);
            if (boo) {
                msg.put("data", frxx);
                msg.put("msg", "保存法人信息成功");
                WxWsbsQyxx qyxx = WxWsbsQyxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
                if (qyxx.getStr("jycsdzlx").equals("1")) {
                    qyxx.set("jycsdz", frxx.getStr("live") + frxx.getStr("detailedLive"));
                    if (qyxx.getStr("ckdzlx").equals("2")) {
                        qyxx.set("ckdz", qyxx.getStr("jycsdz"));
                    }
                    qyxx.update();
                }
                WxWsbsSbxx sb = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
                sb.set("frxx", "1");
                sb.update();
            } else {
                msg.put("msg", "保存法人信息失败");
            }
        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存法人信息失败");
            e.printStackTrace();
        }


        renderJson(msg);
    }

    public void saveBgxx() {
        JSONObject msg = new JSONObject();
        boolean boo = false;
        String bgnrl = getPara("bgnrl");
        setSessionAttr("bgnrl",bgnrl);
        String ysblsh = getPara("ysblsh");
        WxWsbsSbxx sbxx = WxWsbsSbxx.me.findBySblsh(Conts.getWsbsSblsh(getSession()));
        sbxx.set("bgnr", getPara("bgnr"));
        sbxx.set("bgnrl", getPara("bgnrl"));
        sbxx.set("spjyxkzh", getPara("spjyxkzh"));
        if (!bgnrl.contains("11")) {
            WxWsbsQyxx qyxx = new WxWsbsQyxx();
            WxWsbsQyxx qyxxBG = WxWsbsQyxx.me.findBySblsh(ysblsh);
            qyxx.set("id", StringUtils.getUUID());
            qyxx.set("qyxz", qyxxBG.getStr("qyxz"));
            qyxx.set("qyxzt", qyxxBG.getStr("qyxzt"));
            qyxx.set("jyzmc", qyxxBG.getStr("jyzmc"));
            qyxx.set("shtyxym", qyxxBG.getStr("shtyxym"));
            qyxx.set("jycsdz", qyxxBG.getStr("jycsdz"));
            qyxx.set("sblsh", sbxx.getStr("sblsh"));
            //添加联系人信息
            qyxx.set("lxr", qyxxBG.getStr("lxr"));
            qyxx.set("lxrdh", qyxxBG.getStr("lxrdh"));
            qyxx.set("jycsdzlx", qyxxBG.getStr("jycsdzlx"));
            qyxx.set("ckdz", qyxxBG.getStr("jycsdz"));
            qyxx.set("ckdz", qyxxBG.getStr("ckdz"));
            qyxx.set("ckdzlx", qyxxBG.getStr("ckdzlx"));
            qyxx.set("jycsmj", qyxxBG.getStr("jycsmj"));
            qyxx.set("addtime", new DateTime().getTimestamp());
            qyxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
            qyxx.set("updatetime", new DateTime().getTimestamp());
            qyxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
            qyxx.set("datastate", "I");
            try {
                boo = qyxx.save();
                msg.put("success", boo);
                if (boo) {
                    msg.put("qyxx", qyxx);
                    msg.put("qyxxMsg", "保存企业信息成功");
                    sbxx.set("qyxx", "1");
                } else {
                    msg.put("msg", "保存企业信息失败");
                }
            } catch (Exception e) {
                msg.put("data", qyxx);
                msg.put("success", false);
                msg.put("msg", "保存企业信息失败");
                e.printStackTrace();
            }
        }else{
            sbxx.set("qyxx", "0");
        }
        if (!bgnrl.contains("12")) {
            List<WxWsbsFile> fileList = WxWsbsFile.me.findBySblsh(ysblsh);
            for (WxWsbsFile item : fileList) {
                WxWsbsFile file = new WxWsbsFile();
                file.set("id", StringUtils.getUUID());
                file.set("sblsh", sbxx.getStr("sblsh"));
                file.set("userid",item.getStr("userid"));
                file.set("filepath", item.getStr("filepath"));
                file.set("filename", item.getStr("filename"));
                file.set("tablename", item.getStr("tablename"));
                file.set("contenttype", item.getStr("contenttype"));
                file.set("addtime", new DateTime().getTimestamp());
                file.set("adduser", Conts.getSessionUser(getSession()).get("id"));
                file.set("updatetime", new DateTime().getTimestamp());
                file.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                file.set("datastate", "I");

            }
            sbxx.set("zzxx", "1");

        }else{
            sbxx.set("zzxx", "0");
        }
        if (!bgnrl.contains("13")) {


            WxWsbsFrxx frxx = new WxWsbsFrxx();
            WxWsbsFrxx frxxBG = WxWsbsFrxx.me.findBySblsh(ysblsh);
            frxx.set("id", StringUtils.getUUID());
            frxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
            frxx.set("name", frxx.getStr("name"));
            frxx.set("sex", frxx.getStr("sex"));
            frxx.set("nation", frxx.getStr("nation"));
            frxx.set("post", frxx.getStr("post"));
            // frxx.set("place", getPara("place"));
            frxx.set("detailedPlace", frxx.getStr("detailedPlace"));
            frxx.set("live", frxx.getStr("live"));
            frxx.set("detailedLive", frxx.getStr("detailedLive"));
            frxx.set("idType", frxx.getStr("idType"));
            frxx.set("tel", frxx.getStr("tel"));
            frxx.set("zjh", frxx.getStr("zjh"));
            frxx.set("phone", frxx.getStr("phone"));
            frxx.set("sblsh", sbxx.getStr("sblsh"));
            frxx.set("addtime", new DateTime().getTimestamp());
            frxx.set("adduser", Conts.getSessionUser(getSession()).get("id"));
            frxx.set("updatetime", new DateTime().getTimestamp());
            frxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
            frxx.set("datastate", "I");
            try {
                boo = frxx.save();
                msg.put("success", boo);
                if (boo) {
                    msg.put("frxx", frxx);
                    msg.put("frxxMsg", "保存法人信息成功");
                    sbxx.set("frxx", "1");
                } else {
                    msg.put("msg", "保存法人信息失败");
                }
            } catch (Exception e) {
                msg.put("success", false);
                msg.put("msg", "保存法人信息失败");
                e.printStackTrace();
            }
        }else{
            sbxx.set("frxx", "0");
        }

        if (!bgnrl.contains("14")) {
            WxWsbsSbxx sbxxT = WxWsbsSbxx.me.findBySblsh(getPara("ysblsh"));

            sbxx.set("bz", getPara("bz"));
            sbxx.set("ysblsh", ysblsh);
            sbxx.set("updatetime", new DateTime().getTimestamp());
            sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
            sbxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
            sbxx.set("ytlx", sbxxT.getStr("ytlx"));
            sbxx.set("wljy", sbxxT.getStr("wljy"));
            sbxx.set("stmd", sbxxT.getStr("stmd"));
            sbxx.set("wzlx", sbxxT.getStr("wzlx"));
            sbxx.set("wzdz", sbxxT.getStr("wzdz"));
            sbxx.set("jyxm", sbxxT.getStr("jyxm"));
            sbxx.set("datastate", "I");
            sbxx.set("sxmc", sbxxT.getStr("sxmc"));
            sbxx.set("sljg", sbxxT.getStr("sljg"));
            sbxx.set("spyj", sbxxT.getStr("spyj"));
            sbxx.set("lyb", sbxxT.getStr("lyb"));
            sbxx.set("isfrxx", sbxxT.getStr("isfrxx"));
            sbxx.set("sp_name", sbxxT.getStr("sp_name"));
            sbxx.set("sp_sex", sbxxT.getStr("sp_sex"));
            sbxx.set("sp_idtype", sbxxT.getStr("sp_idtype"));
            sbxx.set("sp_idcard", sbxxT.getStr("sp_idcard"));
            sbxx.set("sp_post", sbxxT.getStr("sp_post"));
            sbxx.set("sp_tel", sbxxT.getStr("sp_tel"));
            sbxx.set("ztytlx", sbxxT.getStr("ztytlx"));
            sbxx.set("jyxmlx", sbxxT.getStr("jyxmlx"));
            sbxx.set("print_state", sbxxT.getStr("print_state"));
            sbxx.set("sp_zsh", sbxxT.getStr("sp_zsh"));
            sbxx.set("ysblsh", sbxxT.getStr("sblsh"));
            sbxx.set("sbxx","1");

        }else{
            sbxx.set("sbxx","0");
        }
        boo = sbxx.update();
        msg.put("success", boo);
        if (boo) {
            msg.put("sbxx", sbxx);
            msg.put("sbxxMsg", "保存申报信息成功");

        } else {
            msg.put("msg", "保存申报信息失败");
        }
        renderJson(msg);
    }


    public void saveSbxx() {
        JSONObject msg = new JSONObject();
        String sblx = getPara("sblx");
        String sblsh = Conts.getWsbsSblsh(getSession());
        if (sblx == null) {
            sblx = "1";
        }
        String id = getPara("id");
        WxWsbsSbxx sbxx = null;
        boolean boo = false;
        if (!StringUtils.isEmpty(sblsh)) {
            sbxx = WxWsbsSbxx.me.findBySblsh(sblsh);
        } else {
            sbxx = new WxWsbsSbxx();
            sbxx.set("id", StringUtils.getUUID());
        }
        String zsblsh = getZshlsh();
        try {
            switch (sblx) {
                case "1":
                    sbxx.set("ztytlx", getPara("ztytlx"));
                    JSONArray ytlx = foreachJson(JSONArray.parseArray(ZTYT), getPara("ztytlx"));
                    sbxx.set("ytlx", ytlx.toString());
                    sbxx.set("jyxmlx", getPara("jyxmlx"));
                    JSONArray jyxm = foreachJson(JSONArray.parseArray(JYXM), getPara("jyxmlx"));
                    sbxx.set("jyxm", jyxm.toString());
                    sbxx.set("wljy", getPara("sfwljy"));
                    sbxx.set("stmd", getPara("stmd"));
                    sbxx.set("wzlx", getPara("wzlx"));
                    sbxx.set("wzdz", getPara("wzdz"));
                    sbxx.set("isfrxx", getPara("isfrxx"));
                    sbxx.set("sp_name", getPara("sp_name"));
                    sbxx.set("sp_sex", getPara("sp_sex"));
                    sbxx.set("sp_idtype", getPara("sp_idtype"));
                    sbxx.set("sp_idcard", getPara("sp_idcard"));
                    sbxx.set("sp_post", getPara("sp_post"));
                    sbxx.set("sp_tel", getPara("sp_tel"));
                    sbxx.set("sp_zsh", getPara("sp_zsh"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("datastate", "U");
                    sbxx.set("sbxx", "1");
                    sbxx.set("sblsh", zsblsh);
                    sbxx.set("sfrlsb","2");
                    boo = sbxx.update();
                    msg.put("success", boo);
                    WxWsbsQyxx qyxx = WxWsbsQyxx.me.findBySblsh(sblsh);
                    qyxx.set("sblsh", zsblsh);
                    boo = qyxx.update();
                    WxWsbsFrxx frxx = WxWsbsFrxx.me.findBySblsh(sblsh);
                    frxx.set("sblsh", zsblsh);
                    boo = frxx.update();
                    List<WxWsbsFile> fileList = WxWsbsFile.me.findBySblsh(sblsh);
                    for (WxWsbsFile file : fileList) {
                        file.set("sblsh", zsblsh);
                        boo = file.update();
                    }
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                        setSessionAttr("sblsh", zsblsh);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }

                    break;
                case "2":
                    WxWsbsSbxx sbxxT = WxWsbsSbxx.me.findBySblsh(getPara("ysblsh"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("sblsh", zsblsh);
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("ytlx", sbxxT.getStr("ytlx"));
                    sbxx.set("wljy", sbxxT.getStr("wljy"));
                    sbxx.set("stmd", sbxxT.getStr("stmd"));
                    sbxx.set("wzlx", sbxxT.getStr("wzlx"));
                    sbxx.set("wzdz", sbxxT.getStr("wzdz"));
                    sbxx.set("jyxm", sbxxT.getStr("jyxm"));
                    sbxx.set("qyxx", sbxxT.getStr("qyxx"));
                    sbxx.set("frxx", sbxxT.getStr("frxx"));
                    sbxx.set("sbxx", "1");
                    sbxx.set("zzxx", sbxxT.getStr("zzxx"));
                    sbxx.set("datastate", "I");
                    sbxx.set("sxmc", sbxxT.getStr("sxmc"));
                    sbxx.set("sljg", sbxxT.getStr("sljg"));
                    sbxx.set("spyj", sbxxT.getStr("spyj"));
                    sbxx.set("lyb", sbxxT.getStr("lyb"));
                    sbxx.set("isfrxx", sbxxT.getStr("isfrxx"));
                    sbxx.set("sp_name", sbxxT.getStr("sp_name"));
                    sbxx.set("sp_sex", sbxxT.getStr("sp_sex"));
                    sbxx.set("sp_idtype", sbxxT.getStr("sp_idtype"));
                    sbxx.set("sp_idcard", sbxxT.getStr("sp_idcard"));
                    sbxx.set("sp_post", sbxxT.getStr("sp_post"));
                    sbxx.set("sp_tel", sbxxT.getStr("sp_tel"));
                    sbxx.set("ztytlx", sbxxT.getStr("ztytlx"));
                    sbxx.set("jyxmlx", sbxxT.getStr("jyxmlx"));
                    sbxx.set("print_state", sbxxT.getStr("print_state"));
                    sbxx.set("sp_zsh", sbxxT.getStr("sp_zsh"));
                    sbxx.set("ysblsh", sbxxT.getStr("sblsh"));
                    sbxx.set("spjyxkzh", getPara("spjyxkzh"));
                    sbxx.set("sfrlsb","2");
                    boo = sbxx.update();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                        setSessionAttr("sblsh", zsblsh);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "3":
                    WxWsbsSbxx sbxxBg = WxWsbsSbxx.me.findBySblsh(getPara("ysblsh"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("sblsh", zsblsh);
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("ytlx", sbxxBg.getStr("ytlx"));
                    sbxx.set("wljy", sbxxBg.getStr("wljy"));
                    sbxx.set("stmd", sbxxBg.getStr("stmd"));
                    sbxx.set("wzlx", sbxxBg.getStr("wzlx"));
                    sbxx.set("wzdz", sbxxBg.getStr("wzdz"));
                    sbxx.set("jyxm", sbxxBg.getStr("jyxm"));
                    sbxx.set("qyxx", sbxxBg.getStr("qyxx"));
                    sbxx.set("frxx", sbxxBg.getStr("frxx"));
                    sbxx.set("sbxx", "1");
                    sbxx.set("zzxx", sbxxBg.getStr("zzxx"));
                    sbxx.set("datastate", "I");
                    sbxx.set("sxmc", sbxxBg.getStr("sxmc"));
                    sbxx.set("sljg", sbxxBg.getStr("sljg"));
                    sbxx.set("spyj", sbxxBg.getStr("spyj"));
                    sbxx.set("lyb", sbxxBg.getStr("lyb"));
                    sbxx.set("isfrxx", sbxxBg.getStr("isfrxx"));
                    sbxx.set("sp_name", sbxxBg.getStr("sp_name"));
                    sbxx.set("sp_sex", sbxxBg.getStr("sp_sex"));
                    sbxx.set("sp_idtype", sbxxBg.getStr("sp_idtype"));
                    sbxx.set("sp_idcard", sbxxBg.getStr("sp_idcard"));
                    sbxx.set("sp_post", sbxxBg.getStr("sp_post"));
                    sbxx.set("sp_tel", sbxxBg.getStr("sp_tel"));
                    sbxx.set("ztytlx", sbxxBg.getStr("ztytlx"));
                    sbxx.set("jyxmlx", sbxxBg.getStr("jyxmlx"));
                    sbxx.set("print_state", sbxxBg.getStr("print_state"));
                    sbxx.set("sp_zsh", sbxxBg.getStr("sp_zsh"));
                    sbxx.set("ysblsh", sbxxBg.getStr("sblsh"));
                    sbxx.set("spjyxkzh", getPara("spjyxkzh"));
                    sbxx.set("sfrlsb","2");
                    boo = sbxx.update();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                        setSessionAttr("sblsh", zsblsh);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "4":
                    WxWsbsSbxx sbxxBz = WxWsbsSbxx.me.findBySblsh(getPara("ysblsh"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("sblsh", zsblsh);
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("ytlx", sbxxBz.getStr("ytlx"));
                    sbxx.set("wljy", sbxxBz.getStr("wljy"));
                    sbxx.set("stmd", sbxxBz.getStr("stmd"));
                    sbxx.set("wzlx", sbxxBz.getStr("wzlx"));
                    sbxx.set("wzdz", sbxxBz.getStr("wzdz"));
                    sbxx.set("jyxm", sbxxBz.getStr("jyxm"));
                    sbxx.set("qyxx", sbxxBz.getStr("qyxx"));
                    sbxx.set("frxx", sbxxBz.getStr("frxx"));
                    sbxx.set("sbxx", "1");
                    sbxx.set("zzxx", sbxxBz.getStr("zzxx"));
                    sbxx.set("datastate", "I");
                    sbxx.set("sxmc", sbxxBz.getStr("sxmc"));
                    sbxx.set("sljg", sbxxBz.getStr("sljg"));
                    sbxx.set("spyj", sbxxBz.getStr("spyj"));
                    sbxx.set("lyb", sbxxBz.getStr("lyb"));
                    sbxx.set("isfrxx", sbxxBz.getStr("isfrxx"));
                    sbxx.set("sp_name", sbxxBz.getStr("sp_name"));
                    sbxx.set("sp_sex", sbxxBz.getStr("sp_sex"));
                    sbxx.set("sp_idtype", sbxxBz.getStr("sp_idtype"));
                    sbxx.set("sp_idcard", sbxxBz.getStr("sp_idcard"));
                    sbxx.set("sp_post", sbxxBz.getStr("sp_post"));
                    sbxx.set("sp_tel", sbxxBz.getStr("sp_tel"));
                    sbxx.set("ztytlx", sbxxBz.getStr("ztytlx"));
                    sbxx.set("jyxmlx", sbxxBz.getStr("jyxmlx"));
                    sbxx.set("print_state", sbxxBz.getStr("print_state"));
                    sbxx.set("sp_zsh", sbxxBz.getStr("sp_zsh"));
                    sbxx.set("ysblsh", sbxxBz.getStr("sblsh"));
                    sbxx.set("spjyxkzh", getPara("spjyxkzh"));
                    sbxx.set("sfrlsb","0");
                    boo = sbxx.update();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                        setSessionAttr("sblsh", zsblsh);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                case "5":

                    WxWsbsSbxx sbxxZx = WxWsbsSbxx.me.findBySblsh(getPara("ysblsh"));
                    sbxx.set("bz", getPara("bz"));
                    sbxx.set("sblsh", zsblsh);
                    sbxx.set("updatetime", new DateTime().getTimestamp());
                    sbxx.set("updateuser", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("userid", Conts.getSessionUser(getSession()).get("id"));
                    sbxx.set("ytlx", sbxxZx.getStr("ytlx"));
                    sbxx.set("wljy", sbxxZx.getStr("wljy"));
                    sbxx.set("stmd", sbxxZx.getStr("stmd"));
                    sbxx.set("wzlx", sbxxZx.getStr("wzlx"));
                    sbxx.set("wzdz", sbxxZx.getStr("wzdz"));
                    sbxx.set("jyxm", sbxxZx.getStr("jyxm"));
                    sbxx.set("qyxx", sbxxZx.getStr("qyxx"));
                    sbxx.set("frxx", sbxxZx.getStr("frxx"));
                    sbxx.set("sbxx", "1");
                    sbxx.set("zzxx", sbxxZx.getStr("zzxx"));
                    sbxx.set("datastate", "I");
                    sbxx.set("sxmc", sbxxZx.getStr("sxmc"));
                    sbxx.set("sljg", sbxxZx.getStr("sljg"));
                    sbxx.set("spyj", sbxxZx.getStr("spyj"));
                    sbxx.set("lyb", sbxxZx.getStr("lyb"));
                    sbxx.set("isfrxx", sbxxZx.getStr("isfrxx"));
                    sbxx.set("sp_name", sbxxZx.getStr("sp_name"));
                    sbxx.set("sp_sex", sbxxZx.getStr("sp_sex"));
                    sbxx.set("sp_idtype", sbxxZx.getStr("sp_idtype"));
                    sbxx.set("sp_idcard", sbxxZx.getStr("sp_idcard"));
                    sbxx.set("sp_post", sbxxZx.getStr("sp_post"));
                    sbxx.set("sp_tel", sbxxZx.getStr("sp_tel"));
                    sbxx.set("ztytlx", sbxxZx.getStr("ztytlx"));
                    sbxx.set("jyxmlx", sbxxZx.getStr("jyxmlx"));
                    sbxx.set("print_state", sbxxZx.getStr("print_state"));
                    sbxx.set("sp_zsh", sbxxZx.getStr("sp_zsh"));
                    sbxx.set("ysblsh", sbxxZx.getStr("sblsh"));
                    sbxx.set("spjyxkzh", getPara("spjyxkzh"));
                    sbxx.set("sfrlsb","0");
                    boo = sbxx.update();
                    msg.put("success", boo);
                    if (boo) {
                        msg.put("data", sbxx);
                        msg.put("msg", "保存申报信息成功");
                        setSessionAttr("sblsh", zsblsh);
                    } else {
                        msg.put("msg", "保存申报信息失败");
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            msg.put("success", false);
            msg.put("msg", "保存申报信息失败");
            e.printStackTrace();
        }
        renderJson(msg);
    }

    private Object createSblsh() {
        List<WxWsbsSbxx> sbxxList = WxWsbsSbxx.me.findLikeData();
        NumberFormat f = new DecimalFormat("00000");
        String sblsh = null;
        if (sbxxList == null || sbxxList.size() == 0) {
            sblsh = new DateTime().getDateTimeNYR() + f.format(1);
        } else {
            sblsh = new DateTime().getDateTimeNYR() + f.format(sbxxList.size() + 1);
        }
        return sblsh;
    }


    public void findFrxx() {
        WxWsbsFrxx frxx = WxWsbsFrxx.me.findByUser(Conts.getSessionUser(getSession()).getStr("id"));
        renderJson(frxx);
    }

    private JSONArray foreachJson(JSONArray items, String lx) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < items.size(); i++) {
            JSONObject json = JSONObject.parseObject(items.get(i).toString());
            json.put("value", "0");
            for (int j = 0; j < items.size(); j++) {

                JSONObject itemjson = JSONObject.parseObject(items.get(j).toString());
                //&&getPara(itemjson.getString("name")).contains(json.getString("title"))
                if (!StringUtils.isEmpty(getPara(itemjson.getString("name"))) && getPara(itemjson.getString("name")).equals(json.getString("title"))) {
                    System.out.println(i + "条数据" + json.getString("title") + "=" + getPara(itemjson.getString("name")));
                    json.put("value", "1");
                }

            }
            jsonArray.add(json);
        }
        JSONArray jsonValue = new JSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject json = JSONObject.parseObject(jsonArray.get(i).toString());
            if (lx.contains(json.getString("title"))) {
                json.put("value", "1");
                System.out.println(json.toString());
            }
            jsonValue.add(json);
        }

        return jsonValue;
    }

    public void toBszn() {
        String values = getPara("values");
        System.out.println(values);
        setAttr("values", values);
        render("/WEB-INF/wsbs/bszn/index.html");
    }

    public String getZshlsh() {
        List<WxWsbsSbxx> sbxxList = WxWsbsSbxx.me.findLikeData();
        NumberFormat f = new DecimalFormat("00000");
        String zsblsh = null;
        if (sbxxList == null || sbxxList.size() == 0) {
            zsblsh = new DateTime().getDateTimeNYR() + f.format(1);
        } else {
            zsblsh = new DateTime().getDateTimeNYR() + f.format(sbxxList.size() + 1);
        }
        return zsblsh;
    }
}
