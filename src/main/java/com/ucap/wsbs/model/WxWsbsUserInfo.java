package com.ucap.wsbs.model;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;
import com.ucap.util.DateTime;
import com.ucap.util.StringUtils;
import com.ucap.weixin.model.WxWsbsUser;

/**
 * Created by Administrator on 2018/5/9.
 */
public class WxWsbsUserInfo extends Model<WxWsbsUserInfo> {
    static Log log = Log.getLog(WxWsbsUserInfo.class);

    public static final WxWsbsUserInfo me = new WxWsbsUserInfo();
    public boolean save(String openId,WxWsbsUserInfo userInfo){


        /**
         * 1、判断openId 是否存在
         *    如果存在就update
         *    如果不存在就保存
         */
        WxWsbsUserInfo user = findByOpenId(openId);
        if (user!=null) {
            userInfo.set("id",user.get("id"));
            userInfo.set("updatetime", new DateTime().getTimestamp());
            return userInfo.update();
        }else {
            if (StrKit.notBlank(openId)) {
                userInfo.set("addtime", new DateTime().getTimestamp());
                userInfo.set("id", StringUtils.getUUID());
                return userInfo.save();
            }
        }
        return false;
    }
    public WxWsbsUserInfo findByOpenId(String openid) {
        return this.findFirst("select * from WX_WSBS_USER_INFO where OPENID=?",openid);
    }
}
