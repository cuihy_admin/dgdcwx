package com.ucap.wsbs.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/9.
 */
public class WxWsbsFile extends Model<WxWsbsFile> {
    static Log log = Log.getLog(WxWsbsFile.class);

    public static final WxWsbsFile me = new WxWsbsFile();

    public int deleteSignature(String userId){
        return Db.delete("delete WX_WSBS_FILE WHERE SBLSH is NULL AND TABLENAME = 'SIGNATURE' AND USERID=?", userId);
    }

    public List<WxWsbsFile> findLicenseFileList(String userId, String sblsh) {
        return this.find("select * from WX_WSBS_FILE WHERE (TABLENAME like '%_ZB' OR TABLENAME like '%_FB' OR TABLENAME like 'GSP%' OR TABLENAME like '%_ZXJDS') AND USERID=? AND SBLSH =?", userId, sblsh);
    }

    public int updateSblsh(String sblsh, String tableName, String userId){
        return Db.update("update WX_WSBS_FILE set SBLSH =? where SBLSH IS NULL AND TABLENAME =? AND USERID=?", sblsh, tableName, userId);
    }

    public List<WxWsbsFile> findFileListByTableNameAndUserId(String tableName, String userId, String sblsh) {
        return this.find("select * from WX_WSBS_FILE WHERE TABLENAME =? AND USERID=? AND SBLSH =?", tableName, userId, sblsh);
    }

    public List<WxWsbsFile> findFileListByTableNameAndUserId(String tableName, String userId) {
        return this.find("select * from WX_WSBS_FILE WHERE SBLSH IS NULL AND TABLENAME =? AND USERID=?", tableName, userId);
    }

    public List<WxWsbsFile> findFileListByTableName( List paras) {
        return this.find("select * from WX_WSBS_FILE WHERE TABLENAME =? AND SBLSH=?",paras.get(0),paras.get(1));
    }

    public WxWsbsFile findSbclBySblsh(String wsbsSblsh) {
        return this.findFirst("select * from WX_WSBS_FILE WHERE TABLENAME ='dzsqb' AND SBLSH=?",wsbsSblsh);
    }

    public List<WxWsbsFile> findFileListBysblshAndTableName(List paras){
        return this.find("select * from WX_WSBS_FILE WHERE SBLSH = ? AND TABLENAME = ?",paras.get(1),paras.get(0));
    }

    public List<WxWsbsFile> findClsbyUIDandSBLSHandTbName(List paras){
        return this.find("select * from WX_WSBS_FILE WHERE SBLSH = ? AND TABLENAME = ? ",paras.get(0),paras.get(1));
    }

    public WxWsbsFile findWordBysblshAndTableName(List paras){
        return this.findFirst("select * from wx_wsbs_file where sblsh = ? and tablename = ?",paras.get(0),paras.get(1));
    }

    public List<WxWsbsFile> findBySblsh(String sblsh) {
        return this.find("select * from wx_wsbs_file where sblsh = ? ",sblsh);
    }
}
