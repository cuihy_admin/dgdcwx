package com.ucap.wsbs.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;
import com.ucap.util.DateTime;

import java.util.List;

/**
 * Created by Administrator on 2018/5/9.
 */
public class WxWsbsSbxx extends Model<WxWsbsSbxx> {
    static Log log = Log.getLog(WxWsbsSbxx.class);

    public static final WxWsbsSbxx me = new WxWsbsSbxx();

    public WxWsbsSbxx findByOpenUserId(String userId) {
        return this.findFirst("select * from WX_WSBS_SBXX where USERID=?", userId);
    }

    public List<WxWsbsSbxx> findLikeData() {
        System.out.println(new DateTime().getDateTimeNYR());
        return this.find("select * from WX_WSBS_SBXX where SBLSH like ?", "%"+new DateTime().getDateTimeNYR()+"%");
    }

    /**
     * 通过申报流水号获取申报信息
     * @param wsbsSblsh 申报流水号
     * @return 申报信息
     */
    public WxWsbsSbxx findBySblsh(String wsbsSblsh) {
        return this.findFirst("select * from WX_WSBS_SBXX where SBLSH=?",wsbsSblsh);
    }
    /**
     * 通过申报流水号获取未提交的数据
     * @param wsbsSblsh 申报流水号
     * @return 申报信息
     */
    public WxWsbsSbxx findBySblshAndSbxx(String wsbsSblsh) {
        return this.findFirst("select * from WX_WSBS_SBXX where SBLSH=? AND SBXX <> '1'",wsbsSblsh);
    }
    public List<WxWsbsSbxx> findByUserId(String id) {
        return this.find("SELECT s.*, x. NAME FROM WX_WSBS_SBXX s LEFT JOIN WX_WSBS_FRXX x on s.sblsh = x.sblsh WHERE 1=1  and s.userid=? and s.sbxx = '0'  and s.datastate <> 'D' ORDER BY s.ADDTIME ",id);
    }

    public List<WxWsbsSbxx> findAllByUserId(String id) {
        return this.find("SELECT s.*, x. NAME FROM WX_WSBS_SBXX s LEFT JOIN WX_WSBS_FRXX x on s.sblsh = x.sblsh WHERE 1=1  and s.userid=?   ORDER BY s.ADDTIME ",id);

    }


    public List<WxWsbsSbxx> findByUserIdcard(String id, String sblx) {
        return this.find("SELECT s.*, x. NAME FROM WX_WSBS_SBXX s LEFT JOIN WX_WSBS_FRXX x on s.sblsh = x.sblsh WHERE 1=1  and s.userid=? and s.sblx=?   ORDER BY s.ADDTIME ",id,sblx);

    }
}
