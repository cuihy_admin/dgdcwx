package com.ucap.wsbs.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class WxWsbsQyxx extends Model<WxWsbsQyxx> {
    static Log log = Log.getLog(WxWsbsQyxx.class);

    public static final WxWsbsQyxx me = new WxWsbsQyxx();

    public WxWsbsQyxx findBySblsh(String wsbsSblsh) {
        return this.findFirst("select * from WX_WSBS_QYXX where SBLSH=?",wsbsSblsh);
    }
}
