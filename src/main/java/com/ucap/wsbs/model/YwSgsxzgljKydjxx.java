package com.ucap.wsbs.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;
import com.ucap.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/9/4.
 */
public class YwSgsxzgljKydjxx  extends Model<YwSgsxzgljKydjxx> {
    static Log log = Log.getLog(YwSgsxzgljKydjxx.class);

    public static final YwSgsxzgljKydjxx me = new YwSgsxzgljKydjxx();

    public List<YwSgsxzgljKydjxx> findByShtyxym(String frsfzh) {
        return this.use("qyxxDb").find("select * from YW_SGSXZGLJ_KYDJXX where FRSFZH=?",frsfzh);
    }

    public List<YwSgsxzgljKydjxx> findByQyzch(String qyzch){
        return this.use("qyxxDb").find("select * from YW_SGSXZGLJ_KYDJXX where QYZCH=?", qyzch);
    }

    public List<YwSgsxzgljKydjxx> findZxList(String formatDate) {
        if(StringUtils.isEmpty(formatDate)){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();

            //过去七天
            c.setTime(new Date());
            c.add(Calendar.DATE, - 7);
            Date d = c.getTime();
            formatDate = format.format(d);
        }
        return this.use("qyxxDb").find("select * from YW_SGSXZGLJ_KYDJXX where ( QYZT = '已注销' or QYZT = '吊销') and to_char(UPDATETIME, 'yyyy-mm-dd') = '"+ formatDate +"'");
    }
}
