package com.ucap.wsbs.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class WxWsbsFrxx extends Model<WxWsbsFrxx> {
    static Log log = Log.getLog(WxWsbsFrxx.class);


    public static final WxWsbsFrxx me = new WxWsbsFrxx();

    public WxWsbsFrxx findByUser(String userId) {
        return this.findFirst("select * from WX_WSBS_FRXX where USERID=?",userId);
    }

    public WxWsbsFrxx findBySblsh(String wsbsSblsh) {
        return this.findFirst("select * from WX_WSBS_FRXX where SBLSH=?",wsbsSblsh);
    }
}
