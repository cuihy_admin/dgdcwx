package com.ucap.config;

import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.AnsiSqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.wxaapp.WxaConfig;
import com.jfinal.wxaapp.WxaConfigKit;
import com.ucap.controller.api.WeixinApiController;
import com.ucap.ext.CompatibleOracleDialect;
import com.ucap.ext.DateDirective;
import com.ucap.ext.MyStringExt;
import com.ucap.spyxmp.controller.*;
import com.ucap.spyxmp.model.*;
import com.ucap.weixin.controller.WeiXinOauthController;
import com.ucap.weixin.model.WxWsbsUser;
import com.ucap.wsbs.api.FileApiController;
import com.ucap.wsbs.api.WsbsApiController;
import com.ucap.wsbs.controller.*;
import com.ucap.wsbs.controller.FileController;
import com.ucap.wsbs.controller.ThirdDataController;
import com.ucap.wsbs.model.*;


/**
 * 本 demo 仅表达最为粗浅的 jfinal 用法，更为有价值的实用的企业级用法 详见 JFinal 俱乐部:
 * http://jfinal.com/club
 * 
 * API引导式配置
 */
public class Config extends JFinalConfig {

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		/*PropKit.use("jfrx_config.txt");
		*/
		PropKit.use("a_little_config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", true));
		ApiConfig apiConfig = new ApiConfig();
		apiConfig.setAppId("wxd50ea9577951a885");
		apiConfig.setAppSecret("c2baeca12793fe97e88d7aa089310b1c");
		ApiConfigKit.putApiConfig(apiConfig);
		WxaConfig wc = new WxaConfig();
		wc.setAppId("wxd50ea9577951a885");
		wc.setAppSecret("c2baeca12793fe97e88d7aa089310b1c");
		WxaConfigKit.setWxaConfig(wc);
		//String path = Config.class.getResource("/").toURI().getPath();
		System.out.println("path"+ PathKit.getWebRootPath());

		//me.setViewType(ViewType.FREE_MARKER);
		me.setBaseUploadPath(PathKit.getWebRootPath()+"/files/upload");
		me.setBaseDownloadPath(PathKit.getWebRootPath()+"/files/download");

	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		//me.add("/", IndexControllerbak.class); // 第三个参数为该Controller的视图存放路径
		//me.add("/wfw/wsbs", WsbsControllerbak.class);
		me.add("/", IndexController.class); // 第三个参数为该Controller的视图存放路径
		me.add("/wfw/wsbs", WsbsController.class);
		me.add("/wfw/oauth", WeiXinOauthController.class);
		me.add("/wfw/msg", WeixinApiController.class);
		me.add("/wfw/file", FileController.class);
		me.add("/api/wsbs", WsbsApiController.class);
		me.add("/api/file", FileApiController.class);
		me.add("/wfw/wsbs/thirdData", ThirdDataController.class);
		me.add("/wfw/spyxmp/file", com.ucap.spyxmp.controller.FileController.class);
		me.add("/wfw/spyxmp/spjyxkz", SpjyxkzController.class);
		me.add("/wfw/spyxmp/ypjyxkz", YpjyxkzController.class);
		me.add("/wfw/spyxmp/delylqxjybapz", DelylqxjybapzController.class);
		me.add("/wfw/spyxmp/ylqxjyxkz", YlqxjyxkzController.class);
		me.add("/wfw/spyxmp/mailInfo", MailInfoController.class);
		me.add("/wfw/spyxmp/applyItem", SpyxmpApplyItemController.class);

	}

	public void configEngine(Engine me) {
		me.addDirective("timeToData", new DateDirective());
		me.addExtensionMethod(String.class, MyStringExt.class);

	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {

		C3p0Plugin cp = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"),PropKit.get("password").trim(),"oracle.jdbc.driver.OracleDriver");
		cp.setInitialPoolSize(3);
		cp.setMaxIdleTime(10);
		cp.setMinPoolSize(3);
		cp.setMaxIdleTime(6);
		me.add(cp);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(cp);
		arp.setDialect(new AnsiSqlDialect());
		me.add(arp);

		C3p0Plugin cp2 = new C3p0Plugin(PropKit.get("qysj_jdbcUrl"), PropKit.get("qysj_user"),PropKit.get("qysj_password").trim(),"oracle.jdbc.driver.OracleDriver");
		cp2.setInitialPoolSize(3);
		cp2.setMaxIdleTime(10);
		cp2.setMinPoolSize(3);
		cp2.setMaxIdleTime(6);
		me.add(cp2);
		ActiveRecordPlugin arp2 = new ActiveRecordPlugin("qyxxDb",cp2);
		arp2.setDialect(new AnsiSqlDialect());
		me.add(arp2);

       /* C3p0Plugin cp3 = new C3p0Plugin(PropKit.get("yth_jdbcUrl"), PropKit.get("yth_user"),PropKit.get("yth_password").trim(),"oracle.jdbc.driver.OracleDriver");
        cp3.setInitialPoolSize(3);
        cp3.setMaxIdleTime(10);
        cp3.setMinPoolSize(3);
        cp3.setMaxIdleTime(6);
        me.add(cp3);
        ActiveRecordPlugin arp3 = new ActiveRecordPlugin("ythDb",cp3);
        arp3.setDialect(new AnsiSqlDialect());
        me.add(arp3);*/
		/*  // 配置 druid 数据库连接池插件
		//	DruidPlugin druidPlugin = new  DruidPlugin(); me.add(druidPlugin);
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"),PropKit.get("password").trim());
		me.add(druidPlugin);
		 // 配置ActiveRecord插件
		//配置Oracle驱动
		druidPlugin.setDriverClass("oracle.jdbc.driver.OracleDriver");

		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);
		// 配置Oracle方言*/
		arp.setDialect(new CompatibleOracleDialect());
		// 配置属性名(字段名)大小写不敏感容器工厂
		arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.addMapping("WX_WSBS_USER", WxWsbsUser.class);
		arp.addMapping("WX_WSBS_QYXX", WxWsbsQyxx.class);
		arp.addMapping("WX_WSBS_SBXX", WxWsbsSbxx.class);
		arp.addMapping("WX_WSBS_FRXX", WxWsbsFrxx.class);
		arp.addMapping("WX_WSBS_FILE", WxWsbsFile.class);
		arp.addMapping("WX_WSBS_USER_INFO", WxWsbsUserInfo.class);
		arp.addMapping("SPYXMP_APPLY_ITEM", SpyxmpApplyItem.class);
		arp.addMapping("SPYXMP_DELYLQXJYBAPZ", SpyxmpDelylqxjybapz.class);
		arp.addMapping("SPYXMP_SPJYXKZ", SpyxmpSpjyxkz.class);
		arp.addMapping("SPYXMP_YLQXJYXKZ", SpyxmpYlqxjyxkz.class);
		arp.addMapping("SPYXMP_YPJYXKZ", SpyxmpYpjyxkz.class);
		arp.addMapping("SPYXMP_MAIL_INFO", SpyxmpMailInfo.class);
		arp.addMapping("SPYXMP_ZDZX", SpyxmpZdzx.class);
		me.add(arp);

		//定时器插件配置
		Cron4jPlugin taskCp = new Cron4jPlugin(PropKit.use("cron4j.txt"));
		me.add(taskCp);
	}

	public static DruidPlugin createDruidPlugin() {

		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());


	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {

		me.add(new ContextPathHandler("basePath"));
	}

}
