package com.ucap.cron4j;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.ucap.spyxmp.data.ResultModel;
import com.ucap.spyxmp.data.ZxjdsPdfModel;
import com.ucap.spyxmp.exception.GlobalErrorCode;
import com.ucap.spyxmp.model.*;
import com.ucap.spyxmp.util.CommonUtil;
import com.ucap.spyxmp.util.LicensePdfUtil;
import com.ucap.util.DateTime;
import com.ucap.util.DateUtil;
import com.ucap.util.StringUtils;
import com.ucap.wsbs.model.WxWsbsFile;
import com.ucap.wsbs.model.YwSgsxzgljKydjxx;
import com.ucap.wsbs.util.ConvertCodeUtil;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 自动注销类
 */
public class ZdzxTask implements Runnable {


    @Override
    public void run() {
        System.out.println("开始执行自动注销方法=========================================start");


        syncZdzx();


        System.out.println("结束执行自动注销方法=========================================start");
    }


    @Before(Tx.class)
    private boolean syncZdzx() {

        //获取当天同步的注销数据
        List<YwSgsxzgljKydjxx> kydjxxList = YwSgsxzgljKydjxx.me.findZxList(null);
        System.out.println(kydjxxList.size());

        //食品经营许可证注销
        if (kydjxxList.size() > 0) {

            for (YwSgsxzgljKydjxx kydjxx : kydjxxList) {

                SpyxmpZdzx zdzxData = new SpyxmpZdzx();
                zdzxData.set("ID", StringUtils.getUUID());
                zdzxData.set("QYMC", kydjxx.getStr("QYMC"));
                zdzxData.set("TYSHXYDM", kydjxx.getStr("DGS_TYSHXYDM"));
                zdzxData.set("ISSYNCRONZZ", "0");
                zdzxData.set("CREATE_DATE", DateUtil.getCurrtentDateTime());
                zdzxData.set("MODIFY_DATE", DateUtil.getCurrtentDateTime());


                JSONObject tableNameJson = new JSONObject();
                tableNameJson.put("SPYXMP_SPJYXKZ", 1);
                tableNameJson.put("SPYXMP_YPJYXKZ", 2);
                tableNameJson.put("SPYXMP_DELYLQXJYBAPZ", 3);
                tableNameJson.put("SPYXMP_YLQXJYXKZ", 4);

                for (String tableName : tableNameJson.keySet()) {
                    String tableNameCode = tableNameJson.getString(tableName);
                    String sblsh = ConvertCodeUtil.getZdzxSblsh(tableNameCode, "2");
                    SpyxmpApplyItem spyxmpApplyItem = new SpyxmpApplyItem();
                    spyxmpApplyItem.set("id", StringUtils.getUUID());
                    spyxmpApplyItem.set("userid", "developer");
                    spyxmpApplyItem.set("status", "1");
                    spyxmpApplyItem.set("sblsh", sblsh);
                    spyxmpApplyItem.set("tablename", tableName);
                    spyxmpApplyItem.set("type", "2");
                    spyxmpApplyItem.set("ADDTIME", DateUtil.getCurrtentDateTime());
                    spyxmpApplyItem.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
                    Boolean flag;


                    switch (tableNameJson.getString(tableName)) {
                        case "1":
                            //食品经营许可证注销
                            flag = spjyxkzSync(sblsh, tableNameCode, kydjxx, spyxmpApplyItem);
                            break;
                        case "2":
                            //药品证注销
                            flag = ypSync(sblsh, tableNameCode, kydjxx, spyxmpApplyItem);
                            break;
                        case "3":
                            flag = false;
                            //flag = delylqxjybzpzSync(sblsh, tableName, kydjxx, spyxmpApplyItem);
                            break;
                        default:
                            flag = false;
                            //flag = ylqxjyxkzSync(sblsh, tableName, kydjxx, spyxmpApplyItem);
                            break;
                    }

                    if(flag){
                        zdzxData.set("ISSYNCRONZZ", "1");
                    }else{
                        zdzxData.set("ISSYNCRONZZ", "0");
                    }

                }
                //暂时只保存已注销的数据
                if("1".equals(zdzxData.getStr("ISSYNCRONZZ"))){
                    zdzxData.save();
                }
            }
        }

        return true;
    }

    /**
     * 医疗器械经营许可证注销
     * @param sblsh
     * @param tableNameCode
     * @param kydjxx
     */
    private boolean ylqxjyxkzSync(String sblsh, String tableNameCode, YwSgsxzgljKydjxx kydjxx, SpyxmpApplyItem spyxmpApplyItem){

        spyxmpApplyItem.save();
        SpyxmpYlqxjyxkz data = new SpyxmpYlqxjyxkz();
        data.set("ID", StringUtils.getUUID());
        data.set("APPLYID", sblsh);
        data.set("TYPE", "2");
        data.set("QYMC", kydjxx.getStr("QYMC"));
        //data.set("BABH", "");
        //data.set("BARQ", "");
        data.set("TYSHXYDM", kydjxx.getStr("DGS_TYSHXYDM"));
        data.set("FDDBR", kydjxx.getStr("FDDBR"));
        data.set("QYFZR", "");
        data.set("JYFS", "1");
        data.set("JYMS", "1");
        data.set("ZS", kydjxx.getStr("ZS"));
        data.set("JYCS", "");
        data.set("KFDZ", "");
        data.set("JYFW", kydjxx.getStr("YBJYXM"));
        data.set("LXRXM", kydjxx.getStr("FDDBR"));
        data.set("LXRZJHM", kydjxx.getStr("FRSFZH"));
        data.set("YJRDH", kydjxx.getStr("LXDH"));
        data.set("LXRCZ", "");
        data.set("LXRYX", "");
        data.set("REASON", "自动注销");
        data.set("ADDTIME", DateUtil.getCurrtentDateTime());
        data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
        //添加邮寄信息
        data.set("SFJBYJ", "1");
        data.set("YJR", kydjxx.getStr("FDDBR"));
        data.set("YJRDH", kydjxx.getStr("LXDH"));
        data.set("YJRDZ", kydjxx.getStr("ZS"));
        return data.save();
    }

    /**
     * 第二类医疗器械经营备案凭证注销
     * @param sblsh
     * @param tableNameCode
     * @param kydjxx
     */
    private boolean delylqxjybzpzSync(String sblsh, String tableNameCode, YwSgsxzgljKydjxx kydjxx, SpyxmpApplyItem spyxmpApplyItem){

        spyxmpApplyItem.save();
        SpyxmpDelylqxjybapz data = new SpyxmpDelylqxjybapz();
        data.set("ID", StringUtils.getUUID());
        data.set("APPLYID", sblsh);
        data.set("TYPE", "2");
        data.set("QYMC", kydjxx.getStr("QYMC"));
        data.set("BABH", "");
        data.set("BARQ", "");
        data.set("TYSHXYDM", kydjxx.getStr("DGS_TYSHXYDM"));
        data.set("FDDBR", kydjxx.getStr("FDDBR"));
        data.set("QYFZR", "");
        data.set("JYFS", "1");
        data.set("JYMS", "1");
        data.set("ZS", kydjxx.getStr("ZS"));
        data.set("JYCS", "");
        data.set("KFDZ", "");
        data.set("JYFW", kydjxx.getStr("YBJYXM"));
        data.set("LXRXM", kydjxx.getStr("FDDBR"));
        data.set("LXRZJHM", kydjxx.getStr("FRSFZH"));
        data.set("YJRDH", kydjxx.getStr("LXDH"));
        data.set("LXRCZ", "");
        data.set("LXRYX", "");
        data.set("REASON", "自动注销");
        data.set("ADDTIME", DateUtil.getCurrtentDateTime());
        data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
        //添加邮寄信息
        data.set("SFJBYJ", "1");
        data.set("YJR", kydjxx.getStr("FDDBR"));
        data.set("YJRDH", kydjxx.getStr("LXDH"));
        data.set("YJRDZ", kydjxx.getStr("ZS"));
        return data.save();
    }

    /**
     * 药品业务注销
     * @param sblsh
     * @param tableNameCode
     * @param kydjxx
     */
    private boolean ypSync(String sblsh, String tableNameCode, YwSgsxzgljKydjxx kydjxx, SpyxmpApplyItem spyxmpApplyItem){

        ResultModel resultModel = CommonUtil.getLicense("C1080", kydjxx.getStr("QYMC"), null);
        ResultModel gspResultModel = CommonUtil.getLicense("GSP", kydjxx.getStr("QYMC"), null);


        //如果没有获取到证件数据，则记录到数据库
        if (resultModel.getCode() == GlobalErrorCode.SUCCESS.getCode() && resultModel.getData() != null
            && gspResultModel.getCode() == GlobalErrorCode.SUCCESS.getCode() && gspResultModel.getData() != null) {

            spyxmpApplyItem.save();
            JSONObject licenseData = (JSONObject) resultModel.getData();
            JSONObject gspLicenseData = (JSONObject) resultModel.getData();


            SpyxmpYpjyxkz data = new SpyxmpYpjyxkz();
            data.set("ID", StringUtils.getUUID());
            data.set("APPLYID", sblsh);
            data.set("TYPE", "2");
            data.set("XKZBH", licenseData.getString("证号"));
            data.set("GFRZSBH", gspLicenseData.getString("证书编号"));
            data.set("SQRQ", DateUtil.getCurrtentDateTime());
            data.set("BZ", "自动注销");
            data.set("ADDTIME", DateUtil.getCurrtentDateTime());
            data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
            //添加邮寄信息
            data.set("SFJBYJ", "1");
            data.set("YJR", kydjxx.getStr("FDDBR"));
            data.set("YJRDH", kydjxx.getStr("LXDH"));
            data.set("YJRDZ", kydjxx.getStr("ZS"));
            data.save();
            System.out.println("企业名称:【" + kydjxx.getStr("QYMC") + "】药品经营许可证已同步");
            return true;
        }
        System.out.println("企业名称:【" + kydjxx.getStr("QYMC") + "】药品经营许可证未同步，没有查找到证照数据");
        return false;
    }

    /**
     * 食品经营许可证注销
     * @param sblsh
     * @param tableNameCode
     * @param kydjxx
     */
    private boolean spjyxkzSync(String sblsh, String tableNameCode, YwSgsxzgljKydjxx kydjxx, SpyxmpApplyItem spyxmpApplyItem){

        ResultModel resultModel = CommonUtil.getLicense("C1020", kydjxx.getStr("QYMC"), null);

        //如果没有获取到证件数据，则记录到数据库
        if (resultModel.getCode() == GlobalErrorCode.SUCCESS.getCode() && resultModel.getData() != null) {

            spyxmpApplyItem.save();
            JSONObject licenseData = (JSONObject) resultModel.getData();
            String jyzmc = licenseData.getString("经营者名称");

            SpyxmpSpjyxkz data = new SpyxmpSpjyxkz();
            data.set("ID", StringUtils.getUUID());
            data.set("APPLYID", sblsh);
            data.set("TYPE", "2");
            data.set("SPJYXKZH", licenseData.getString("许可证编号"));
            data.set("LXR", kydjxx.getStr("FDDBR"));
            data.set("LXRDH", kydjxx.getStr("LXDH"));
            data.set("REASON", "系统自动注销");
            data.set("ADDTIME", DateUtil.getCurrtentDateTime());
            data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
            //添加邮寄信息
            data.set("SFJBYJ", "1");
            data.set("YJR", kydjxx.getStr("FDDBR"));
            data.set("YJRDH", kydjxx.getStr("LXDH"));
            data.set("YJRDZ", kydjxx.getStr("ZS"));
            data.set("JYZMC", jyzmc);
            data.save();

            //生成证件信息
            Calendar nowCalendar = Calendar.getInstance();
            String name = jyzmc;
            String zi = "2018";
            String hao;
            String year = String.valueOf(nowCalendar.get(Calendar.YEAR));
            String month = String.valueOf(nowCalendar.get(Calendar.MONTH) + 1);
            String day = String.valueOf(nowCalendar.get(Calendar.DATE));

            int typeCodeInt = 2;
            String typeCode = ConvertCodeUtil.getOrderCode(typeCodeInt);
            String datePrefix = new DateTime().getDateTimeNYR();
            StringBuffer sblshStb = new StringBuffer(datePrefix);
            sblshStb.append(tableNameCode).append(typeCode);
            int code = SpyxmpApplyItem.me.findLikeSblshData(sblshStb.toString()).size() + 1;
            String haoCode;
            if (code > 99) {
                haoCode = "" + code;
            } else {
                haoCode = "0" + code;
            }
            hao = "18" + month + day + haoCode;

            ZxjdsPdfModel pdfModel = new ZxjdsPdfModel(name, zi, hao, year, month, day);
            //正本路径
            String zxjdsPath = LicensePdfUtil.generateSpjyxkzZxjds(pdfModel);


            WxWsbsFile licenseZb = new WxWsbsFile();
            licenseZb.set("ID", StringUtils.getUUID());
            licenseZb.set("SBLSH", sblsh);
            licenseZb.set("USERID", "developer");
            licenseZb.set("FILEPATH", zxjdsPath);
            licenseZb.set("FILENAME", "注销决定书.png");
            licenseZb.set("CONTENTTYPE", "image/png");
            licenseZb.set("TABLENAME", "SPJYXKZ_ZXJDS");
            licenseZb.set("ADDTIME", new DateTime().getTimestamp());
            licenseZb.set("ADDUSER", "developer");
            licenseZb.set("UPDATETIME", new DateTime().getTimestamp());
            licenseZb.set("UPDATEUSER", "developer");
            licenseZb.set("DATASTATE", "I");

            licenseZb.save();

            System.out.println("企业名称:【" + kydjxx.getStr("QYMC") + "】食品经营许可证已同步");
            return true;
        }
        System.out.println("企业名称:【" + kydjxx.getStr("QYMC") + "】食品经营许可证未同步，没有查找到证照数据");
        return false;
    }

    public static void main(String[] args) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();

        //过去七天
        c.setTime(new Date());
        c.add(Calendar.DATE, -7);
        Date d = c.getTime();
        String day = format.format(d);
        System.out.println("过去七天：" + day);


        SpyxmpZdzx zdzxData = new SpyxmpZdzx();
        zdzxData.set("ID", UUID.randomUUID());
        zdzxData.set("QYMC", "1231");
        zdzxData.set("TYSHXYDM", "1231");
        zdzxData.set("ISSYNCRONZZ", "0");
        zdzxData.set("CREATE_DATE", DateUtil.getCurrtentDateTime());
        zdzxData.set("MODIFY_DATE", DateUtil.getCurrtentDateTime());
        zdzxData.save();
    }
}
