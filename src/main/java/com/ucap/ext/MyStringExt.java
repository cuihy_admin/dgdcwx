package com.ucap.ext;

import java.text.SimpleDateFormat;

public class MyStringExt {
    public boolean contains(String self, String str) {
        return self.contains(str);
    }

    public String toDate(String str) {
        SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(time.format(str));
        return time.format(str);
    }
}
