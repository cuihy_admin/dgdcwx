package com.ucap.ext;

import com.jfinal.plugin.activerecord.dialect.OracleDialect;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class CompatibleOracleDialect extends OracleDialect {

    @Override
    public void fillStatement(PreparedStatement pst, List<Object> paras) throws SQLException {
        for (int i=0, size=paras.size(); i<size; i++) {
            Object value = paras.get(i);
            if (value instanceof java.sql.Date) {
                pst.setDate(i + 1, (java.sql.Date)value);
            } else if (value instanceof java.sql.Timestamp) {
                pst.setTimestamp(i + 1, (java.sql.Timestamp)value);
            } else if (value instanceof java.util.Date) {
                Date date = (Date)value;
                pst.setTimestamp(i + 1, new Timestamp(date.getTime()));
            } else {
                pst.setObject(i + 1, value);
            }
        }
    }

    @Override
    public void fillStatement(PreparedStatement pst, Object... paras) throws SQLException {
        for (int i=0; i<paras.length; i++) {
            Object value = paras[i];
            if (value instanceof java.sql.Date) {
                pst.setDate(i + 1, (java.sql.Date)value);
            } else if (value instanceof java.sql.Timestamp) {
                pst.setTimestamp(i + 1, (java.sql.Timestamp)value);
            } else if (value instanceof java.util.Date) {
                Date date = (Date)value;
                pst.setTimestamp(i + 1, new Timestamp(date.getTime()));
            } else {
                pst.setObject(i + 1, value);
            }
        }
    }
}
