package com.ucap.ext;

import java.text.SimpleDateFormat;

import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.ucap.util.DateTime;

public class DateDirective extends Directive {
	public void exec(Env env, Scope scope, Writer writer) {

		Object value = exprList.eval(scope);
		if (value != null) {
		DateTime date=new DateTime();
			try {
				value=	value.toString().substring(0,10);
			} catch (Exception e) {
				e.printStackTrace();
			}
		write(writer, value.toString());
		}
	}
	
}
