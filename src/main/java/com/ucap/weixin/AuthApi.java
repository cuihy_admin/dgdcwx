package com.ucap.weixin;

import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.kit.ParaMap;
import com.jfinal.weixin.sdk.utils.HttpUtils;

/**
 * Created by Administrator on 2018/6/13.
 */
public class AuthApi {
    private static String getUserInfo = "https://iauth-sandbox.wecity.qq.com/new/cgi-bin/getdetectinfo.php?token=TOKEN&uid=UID";

    /**
     * 获取用户个人信
     * @param token 调用凭证token
     * @param uid 普通用户的标识，对当前开发者帐号唯一
     * @return ApiResult
     */
    public static ApiResult getDetectInfo(String token, String uid)
    {
        ParaMap pm = ParaMap.create("TOKEN", token).put("UID", uid).put("lang", "zh_CN");
        return new ApiResult(HttpUtils.get(getUserInfo, pm.getData()));
    }


}
