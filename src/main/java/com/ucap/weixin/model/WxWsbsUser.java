package com.ucap.weixin.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.ucap.util.DateTime;
import com.ucap.util.StringUtils;

/**
 * 授权获取到的用户信息
 * @author Javen
 */
public class WxWsbsUser extends Model<WxWsbsUser> {

	private static final long serialVersionUID = 6204222383226990020L;
	
	static Log log = Log.getLog(WxWsbsUser.class);
	
	public static final WxWsbsUser me = new WxWsbsUser();
	
	public boolean save(String openId,String nickName,String unionid,String headimgurl,String country,String city,String province ,int sex){
		
		log.error("openId:"+openId+" nickName:"+nickName+" unionid:"+unionid+ " headimgurl:"+headimgurl+" country:"+country+" city:"+city+" province:"+province+" sex:"+sex);
		
		/**
		 * 1、判断openId 是否存在 
		 *    如果存在就update
		 *    如果不存在就保存
		 */
		WxWsbsUser user = findByOpenId(openId);
		if (user!=null) {
			user.set("NICKNAME", nickName);
			user.set("UNIONID", unionid);
			user.set("HEADIMGURL", headimgurl);
			user.set("COUNTRY", country);
			user.set("CITY", city);
			user.set("PROVINCE", province);
			user.set("SEX", sex);
			user.set("UPDATETIME", new DateTime().getTimestamp());
			return user.update();
		}else {
			if (StrKit.notBlank(openId)) {
				WxWsbsUser me = new WxWsbsUser();
				me.set("ID", StringUtils.getUUID());
				me.set("OPENID", openId);
				me.set("NICKNAME", nickName);
				me.set("UNIONID", unionid);
				me.set("HEADIMGURL", headimgurl);
				me.set("COUNTRY", country);
				me.set("CITY", city);
				me.set("PROVINCE", province);
				me.set("SEX", sex);
				me.set("UPDATETIME", new DateTime().getTimestamp());
				return me.save();
			}
		}
		return false;
	}
	
	public List<WxWsbsUser> getAll(){
		return me.find("select * from WX_WSBS_USER");
	}

	/**
	 * 所有 sql 与业务逻辑写在 Model 或 Service 中，不要写在 Controller 中，养成好习惯，有利于大型项目的开发与维护
	 */
	public Page<WxWsbsUser> paginate(int pageNumber, int pageSize) {
		return paginate(pageNumber, pageSize, "select *", "from WX_WSBS_USER order by id asc");
	}
	
	public WxWsbsUser findByOpenId(String openId){
		return this.findFirst("select * from WX_WSBS_USER where OPENID=?", openId);
		/*
		LANGUAGE CITY PROVINCE COUNTRY HEADIMGURL MOBILE  IDCARD NAME SUBSCRIBETIME UNSUBSCRIBETIME NICKNAME UNIONID UPDATETIME REMARK SUBSCRIBE
		* */
	}

	/**
	 * 根据map参数查找
	 * @param paras
	 * @return 
	 */
	public List<WxWsbsUser> findByMap(Map<String, Object> paras) {
		StringBuilder sql = new StringBuilder("select * from WX_WSBS_USER ");
		if (paras.containsKey("order")) {
			sql.append(" ORDER BY ");
			sql.append(paras.get("order"));
			sql.append(" ");
		}
		if (paras.containsKey("limit")) {
			sql.append(" LIMIT ");
			sql.append(paras.get("limit"));
		}
		return this.find(sql.toString());
	}

}
