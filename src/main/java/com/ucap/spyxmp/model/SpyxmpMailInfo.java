package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpMailInfo extends Model<SpyxmpMailInfo> {
    static Log log = Log.getLog(SpyxmpMailInfo.class);

    public static final SpyxmpMailInfo me = new SpyxmpMailInfo();

    public List<SpyxmpMailInfo> findByUserId(String userId) {
        return this.find("select * from SPYXMP_MAIL_INFO where userId = ?", userId);
    }
}
