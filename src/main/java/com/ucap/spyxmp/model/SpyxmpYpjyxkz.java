package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpYpjyxkz extends Model<SpyxmpYpjyxkz> {
    static Log log = Log.getLog(SpyxmpYpjyxkz.class);

    public static final SpyxmpYpjyxkz me = new SpyxmpYpjyxkz();

    public SpyxmpYpjyxkz findByApplyId(String applyId) {
        return this.findFirst("select * from SPYXMP_YPJYXKZ where APPLYID=?", applyId);
    }
}
