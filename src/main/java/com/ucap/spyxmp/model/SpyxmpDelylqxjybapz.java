package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpDelylqxjybapz extends Model<SpyxmpDelylqxjybapz> {
    static Log log = Log.getLog(SpyxmpDelylqxjybapz.class);

    public static final SpyxmpDelylqxjybapz me = new SpyxmpDelylqxjybapz();

    public SpyxmpDelylqxjybapz findByApplyId(String applyId) {
        return this.findFirst("select * from SPYXMP_DELYLQXJYBAPZ where APPLYID=?", applyId);
    }
}
