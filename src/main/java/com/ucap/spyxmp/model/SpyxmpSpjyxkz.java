package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpSpjyxkz extends Model<SpyxmpSpjyxkz> {
    static Log log = Log.getLog(SpyxmpSpjyxkz.class);

    public static final SpyxmpSpjyxkz me = new SpyxmpSpjyxkz();

    public SpyxmpSpjyxkz findByApplyId(String applyId) {
        return this.findFirst("select * from SPYXMP_SPJYXKZ where APPLYID=?", applyId);
    }
}
