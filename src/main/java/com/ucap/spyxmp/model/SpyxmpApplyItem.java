package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpApplyItem extends Model<SpyxmpApplyItem> {
    static Log log = Log.getLog(SpyxmpApplyItem.class);

    public static final SpyxmpApplyItem me = new SpyxmpApplyItem();

    public List<SpyxmpApplyItem> findAll() {
        return this.find("select * from SPYXMP_APPLY_ITEM where MODIFYDATE >= to_date('2018-12-29','yyyy-mm-dd')");
    }

    public List<SpyxmpApplyItem> findLikeSblshData(String str) {
        return this.find("select * from SPYXMP_APPLY_ITEM where SBLSH like ?", "%" + str + "%");
    }

    /**
     * 根据用户ID获取申请数据
     * @param userId 用户ID
     * @return
     */
    public List<SpyxmpApplyItem> findByUserId(String userId) {
        return this.find("select * from SPYXMP_APPLY_ITEM where USERID=? order by ADDTIME DESC", userId);
    }

    /**
     * 根据用户ID和数据状态获取申请数据
     * @param userId 用户ID
     * @param status 数据状态
     * @return
     */
    public List<SpyxmpApplyItem> findByUserIdAndStatus(String userId, String status) {
        List<SpyxmpApplyItem> list = this.find("select * from SPYXMP_APPLY_ITEM where USERID=? and status=?", userId, status);
        return list;
    }

    public SpyxmpApplyItem findBySblsh(String wsbsSblsh) {
        return this.findFirst("select * from SPYXMP_APPLY_ITEM where SBLSH=?", wsbsSblsh);
    }
}
