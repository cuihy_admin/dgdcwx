package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpYlqxjyxkz extends Model<SpyxmpYlqxjyxkz> {
    static Log log = Log.getLog(SpyxmpYlqxjyxkz.class);

    public static final SpyxmpYlqxjyxkz me = new SpyxmpYlqxjyxkz();

    public SpyxmpYlqxjyxkz findByApplyId(String applyId) {
        return this.findFirst("select * from SPYXMP_YLQXJYXKZ where APPLYID=?", applyId);
    }
}
