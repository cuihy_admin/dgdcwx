package com.ucap.spyxmp.model;

import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;

/**
 * Created by Administrator on 2018/5/9.
 */
public class SpyxmpZdzx extends Model<SpyxmpZdzx> {
    static Log log = Log.getLog(SpyxmpZdzx.class);

    public static final SpyxmpZdzx me = new SpyxmpZdzx();

    public SpyxmpZdzx findByApplyId(String qymc) {
        return this.findFirst("select * from SPYXMP_ZDZX where QYMC=?", qymc);
    }
}
