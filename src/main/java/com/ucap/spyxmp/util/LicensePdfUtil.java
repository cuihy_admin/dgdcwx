package com.ucap.spyxmp.util;

import com.alibaba.fastjson.JSONObject;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.ucap.spyxmp.data.SpjyxkzPdfModel;
import com.ucap.spyxmp.data.YpgsprzPdfModel;
import com.ucap.spyxmp.data.YpjyxkzPdfModel;
import com.ucap.spyxmp.data.ZxjdsPdfModel;
import com.ucap.util.DateUtil;
import com.ucap.util.FileUtil;
import com.ucap.util.PDF2IMAGE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class LicensePdfUtil {

    /**
     * 食品经营许可证正本模板
     */
    private static final String SPJYXKZ_PDF_ZB_PATH = "/files/template/spjyxkz/zb.pdf";
    /**
     * 食品经营许可证副本模板
     */
    private static final String SPJYXKZ_PDF_FB_PATH = "/files/template/spjyxkz/fb.pdf";
    /**
     * 食品经营许可证注销决定书模板
     */
    private static final String SPJYXKZ_PDF_ZXJDS_PATH = "/files/template/spjyxkz/zxjds.pdf";
    /**
     * 药品经营许可证正本模板
     */
    private static final String YPJYXKZ_PDF_ZB_PATH = "/files/template/ypjyxkz/zb.pdf";
    /**
     * 药品经营许可证副本模板
     */
    private static final String YPJYXKZ_PDF_FB_PATH = "/files/template/ypjyxkz/fb.pdf";
    /**
     * 药品经营质量管理规范认证证书模板
     */
    private static final String YPGSPRZZS_PDF_FB_PATH = "/files/template/ypjyxkz/gsp.pdf";
    /**
     * 生成文件路径
     */
    private static final String GENERATE_LICENSE_PATH = "/files/license/";

    private static final String pdfSuffix = ".pdf";
    private static final String imgSuffix = ".png";


    /**
     * 生成药品经营质量管理规范认证证书PDF
     * @param data
     * @return
     */
    public static String generateYpGspRzzs(YpgsprzPdfModel data){

        String templatePath = PathKit.getWebRootPath() + YPGSPRZZS_PDF_FB_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "ypjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "gsp_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);

        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;

        return imagePath;

    }


    /**
     * 生成药品经营许可证正本PDF
     * @param data
     * @return
     */
    public static String generateYpjyxkzZb(YpjyxkzPdfModel data){

        String templatePath = PathKit.getWebRootPath() + YPJYXKZ_PDF_ZB_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "ypjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "zb_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);

        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;

        return imagePath;

    }

    /**
     * 生成药品经营许可证副本PDF
     * @param data
     * @return
     */
    public static String generateYpjyxkzFb(YpjyxkzPdfModel data){

        String templatePath = PathKit.getWebRootPath() + YPJYXKZ_PDF_FB_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "ypjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "fb_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);

        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;
        return imagePath;

    }


    /**
     * 生成食品经营许可证注销决定书PDF
     * @param data
     * @return
     */
    public static String generateSpjyxkzZxjds(ZxjdsPdfModel data){

        String templatePath = PathKit.getWebRootPath() + SPJYXKZ_PDF_ZXJDS_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "spjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "zxjds_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);

        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;

        return imagePath;

    }


    /**
     * 生成食品经营许可证正本PDF
     * @param data
     * @return
     */
    public static String generateSpjyxkzZb(SpjyxkzPdfModel data){

        String templatePath = PathKit.getWebRootPath() + SPJYXKZ_PDF_ZB_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "spjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "zb_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);

        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;

        return imagePath;

    }

    /**
     * 生成食品经营许可证副本PDF
     * @param data
     * @return
     */
    public static String generateSpjyxkzFb(SpjyxkzPdfModel data){

        String templatePath = PathKit.getWebRootPath() + SPJYXKZ_PDF_FB_PATH;
        String newPdfPrefix = GENERATE_LICENSE_PATH + "spjyxkz/";
        String newPdfFoldPath = PathKit.getWebRootPath()+ newPdfPrefix;
        String pdfFileName = "fb_" + DateUtil.getSdfTimes();
        JSONObject dataJson = JSONObject.parseObject(JSONObject.toJSONString(data));
        generatePdf(dataJson, templatePath, newPdfFoldPath, pdfFileName + pdfSuffix);
        String imagePath = newPdfPrefix + pdfFileName + imgSuffix;
        return imagePath;

    }

    private static void generatePdf(JSONObject dataJson, String templatePath, String newPdfFoldPath, String pdfFileName){

        PdfReader reader;
        FileOutputStream out;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        String newPDFPath = newPdfFoldPath + pdfFileName;
        try {
            FileUtil.copyFile(templatePath, newPDFPath);

            out = new FileOutputStream(newPDFPath);//输出流
            reader = new PdfReader(templatePath);//读取pdf模板
            bos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, bos);
            AcroFields form = stamper.getAcroFields();
            //使用中文字体
            BaseFont bf = BaseFont.createFont(PathKit.getWebRootPath() + "/fonts/simsun.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            ArrayList<BaseFont> fontList = new ArrayList<>();
            fontList.add(bf);
            form.setSubstitutionFonts(fontList);
            int i = 0;
            java.util.Iterator<String> it = form.getFields().keySet().iterator();
            while(it.hasNext()){
                String name = it.next();
                form.setField(name, dataJson.getString(name));
            }
            stamper.setFormFlattening(true);//如果为false那么生成的PDF文件还能编辑，一定要设为true
            stamper.close();

            Document doc = new Document();
            PdfCopy copy = new PdfCopy(doc, out);
            doc.open();
            PdfImportedPage importPage = copy.getImportedPage(
                    new PdfReader(bos.toByteArray()), 1);
            copy.addPage(importPage);
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        PDF2IMAGE.pdf2Image(newPDFPath, newPdfFoldPath, 100);

        //删除生成的PDF文件
        if(PropKit.getBoolean("delPdfLicense")){
            File newPdf = new File(newPDFPath);
            if(newPdf.exists()){
                newPdf.delete();
            }
        }

    }
}
