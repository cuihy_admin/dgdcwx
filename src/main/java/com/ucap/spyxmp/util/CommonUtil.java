package com.ucap.spyxmp.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ucap.spyxmp.data.RestResultBuilder;
import com.ucap.spyxmp.data.ResultModel;
import com.ucap.util.HttpClientUtil;
import com.ucap.util.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonUtil {

    public static String LICENSE_DATA = "syj_license_data";
    public static String LICENSE_DATA_GSP = "syj_license_gsp_data";

    public static ResultModel getLicense(String classifyId, String qymc, String tyshxydm){

        if(StringUtils.isBlank(qymc) && StringUtils.isBlank(tyshxydm)){

            return null;
        }
        String url="http://zwfw.dg.gov.cn/igsp/pub/fda/getXkzInfo";
        //String url="http://127.0.0.1/igsp/pub/fda/getXkzInfo";
        System.out.println("获取许可证详细信息:" + url);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("classifyId", classifyId));
        if(StringUtils.isNotBlank(qymc)){
            params.add(new BasicNameValuePair("qymc", qymc));
        }else{
            params.add(new BasicNameValuePair("tyshxydm", tyshxydm));
        }
        String result = HttpClientUtil.httpPost(url, params);
        JSONObject r= JSON.parseObject(result);
        System.out.println("=========================================返回结果");
        System.out.println(r.toJSONString());
        System.out.println("=========================================返回结果");
        if("1".equals(r.getString("code"))){

            JSONObject resultData = r.getJSONObject("data");

            if(resultData != null){

                return getLicenseInfo(classifyId, resultData.getString("certSeq"));
            }else{
                return RestResultBuilder.builder().success().build();
            }

        }else{
            return RestResultBuilder.builder().failure().build();
        }
    }

    public static ResultModel getLicenseInfo(String classifyId, String certSeq){
        String url="http://zwfw.dg.gov.cn/igsp/pub/fda/getLicense";
        //String url="http://127.0.0.1/igsp/pub/fda/getLicense";
        System.out.println("获取许可证详细信息:" + url);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("classifyId", classifyId));
        params.add(new BasicNameValuePair("certSeq", certSeq));
        String result = HttpClientUtil.httpPost(url, params);
        JSONObject r= JSON.parseObject(result);
        System.out.println("=========================================返回结果");
        System.out.println(r.toJSONString());
        System.out.println("=========================================返回结果");

        if("1".equals(r.getString("code"))){

            JSONObject resultData = r.getJSONObject("data");
            return RestResultBuilder.builder().success().data(resultData).build();

        }else{
            return RestResultBuilder.builder().failure().build();
        }

    }

    public static String convertStep(String tableName, String type) throws Exception{

        Map tableNameMap = new HashMap(){{
            put("SPYXMP_SPJYXKZ", "");
            put("SPYXMP_YPJYXKZ", "");
            put("SPYXMP_DELYLQXJYBAPZ", "");
            put("SPYXMP_YLQXJYXKZ", "");
        }};
        if(tableName == null){
            throw new Exception("tableName 不能为空");
        }else if(!tableNameMap.containsKey(tableName)){
            throw new Exception("tableName 参数非法");
        }

        if(type == null){
            throw new Exception("type 不能为空");
        }

        String step = "";

        switch (tableName){

            case "SPYXMP_SPJYXKZ":
                switch (type){
                    case "1":
                        step = "1";
                        break;
                    case "2":
                        step = "2";
                        break;
                    case "3":
                        step = "3";
                        break;
                    default:
                        step = "4";
                        break;
                }
                break;
            case "SPYXMP_YPJYXKZ":
                switch (type){
                    case "1":
                        step = "5";
                        break;
                    default:
                        step = "6";
                        break;
                }
                break;
            case "SPYXMP_DELYLQXJYBAPZ":
                switch (type){
                    case "1":
                        step = "7";
                        break;
                    default:
                        step = "8";
                        break;
                }
                break;
            default:
                switch (type){
                    case "1":
                        step = "9";
                        break;
                    default:
                        step = "10";
                        break;
                }
                break;
        }

        return step;
    }

    public static ResultModel getLicenseData(String classifyId, String certNo){
        String url="http://zwfw.dg.gov.cn/igsp/pub/fda/getXkzInfo";
        //String url="http://127.0.0.1/igsp/pub/fda/getXkzInfo";
        System.out.println("获取许可证信息:" + url);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("classifyId", classifyId));
        params.add(new BasicNameValuePair("certNo", certNo));
        String result = HttpClientUtil.httpPost(url, params);
        JSONObject r= JSON.parseObject(result);
        System.out.println("=========================================返回结果");
        System.out.println(r.toJSONString());
        System.out.println("=========================================返回结果");

        if("1".equals(r.getString("code"))){

            JSONObject resultData = r.getJSONObject("data");

            if(resultData != null){

                return getLicenseInfo(classifyId, resultData.getString("certSeq"));
            }else{
                return RestResultBuilder.builder().success().build();
            }


        }else{
            return RestResultBuilder.builder().failure().build();
        }
    }

    public static String toConceal(String value) {
        int SIZE = 6;
        String SYMBOL = "*";
        if (null == value || "".equals(value)) {
            return value;
        }
        int len = value.length();
        int pamaone = len / 2;
        int pamatwo = pamaone - 1;
        int pamathree = len % 2;
        StringBuilder stringBuilder = new StringBuilder();
        if (len <= 2) {
            if (pamathree == 1) {
                return SYMBOL;
            }
            stringBuilder.append(SYMBOL);
            stringBuilder.append(value.charAt(len - 1));
        } else {
            if (pamatwo <= 0) {
                stringBuilder.append(value.substring(0, 1));
                stringBuilder.append(SYMBOL);
                stringBuilder.append(value.substring(len - 1, len));

            } else if (pamatwo >= SIZE / 2 && SIZE + 1 != len) {
                int pamafive = (len - SIZE) / 2;
                stringBuilder.append(value.substring(0, pamafive));
                for (int i = 0; i < SIZE; i++) {
                    stringBuilder.append(SYMBOL);
                }
                if ((pamathree == 0 && SIZE / 2 == 0) || (pamathree != 0 && SIZE % 2 != 0)) {
                    stringBuilder.append(value.substring(len - pamafive, len));
                } else {
                    stringBuilder.append(value.substring(len - (pamafive + 1), len));
                }
            } else {
                int pamafour = len - 2;
                stringBuilder.append(value.substring(0, 1));
                for (int i = 0; i < pamafour; i++) {
                    stringBuilder.append(SYMBOL);
                }
                stringBuilder.append(value.substring(len - 1, len));
            }
        }
        return stringBuilder.toString();

    }
}
