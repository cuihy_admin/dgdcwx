package com.ucap.spyxmp.enums;

/**
 * 证件名称枚举
 * 证件名称
 */
public enum TableNameEnum {

    //食品经营许可证
    SPJYXKZ("SPYXMP_SPJYXKZ", "食品经营许可证")

    //药品经营许可证
    ,YPJYXKZ("SPYXMP_YPJYXKZ", "药品经营许可证")

	//第二类医疗器械经营备案凭证
	,DELYLQXJYBAPZ("SPYXMP_DELYLQXJYBAPZ", "第二类医疗器械经营备案凭证")

	//医疗器械经营许可证
	,YLQXJYXKZ("SPYXMP_YLQXJYXKZ", "医疗器械经营许可证")
    ;

	private final String key;
	private final String value;

	TableNameEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
