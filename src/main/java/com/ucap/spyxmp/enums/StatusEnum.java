package com.ucap.spyxmp.enums;

/**
 * 材料复用库附件来源枚举
 * 附件来源（1：网上申报材料复用 2：现场申报材料复用）
 */
public enum StatusEnum {

    //提交网上申请
    CG("0", "草稿")

    //网上预审通过
    ,YTJ("1", "已提交")

    ;

	private final String key;
	private final String value;

	StatusEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
