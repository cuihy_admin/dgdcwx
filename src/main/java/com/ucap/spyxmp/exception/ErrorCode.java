package com.ucap.spyxmp.exception;

/**
 * 错误码定义接口
 */
public interface ErrorCode {
    int getCode();
    String getMessage();
}
