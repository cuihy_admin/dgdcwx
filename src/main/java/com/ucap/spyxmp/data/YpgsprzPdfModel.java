package com.ucap.spyxmp.data;

import com.ucap.util.DateUtil;

import java.util.Date;

/**
 * 食品GSP认证证书PDF生成 model
 */
public class YpgsprzPdfModel {

    //===============正本

    /**
     * 药品经营许可证号
     */
    private String zsbh;
    /**
     * 企业名称
     */
    private String qymc;
    /**
     * 地址
     */
    private String dz;
    /**
     * 法定代表人
     */
    private String rzfw;
    /**
     * 有效期年
     */
    private String yxqn;
    /**
     * 有效期月
     */
    private String yxqy;
    /**
     * 有效期日
     */
    private String yxqr;
    /**
     * 发证机关
     */
    private String fzjg;
    /**
     * 签发日期年
     */
    private String qfrqn;
    /**
     * 签发日期月
     */
    private String qfrqy;
    /**
     * 签发日期日
     */
    private String qfrqr;

    public Date getYxq(){

        String yxq = yxqn + "-" + yxqy + "-" + yxqr;

        return DateUtil.fomatDate(yxq);
    }

    public Date getQfrq(){

        String qfrq = qfrqn + "-" + qfrqy + "-" + qfrqr;

        return DateUtil.fomatDate(qfrq);
    }

    public YpgsprzPdfModel() {
    }

    /**
     *
     * @param zsbh 证书编号
     * @param qymc 企业名称
     * @param dz 地址
     * @param rzfw 认证范围
     * @param yxqn 有效期年
     * @param yxqy 有效期月
     * @param yxqr 有效期日
     * @param fzjg 发证机关
     * @param qfrqn 签发日期年
     * @param qfrqy 签发日期月
     * @param qfrqr 签发日期日
     */
    public YpgsprzPdfModel(String zsbh, String qymc, String dz, String rzfw, String yxqn, String yxqy, String yxqr, String fzjg, String qfrqn, String qfrqy, String qfrqr) {
        this.zsbh = zsbh;
        this.qymc = qymc;
        this.dz = dz;
        this.rzfw = rzfw;
        this.yxqn = yxqn;
        this.yxqy = yxqy;
        this.yxqr = yxqr;
        this.fzjg = fzjg;
        this.qfrqn = qfrqn;
        this.qfrqy = qfrqy;
        this.qfrqr = qfrqr;
    }

    public String getZsbh() {
        return zsbh;
    }

    public void setZsbh(String zsbh) {
        this.zsbh = zsbh;
    }

    public String getQymc() {
        return qymc;
    }

    public void setQymc(String qymc) {
        this.qymc = qymc;
    }

    public String getDz() {
        return dz;
    }

    public void setDz(String dz) {
        this.dz = dz;
    }

    public String getRzfw() {
        return rzfw;
    }

    public void setRzfw(String rzfw) {
        this.rzfw = rzfw;
    }

    public String getYxqn() {
        return yxqn;
    }

    public void setYxqn(String yxqn) {
        this.yxqn = yxqn;
    }

    public String getYxqy() {
        return yxqy;
    }

    public void setYxqy(String yxqy) {
        this.yxqy = yxqy;
    }

    public String getYxqr() {
        return yxqr;
    }

    public void setYxqr(String yxqr) {
        this.yxqr = yxqr;
    }

    public String getFzjg() {
        return fzjg;
    }

    public void setFzjg(String fzjg) {
        this.fzjg = fzjg;
    }

    public String getQfrqn() {
        return qfrqn;
    }

    public void setQfrqn(String qfrqn) {
        this.qfrqn = qfrqn;
    }

    public String getQfrqy() {
        return qfrqy;
    }

    public void setQfrqy(String qfrqy) {
        this.qfrqy = qfrqy;
    }

    public String getQfrqr() {
        return qfrqr;
    }

    public void setQfrqr(String qfrqr) {
        this.qfrqr = qfrqr;
    }
}
