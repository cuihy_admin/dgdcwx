package com.ucap.spyxmp.data;


/**
 * 分页统一返回结果集
 */
public class PageRestResult<T> extends ResultModel<T> {

    private int start;
    private int limit;
    private int total;
    private int totalCount;

    public PageRestResult() { super(); }

    public PageRestResult(int code, String message) {
        super(code, message);
    }

    public PageRestResult(int code, String message, T data) {
        super(code, message, data);
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
