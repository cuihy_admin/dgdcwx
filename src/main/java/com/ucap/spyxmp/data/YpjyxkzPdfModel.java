package com.ucap.spyxmp.data;

import com.ucap.util.DateUtil;

import java.util.Date;

/**
 * 食品经营许可证PDF生成 model
 */
public class YpjyxkzPdfModel {

    //===============正本

    /**
     * 药品经营许可证号
     */
    private String ypjyxkzh;
    /**
     * 企业名称
     */
    private String qymc;
    /**
     * 注册地址
     */
    private String zcdz;
    /**
     * 法定代表人
     */
    private String fddbr;
    /**
     * 企业负责人
     */
    private String qyfzr;
    /**
     * 质量负责人
     */
    private String zlfzr;
    /**
     * 仓库地址
     */
    private String ckdz;
    /**
     * 经营范围
     */
    private String jyfw;
    /**
     * 有效期年
     */
    private String yxqn;
    /**
     * 有效期月
     */
    private String yxqy;
    /**
     * 有效期日
     */
    private String yxqr;
    /**
     * 发证机关
     */
    private String fzjg;
    /**
     * 签发日期年
     */
    private String qfrqn;
    /**
     * 签发日期月
     */
    private String qfrqy;
    /**
     * 签发日期日
     */
    private String qfrqr;

    public Date getYxq(){

        String yxq = yxqn + "-" + yxqy + "-" + yxqr;

        return DateUtil.fomatDate(yxq);
    }

    public Date getQfrq(){

        String qfrq = qfrqn + "-" + qfrqy + "-" + qfrqr;

        return DateUtil.fomatDate(qfrq);
    }

    public YpjyxkzPdfModel() {
    }

    /**
     *
     * @param ypjyxkzh 药品经营许可证号
     * @param qymc 企业名称
     * @param zcdz 注册地址
     * @param fddbr 法定代表人
     * @param qyfzr 企业负责人
     * @param zlfzr 质量负责人
     * @param ckdz 仓库地址
     * @param jyfw 经营范围
     * @param yxqn 有效期年
     * @param yxqy 有效期月
     * @param yxqr 有效期日
     * @param fzjg 发证机关
     * @param qfrqn 签发日期年
     * @param qfrqy 签发日期月
     * @param qfrqr 签发日期日
     */
    public YpjyxkzPdfModel(String ypjyxkzh, String qymc, String zcdz, String fddbr, String qyfzr, String zlfzr, String ckdz, String jyfw, String yxqn, String yxqy, String yxqr, String fzjg, String qfrqn, String qfrqy, String qfrqr) {
        this.ypjyxkzh = ypjyxkzh;
        this.qymc = qymc;
        this.zcdz = zcdz;
        this.fddbr = fddbr;
        this.qyfzr = qyfzr;
        this.zlfzr = zlfzr;
        this.ckdz = ckdz;
        this.jyfw = jyfw;
        this.yxqn = yxqn;
        this.yxqy = yxqy;
        this.yxqr = yxqr;
        this.fzjg = fzjg;
        this.qfrqn = qfrqn;
        this.qfrqy = qfrqy;
        this.qfrqr = qfrqr;
    }

    public String getYpjyxkzh() {
        return ypjyxkzh;
    }

    public void setYpjyxkzh(String ypjyxkzh) {
        this.ypjyxkzh = ypjyxkzh;
    }

    public String getQymc() {
        return qymc;
    }

    public void setQymc(String qymc) {
        this.qymc = qymc;
    }

    public String getZcdz() {
        return zcdz;
    }

    public void setZcdz(String zcdz) {
        this.zcdz = zcdz;
    }

    public String getFddbr() {
        return fddbr;
    }

    public void setFddbr(String fddbr) {
        this.fddbr = fddbr;
    }

    public String getQyfzr() {
        return qyfzr;
    }

    public void setQyfzr(String qyfzr) {
        this.qyfzr = qyfzr;
    }

    public String getZlfzr() {
        return zlfzr;
    }

    public void setZlfzr(String zlfzr) {
        this.zlfzr = zlfzr;
    }

    public String getCkdz() {
        return ckdz;
    }

    public void setCkdz(String ckdz) {
        this.ckdz = ckdz;
    }

    public String getJyfw() {
        return jyfw;
    }

    public void setJyfw(String jyfw) {
        this.jyfw = jyfw;
    }

    public String getYxqn() {
        return yxqn;
    }

    public void setYxqn(String yxqn) {
        this.yxqn = yxqn;
    }

    public String getYxqy() {
        return yxqy;
    }

    public void setYxqy(String yxqy) {
        this.yxqy = yxqy;
    }

    public String getYxqr() {
        return yxqr;
    }

    public void setYxqr(String yxqr) {
        this.yxqr = yxqr;
    }

    public String getFzjg() {
        return fzjg;
    }

    public void setFzjg(String fzjg) {
        this.fzjg = fzjg;
    }

    public String getQfrqn() {
        return qfrqn;
    }

    public void setQfrqn(String qfrqn) {
        this.qfrqn = qfrqn;
    }

    public String getQfrqy() {
        return qfrqy;
    }

    public void setQfrqy(String qfrqy) {
        this.qfrqy = qfrqy;
    }

    public String getQfrqr() {
        return qfrqr;
    }

    public void setQfrqr(String qfrqr) {
        this.qfrqr = qfrqr;
    }
}
