package com.ucap.spyxmp.data;

import com.ucap.util.DateUtil;

import java.util.Date;

/**
 * 食品经营许可证PDF生成 model
 */
public class SpjyxkzPdfModel {

    //===============正本

    /**
     * 经营者名称
     */
    private String jyzmc;
    /**
     * 统一社会信用代码
     */
    private String tyshxydm;
    /**
     * 法定代表人
     */
    private String fddbr;
    /**
     * 住所
     */
    private String zs;
    /**
     * 经营场所
     */
    private String jycs;
    /**
     * 主体业态
     */
    private String ztyt;
    /**
     * 经营项目
     */
    private String jyxm;
    /**
     * 有效期年
     */
    private String yxqn;
    /**
     * 有效期月
     */
    private String yxqy;
    /**
     * 有效期日
     */
    private String yxqr;
    /**
     * 许可证编号
     */
    private String xkzbh;
    /**
     * 日常监督管理机构
     */
    private String rcjdgljg;
    /**
     * 日常监督管理人员
     */
    private String rcjdglry;
    /**
     * 发证机关
     */
    private String fzjg;
    /**
     * 签发人
     */
    private String qfr;
    /**
     * 签发日期年
     */
    private String qfrqn;
    /**
     * 签发日期月
     */
    private String qfrqy;
    /**
     * 签发日期日
     */
    private String qfrqr;

    public Date getYxq(){

        String yxq = yxqn + "-" + yxqy + "-" + yxqr;

        return DateUtil.fomatDate(yxq);
    }

    public Date getQfrq(){

        String qfrq = qfrqn + "-" + qfrqy + "-" + qfrqr;

        return DateUtil.fomatDate(qfrq);
    }

    public SpjyxkzPdfModel() {
    }

    /**
     *
     * @param jyzmc 经营者名称
     * @param tyshxydm 统一社会信用代码
     * @param fddbr 法定代表人
     * @param zs 住所
     * @param jycs 经营场所
     * @param ztyt 主体业态
     * @param jyxm 经营项目
     * @param yxqn 有效期年
     * @param yxqy 有效期月
     * @param yxqr 有效期日
     * @param xkzbh 许可证编号
     * @param rcjdgljg 日常监督管理机构
     * @param rcjdglry 日常监督管理人员
     * @param fzjg 发证机关
     * @param qfr 签发人
     * @param qfrqn 签发日期年
     * @param qfrqy 签发日期月
     * @param qfrqr 签发日期日
     */
    public SpjyxkzPdfModel(String jyzmc, String tyshxydm, String fddbr, String zs, String jycs, String ztyt, String jyxm, String yxqn, String yxqy, String yxqr, String xkzbh, String rcjdgljg, String rcjdglry, String fzjg, String qfr, String qfrqn, String qfrqy, String qfrqr) {
        this.jyzmc = jyzmc;
        this.tyshxydm = tyshxydm;
        this.fddbr = fddbr;
        this.zs = zs;
        this.jycs = jycs;
        this.ztyt = ztyt;
        this.jyxm = jyxm;
        this.yxqn = yxqn;
        this.yxqy = yxqy;
        this.yxqr = yxqr;
        this.xkzbh = xkzbh;
        this.rcjdgljg = rcjdgljg;
        this.rcjdglry = rcjdglry;
        this.fzjg = fzjg;
        this.qfr = qfr;
        this.qfrqn = qfrqn;
        this.qfrqy = qfrqy;
        this.qfrqr = qfrqr;
    }

    public String getJyzmc() {
        return jyzmc;
    }

    public void setJyzmc(String jyzmc) {
        this.jyzmc = jyzmc;
    }

    public String getTyshxydm() {
        return tyshxydm;
    }

    public void setTyshxydm(String tyshxydm) {
        this.tyshxydm = tyshxydm;
    }

    public String getFddbr() {
        return fddbr;
    }

    public void setFddbr(String fddbr) {
        this.fddbr = fddbr;
    }

    public String getZs() {
        return zs;
    }

    public void setZs(String zs) {
        this.zs = zs;
    }

    public String getJycs() {
        return jycs;
    }

    public void setJycs(String jycs) {
        this.jycs = jycs;
    }

    public String getZtyt() {
        return ztyt;
    }

    public void setZtyt(String ztyt) {
        this.ztyt = ztyt;
    }

    public String getJyxm() {
        return jyxm;
    }

    public void setJyxm(String jyxm) {
        this.jyxm = jyxm;
    }

    public String getYxqn() {
        return yxqn;
    }

    public void setYxqn(String yxqn) {
        this.yxqn = yxqn;
    }

    public String getYxqy() {
        return yxqy;
    }

    public void setYxqy(String yxqy) {
        this.yxqy = yxqy;
    }

    public String getYxqr() {
        return yxqr;
    }

    public void setYxqr(String yxqr) {
        this.yxqr = yxqr;
    }

    public String getXkzbh() {
        return xkzbh;
    }

    public void setXkzbh(String xkzbh) {
        this.xkzbh = xkzbh;
    }

    public String getRcjdgljg() {
        return rcjdgljg;
    }

    public void setRcjdgljg(String rcjdgljg) {
        this.rcjdgljg = rcjdgljg;
    }

    public String getRcjdglry() {
        return rcjdglry;
    }

    public void setRcjdglry(String rcjdglry) {
        this.rcjdglry = rcjdglry;
    }

    public String getFzjg() {
        return fzjg;
    }

    public void setFzjg(String fzjg) {
        this.fzjg = fzjg;
    }

    public String getQfr() {
        return qfr;
    }

    public void setQfr(String qfr) {
        this.qfr = qfr;
    }

    public String getQfrqn() {
        return qfrqn;
    }

    public void setQfrqn(String qfrqn) {
        this.qfrqn = qfrqn;
    }

    public String getQfrqy() {
        return qfrqy;
    }

    public void setQfrqy(String qfrqy) {
        this.qfrqy = qfrqy;
    }

    public String getQfrqr() {
        return qfrqr;
    }

    public void setQfrqr(String qfrqr) {
        this.qfrqr = qfrqr;
    }
}
