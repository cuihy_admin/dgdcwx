package com.ucap.spyxmp.data;

import com.sun.org.apache.xerces.internal.impl.dv.xs.YearDV;
import com.ucap.util.DateUtil;

import java.util.Date;

/**
 * 食品经营许可证注销决定书PDF生成 model
 */
public class ZxjdsPdfModel {

    //===============正本

    /**
     * 药品经营许可证号
     */
    private String name;
    /**
     * 企业名称
     */
    private String zi;
    /**
     * 地址
     */
    private String hao;
    /**
     * 法定代表人
     */
    private String year;
    /**
     * 有效期年
     */
    private String month;
    /**
     * 有效期月
     */
    private String day;

    public Date getDate(){

        String yxq = year + "-" + month + "-" + day;

        return DateUtil.fomatDate(yxq);
    }

    public ZxjdsPdfModel() {
    }

    /**
     *
     * @param name 名称或姓名
     * @param zi 字
     * @param hao 号
     * @param year 年
     * @param month 月
     * @param day 日
     */
    public ZxjdsPdfModel(String name, String zi, String hao, String year, String month, String day) {
        this.name = name;
        this.zi = zi;
        this.hao = hao;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZi() {
        return zi;
    }

    public void setZi(String zi) {
        this.zi = zi;
    }

    public String getHao() {
        return hao;
    }

    public void setHao(String hao) {
        this.hao = hao;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
