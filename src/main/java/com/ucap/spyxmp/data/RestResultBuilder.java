package com.ucap.spyxmp.data;


import com.ucap.spyxmp.exception.ErrorCode;
import com.ucap.spyxmp.exception.GlobalErrorCode;

/**
 * 分页查询返回值封装类
 */
public class RestResultBuilder<T>  {
	
	protected int code;

    protected String message;

    protected Boolean success;

    protected T data;

	
    public static RestResultBuilder builder() {
        RestResultBuilder restResultBuilder = new RestResultBuilder();
        return restResultBuilder;
    }

    public RestResultBuilder code(int code) {
        this.code = code;
        return this;
    }

    public RestResultBuilder message(String message) {
        this.message = message;
        this.code = GlobalErrorCode.FAILURE.getCode();
        return this;
    }

    public RestResultBuilder data(T data) {
        this.data = data;
        return this;
    }

    public RestResultBuilder errorCode(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
        return this;
    }

    public RestResultBuilder success() {
        this.code = GlobalErrorCode.SUCCESS.getCode();
        this.message = GlobalErrorCode.SUCCESS.getMessage();
        this.success = true;
        return this;
    }

    public RestResultBuilder success(T data) {
        this.code = GlobalErrorCode.SUCCESS.getCode();
        this.message = GlobalErrorCode.SUCCESS.getMessage();
        this.success = true;
        this.data = data;
        return this;
    }

    public RestResultBuilder failure() {
        this.code = GlobalErrorCode.FAILURE.getCode();
        this.message = GlobalErrorCode.FAILURE.getMessage();
        this.success = false;
        return this;
    }
    public RestResultBuilder noData() {
    	this.code = GlobalErrorCode.NO_DATA.getCode();
        this.message = GlobalErrorCode.NO_DATA.getMessage();
        this.success = false;
        return this;
    }
    public RestResultBuilder noData(T data) {
    	this.code = GlobalErrorCode.NO_DATA.getCode();
        this.message = GlobalErrorCode.NO_DATA.getMessage();
        this.success = false;
        this.data = data;
        return this;
    }
    public RestResultBuilder noLogin() {
        ErrorCode errorCode = GlobalErrorCode.NO_LOGIN;
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
        this.success = false;
        return this;
    }
    public RestResultBuilder failure(T data) {
        this.code = GlobalErrorCode.FAILURE.getCode();
        this.message = GlobalErrorCode.FAILURE.getMessage();
        this.data = data;
        this.success = false;
        return this;
    }

    public RestResultBuilder result(boolean successful) {
        if (successful) {
            return this.success();
        } else {
            return this.failure();
        }
    }

    public RestResultBuilder success(Boolean result) {
        if (result == Boolean.TRUE) {
            success();
        } else {
            failure();
        }
        return this;
    }

    public ResultModel<T> build() {
        return new ResultModel<T>(this.code, this.message, this.success, this.data);
    }

    public ResultModel build(ResultModel restResult) {
        return restResult;
    }
    
}
