package com.ucap.spyxmp.data;

/**
 * @author lemon 2017-9-30 下午3:29:17
 * @Description TODO(数据模板)
 */
public class Data {

    private int pageIndex = 1;// 当前页

    private int pageSize = 10;// 每页显示记录数

    private int pageTotal;// 本页记录数

    private long total;// 总记录数

    private int count;// 总页数

    private String sort = null;// 排序字段名

    private String order = null;// 排序(asc,desc)

    private Object obj = null;// 其他信息

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getCount() {
        if (total <= 0) {
            total = 0;
        }
        if (pageSize <= 0) {
            pageSize = 10;
        }
        this.count = (int) ((total + pageSize - 1) / pageSize);
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Data() {
        super();
    }
    public Data(int pageIndex, int pageSize) {
        super();
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public Data(int pageIndex, int pageSize, int pageTotal, long total, int count, String sort, String order, Object obj) {
        super();
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.pageTotal = pageTotal;
        this.total = total;
        this.count = count;
        this.sort = sort;
        this.order = order;
        this.obj = obj;
    }

    public Data(Object obj) {
        super();
        this.obj = obj;
    }

	@Override
	public String toString() {
		return "Data [pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", pageTotal=" + pageTotal + ", total="
				+ total + ", count=" + count + ", sort=" + sort + ", order=" + order + ", obj=" + obj + "]";
	}

}
