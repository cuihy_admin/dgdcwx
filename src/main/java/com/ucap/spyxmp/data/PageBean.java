package com.ucap.spyxmp.data;

import java.util.List;

/**
 * 分页查询封装类（输入+输出）
 */
public class PageBean {
    /****************** 输入 *******************/
	/** 起始记录 */
	private int start;
	/** 每页记录数 */
	private int limit;

    /****************** 输出 *******************/
    /** 查询结果 */
    private List<? extends Object> rows;
    /** 当前页数 */
    private int currentPage;
    /** 总页数 */
    private int totalPage;
    /** 总数量 */
    private int totalCount;

	public int getLimit()
	{
		return limit;
	}

	public void setLimit(int limit)
	{
		this.limit = limit;
	}

	public int getStart()
	{
		return start;
	}

	public void setStart(int start)
	{
		this.start = start;
	}

	public List<? extends Object> getRows() {
		return rows;
	}

	public void setRows(List<? extends Object> rows) {
		this.rows = rows;
	}
 

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

 
	
	
	
	

}
