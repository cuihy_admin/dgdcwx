package com.ucap.spyxmp.controller;


import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.ucap.spyxmp.data.RestResultBuilder;
import com.ucap.spyxmp.model.SpyxmpApplyItem;
import com.ucap.spyxmp.model.SpyxmpMailInfo;
import com.ucap.spyxmp.model.SpyxmpYlqxjyxkz;
import com.ucap.util.DateUtil;
import com.ucap.util.StringUtils;
import com.ucap.wsbs.model.*;

import java.util.List;

/**
 * 食品经营许可证controller
 */
public class YlqxjyxkzController extends SpyxmpBaseController {

    private static String tableName = "SPYXMP_YLQXJYXKZ";
    public String retPath;

    @Before(Tx.class)
    public void save() throws Exception{

        SpyxmpApplyItem applyItem = this.getApplyItemCg();
        String sblsh = applyItem.getStr("SBLSH");
        System.out.println(sblsh);

        //保存申请主体信息
        applyItem.set("STATUS", "1");
        applyItem.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
        applyItem.update();

        List<WxWsbsFile> wsbsFiles = WxWsbsFile.me.findFileListByTableNameAndUserId("SIGNATURE", this.getUserId());
        if(wsbsFiles.size() == 0){
            throw new Exception("未获取到签名数据");
        }
        WxWsbsFile.me.updateSblsh(sblsh, "SIGNATURE", this.getUserId());

        //保存申请表信息
        switch (getType()){
            case "1":
                this.saveBz(sblsh);
                break;
            default:
                this.saveZx(sblsh);
                break;
        }
        renderJson(RestResultBuilder.builder().success(applyItem).build());
    }

    /**
     * 补正数据保存
     * @param sblsh
     * @throws Exception
     */
    private void saveBz(String sblsh) throws Exception{

        List<WxWsbsFile> wsbsFiles = WxWsbsFile.me.findFileListByTableNameAndUserId(tableName, this.getUserId());
        if(wsbsFiles.size() == 0){
            throw new Exception("请上传遗失声明");
        }
        WxWsbsFile.me.updateSblsh(sblsh, tableName, this.getUserId());

        SpyxmpYlqxjyxkz data = new SpyxmpYlqxjyxkz();
        data.set("ID", StringUtils.getUUID());
        data.set("APPLYID", sblsh);
        data.set("TYPE", getType());
        data.set("QYMC", getPara("qymc"));
        data.set("XKZBH", getPara("xkzbh"));
        data.set("SQRQ", DateUtil.getCurrtentDateTime());
        data.set("FZRQ", getParaToDate("fzrq"));
        data.set("TYSHXYDM", getPara("tyshxydm"));
        data.set("YXQX", getParaToDate("yxqx"));
        data.set("FDDBR", getPara("fddbr"));
        data.set("QYFZR", getPara("qyfzr"));
        data.set("JYFS", getPara("jyfs"));
        data.set("JYMS", getPara("jyms"));
        data.set("ZS", getPara("zs"));
        data.set("JYCS", getPara("jycs"));
        data.set("KFDZ", getPara("kfdz"));
        data.set("JYFW", getPara("jyfw"));
        data.set("LXRXM", getPara("lxrxm"));
        data.set("LXRZJHM", getPara("lxrzjhm"));
        data.set("LXRDH", getPara("lxrdh"));
        data.set("LXRCZ", getPara("lxrcz"));
        data.set("LXRYX", getPara("lxryx"));
        data.set("REASON", getPara("reason"));
        data.set("ADDTIME", DateUtil.getCurrtentDateTime());
        data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
        data.save();
    }

    /**
     * 注销数据保存
     * @param sblsh
     * @throws Exception
     */
    private void saveZx(String sblsh) throws Exception{


        String sfjbyj = getPara("sfjbyj");
        if("1".equals(sfjbyj)){
            //具备原件，填写邮寄地址
            SpyxmpMailInfo mailInfo = null;
            List<SpyxmpMailInfo> mailInfos = SpyxmpMailInfo.me.findByUserId(getUserId());
            if(mailInfos.size() > 0){
                mailInfo = mailInfos.get(0);
                mailInfo.set("NAME", getPara("yjr"));
                mailInfo.set("MOBILE", getPara("yjrdh"));
                mailInfo.set("ADDRESS", getPara("yjrdz"));
                mailInfo.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
                mailInfo.update();
            }else{
                mailInfo = new SpyxmpMailInfo();
                mailInfo.set("ID", StringUtils.getUUID());
                mailInfo.set("USERID", getUserId());
                mailInfo.set("NAME", getPara("yjr"));
                mailInfo.set("MOBILE", getPara("yjrdh"));
                mailInfo.set("ADDRESS", getPara("yjrdz"));
                mailInfo.set("ADDTIME", DateUtil.getCurrtentDateTime());
                mailInfo.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
                mailInfo.save();
            }
        }else{
            //上传遗失证明
            List<WxWsbsFile> wsbsFiles = WxWsbsFile.me.findFileListByTableNameAndUserId(tableName, this.getUserId());
            if(wsbsFiles.size() == 0){
                throw new Exception("请上传遗失证明");
            }
            WxWsbsFile.me.updateSblsh(sblsh, tableName, this.getUserId());
        }

        SpyxmpYlqxjyxkz data = new SpyxmpYlqxjyxkz();
        data.set("ID", StringUtils.getUUID());
        data.set("APPLYID", sblsh);
        data.set("TYPE", getType());
        data.set("QYMC", getPara("qymc"));
        data.set("XKZBH", getPara("xkzbh"));
        data.set("SQRQ", getParaToDate("sqrq"));
        data.set("FZRQ", getParaToDate("fzrq"));
        data.set("TYSHXYDM", getPara("tyshxydm"));
        data.set("YXQX", getPara("yxqx"));
        data.set("FDDBR", getPara("fddbr"));
        data.set("QYFZR", getPara("qyfzr"));
        data.set("JYFS", getPara("jyfs"));
        data.set("JYMS", getPara("jyms"));
        data.set("ZS", getPara("zs"));
        data.set("JYCS", getPara("jycs"));
        data.set("KFDZ", getPara("kfdz"));
        data.set("JYFW", getPara("jyfw"));
        data.set("LXRXM", getPara("lxrxm"));
        data.set("LXRZJHM", getPara("lxrzjhm"));
        data.set("LXRDH", getPara("lxrdh"));
        data.set("LXRCZ", getPara("lxrcz"));
        data.set("LXRYX", getPara("lxryx"));
        data.set("REASON", getPara("reason"));
        data.set("ADDTIME", DateUtil.getCurrtentDateTime());
        data.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
        //添加邮寄信息
        data.set("SFJBYJ", sfjbyj);
        data.set("YJR", getPara("yjr"));
        data.set("YJRDH", getPara("yjrdh"));
        data.set("YJRDZ", getPara("yjrdz"));
        data.save();
    }

    public void getData(){

    }

    public YlqxjyxkzController() {
        super(tableName);
    }
}
