package com.ucap.spyxmp.controller;

import com.jfinal.core.Controller;
import com.ucap.spyxmp.data.RestResultBuilder;
import com.ucap.spyxmp.model.SpyxmpMailInfo;
import com.ucap.util.Conts;
import com.ucap.weixin.model.WxWsbsUser;

import java.util.List;

/**
 * 食品经营许可证controller
 */
public class MailInfoController extends Controller {

    public void getData(){

        WxWsbsUser user = Conts.getSessionUser(getSession());

        if(user != null){

            List<SpyxmpMailInfo> dataList = SpyxmpMailInfo.me.findByUserId(user.getStr("ID"));

            if(dataList.size() > 0){

                renderJson(RestResultBuilder.builder().success().data(dataList.get(0)).build());
            }else{

                renderJson(RestResultBuilder.builder().success().build());
            }

        }else{

            renderJson(RestResultBuilder.builder().failure().message("未登录").build());
        }

    }
}
