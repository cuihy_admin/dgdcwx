package com.ucap.spyxmp.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.CustomServiceApi;
import com.jfinal.weixin.sdk.jfinal.ApiController;
import com.ucap.util.Conts;
import com.ucap.util.DateTime;
import com.ucap.util.DateUtil;
import com.ucap.util.StringUtils;
import com.ucap.weixin.model.WxWsbsUser;
import com.ucap.spyxmp.enums.StatusEnum;
import com.ucap.spyxmp.model.SpyxmpApplyItem;
import com.ucap.wsbs.util.ConvertCodeUtil;

import java.util.List;

public abstract class SpyxmpBaseController extends ApiController {
    private String tableName;

    public SpyxmpBaseController(String tableName) {
        this.tableName = tableName;
    }

    public String getUserId() throws Exception{
        return getUser().getStr("id");
    }

    public WxWsbsUser getUser() throws Exception{
        return Conts.getSessionUser(getSession());
    }

    public String getType() throws Exception{

        String type = getPara("type");
        if(StringUtils.isEmpty(type)){
            throw new NullPointerException("数据类型为空，无法初始化申报流水号");
        }
        String vstr = "1234";
        if(!vstr.contains(type)){
            throw new Exception("请传入正确的数据类型值");
        }
        return type;
    }

    private String getTypeCode() throws Exception{

        int code = Integer.valueOf(getType());
        return ConvertCodeUtil.getOrderCode(code);
    }

    public String getTableNameCode() throws Exception{

        if(StringUtils.isEmpty(tableName)){
            throw new NullPointerException("数据表格类型为空，无法初始化申报流水号");
        }

        JSONObject tableNameJson = new JSONObject();
        tableNameJson.put("SPYXMP_SPJYXKZ", 1);
        tableNameJson.put("SPYXMP_YPJYXKZ", 2);
        tableNameJson.put("SPYXMP_DELYLQXJYBAPZ", 3);
        tableNameJson.put("SPYXMP_YLQXJYXKZ", 4);

        int code = tableNameJson.getIntValue(tableName);
        return ConvertCodeUtil.getOrderCode(code);
    }

    public String getSblsh() throws Exception{
        String datePrefix = new DateTime().getDateTimeNYR();
        StringBuffer sblshStb = new StringBuffer(datePrefix);
        sblshStb.append(getTableNameCode()).append(getTypeCode());
        int code = SpyxmpApplyItem.me.findLikeSblshData(sblshStb.toString()).size() + 1;
        sblshStb.append(ConvertCodeUtil.getOrderCode(code));
        return sblshStb.toString();
    }

    //获取草稿申请数据
    public SpyxmpApplyItem getApplyItemCg() throws Exception {

        WxWsbsUser user = Conts.getSessionUser(getSession());
        List<SpyxmpApplyItem> spyxmpApplyItemList = SpyxmpApplyItem.me.findByUserIdAndStatus(user.getStr("id"), StatusEnum.CG.getKey());
        if(spyxmpApplyItemList.size() > 0){
            return spyxmpApplyItemList.get(0);
        }else{
            SpyxmpApplyItem spyxmpApplyItem = new SpyxmpApplyItem();
            spyxmpApplyItem.set("id", StringUtils.getUUID());
            spyxmpApplyItem.set("userid", user.getStr("id"));
            spyxmpApplyItem.set("status", StatusEnum.CG.getKey());
            spyxmpApplyItem.set("sblsh", this.getSblsh());
            spyxmpApplyItem.set("tablename", tableName);
            spyxmpApplyItem.set("type", getPara("type"));
            spyxmpApplyItem.set("ADDTIME", DateUtil.getCurrtentDateTime());
            spyxmpApplyItem.set("MODIFYDATE", DateUtil.getCurrtentDateTime());
            Boolean flag = spyxmpApplyItem.save();
            if(!flag){
                throw new Exception("添加草稿数据失败");
            }
            return spyxmpApplyItem;
        }
    }

    //获取草稿申请数据
    public List<SpyxmpApplyItem> getApplyItemList() {

        WxWsbsUser user = Conts.getSessionUser(getSession());
        List<SpyxmpApplyItem> spyxmpApplyItemList = SpyxmpApplyItem.me.findByUserId(user.getStr("id"));
        return spyxmpApplyItemList;

    }

    /**
     * 发送提交申请信息
     * @return
     */
    public ApiResult sendTextMsg() throws Exception {
        WxWsbsUser user = Conts.getSessionUser(getSession());

        StringBuffer msgStb = new StringBuffer();

        String tableCode = getTableNameCode();

        String tableNameJsonStr = "{\"001\": \"食品经营许可证\",\"002\": \"药品经营许可证\",\"003\": \"第二类医疗器械经营备案凭证\",\"004\": \"医疗器械经营许可证\"}";

        JSONObject tableNameJson = JSONObject.parseObject(tableNameJsonStr);

        String typeCode = getTypeCode();

        String typeNameJsonStr = "{\"001\": \"补发\",\"002\": \"注销\",\"003\": \"变更\",\"004\": \"续期\"}";

        JSONObject typeNameJson = JSONObject.parseObject(typeNameJsonStr);

        msgStb.append("您好，您申请的");
        msgStb.append(tableNameJson.getString(tableCode));
        msgStb.append(typeNameJson.getString(typeCode));
        msgStb.append("业务已成功办理！");
        ApiResult apiResult = CustomServiceApi.sendText(user.getStr("OPENID"), msgStb.toString());
        return apiResult;
    }
}
