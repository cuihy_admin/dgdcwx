package com.ucap.spyxmp.controller;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.ucap.spyxmp.model.SpyxmpApplyItem;
import com.ucap.spyxmp.util.CommonUtil;
import com.ucap.util.Conts;
import com.ucap.weixin.model.WxWsbsUser;

import java.util.List;

public class SpyxmpApplyItemController extends Controller {

    public void toSbls() throws Exception{

        WxWsbsUser user =  Conts.getSessionUser(getSession());

        if(user == null){
            throw new Exception("用户未登录");
        }

        List<SpyxmpApplyItem> dataList = SpyxmpApplyItem.me.findByUserId(user.getStr("ID"));

        setAttr("sbxx", dataList);

        render("/WEB-INF/wfw/spyxmp/sbls.html");
    }


    public void toFillHist() throws Exception {
        String tableName = getPara("tableName");
        String type = getPara("type");
        String sblsh = getPara("sblsh");
        String step = CommonUtil.convertStep(tableName, type);
        setAttr("data", Db.findFirst("select * from " + tableName + " where APPLYID = ?", sblsh));
        setAttr("model", "readonly");
        switch (step) {
            case "1":
                render("/WEB-INF/wfw/spyxmp/spjyxkz/bzxx.html");
                break;
            case "2":
                render("/WEB-INF/wfw/spyxmp/spjyxkz/zxxx.html");
                break;
            case "3":
                render("/WEB-INF/wfw/spyxmp/spjyxkz/bgxx.html");
                break;
            case "4":
                render("/WEB-INF/wfw/spyxmp/spjyxkz/yxxx.html");
                break;
            case "5":
                render("/WEB-INF/wfw/spyxmp/ypjyxkz/bzxx.html");
                break;
            case "6":
                render("/WEB-INF/wfw/spyxmp/ypjyxkz/zxxx.html");
                break;
            case "7":
                render("/WEB-INF/wfw/spyxmp/delylqxjybapz/bzxx.html");
                break;
            case "8":
                render("/WEB-INF/wfw/spyxmp/delylqxjybapz/zxxx.html");
                break;
            case "9":
                render("/WEB-INF/wfw/spyxmp/ylqxjyxkz/bzxx.html");
                break;
            case "10":
                render("/WEB-INF/wfw/spyxmp/ylqxjyxkz/zxxx.html");
                break;
            default:
                render("/WEB-INF/wfw/wsbs/navigation.html");
                break;
        }
    }
}
