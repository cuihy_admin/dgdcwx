package com.ucap.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 说明：日期处理
 * 创建人：cuihy
 * 修改时间：2015年11月24日
 * @version
 */
public class DateUtil {

	public final static String df_year = "yyyy";
	public final static String df_month = "yyyy-MM";
	public final static String df_months = "yyyyMM";
	public final static String df_day = "yyyy-MM-dd";
	public final static String df_days = "yyyyMMdd";
	public final static String df_time = "yyyy-MM-dd HH:mm:ss";
	public final static String df_times = "yyyyMMddHHmmss";
	public final static String df_msecs = "yyyy-MM-dd HH:mm:ss.SSS";
	private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");
	private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final static SimpleDateFormat sdfTimes = new SimpleDateFormat("yyyyMMddHHmmss");

	/**
	 * 得到服务器系统年-天-小时-秒
	 * @return String
	 */
	public static String getServerSysDateAndTimeAsCode() {
		String result = null;
		Date currentDate = null;
		SimpleDateFormat df = null;
		String yearAndMonth = null;
		// 年-天-小时-秒
		df = new SimpleDateFormat("yyyyMMddHHmmssSS");
		currentDate = new Date();
		yearAndMonth = df.format(currentDate);
		result = yearAndMonth;
		return result;
	}

	/**
	 * 获取YYYY格式
	 * @return
	 */
	public static String getSdfTimes() {
		return sdfTimes.format(new Date());
	}

	/**
	 * 获取YYYY格式
	 * @return
	 */
	public static String getYear() {
		return sdfYear.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD格式
	 * @return
	 */
	public static String getDay() {
		return sdfDay.format(new Date());
	}

	/**
	 * 获取YYYYMMDD格式
	 * @return
	 */
	public static String getDays(){
		return sdfDays.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD HH:mm:ss格式
	 * @return
	 */
	public static String getTime() {
		return sdfTime.format(new Date());
	}

	/**
	* @Title: compareDate
	* @Description: TODO(日期比较，如果s>=e 返回true 否则返回false)
	* @param s
	* @param e
	* @return boolean
	* @throws
	* @author fh
	 */
	public static boolean compareDate(String s, String e) {
		if(fomatDate(s)==null||fomatDate(e)==null){
			return false;
		}
		return fomatDate(s).getTime() >=fomatDate(e).getTime();
	}

	/**
	 * 格式化日期
	 * @return
	 */
	public static Date fomatDate(String date) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 格式化日期
	 * @return
	 */
	public static String fomatDateYMD(String date) {
	Date formatDate=formatDate(date);
		return format(formatDate,"yyyy年MM月dd日");
	}
	public static Date getDateTime(){
		return fomatDate(getTime());
	}

	/**
	 * 校验日期是否合法
	 * @return
	 */
	public static boolean isValidDate(String s) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fmt.parse(s);
			return true;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return false;
		}
	}

	/**
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static int getDiffYear(String startTime, String endTime) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			//long aa=0;
			int years=(int) (((fmt.parse(endTime).getTime()-fmt.parse(startTime).getTime())/ (1000 * 60 * 60 * 24))/365);
			return years;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return 0;
		}
	}

	/**
     * <li>功能描述：时间相减得到天数
     * @param beginDateStr
     * @param endDateStr
     * @return
     * long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr, String endDateStr){
        long day=0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;

            try {
				beginDate = format.parse(beginDateStr);
				endDate= format.parse(endDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
            day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
            //System.out.println("相隔的天数="+day);

        return day;
    }

    /**
     * 得到n天之后的日期
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
    	int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后是周几
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
    	int daysInt = Integer.parseInt(days);
        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);
        return dateStr;
    }

    public static Date formatDate(String dateStr) {
		Date date = null;
		List<String> formatStrs = new ArrayList<String>();
		formatStrs.add(df_msecs);
		formatStrs.add(df_time);
		formatStrs.add(df_day);
		formatStrs.add(df_times);
		formatStrs.add(df_days);
		formatStrs.add(df_month);
		formatStrs.add(df_months);
		formatStrs.add(df_year);

		for (String formatStr : formatStrs) {
			SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
			sdf.setLenient(false);
			try {
				date = sdf.parse(dateStr);
				break;
			} catch (ParseException e) {
				//e.printStackTrace();
				continue;
			}
		}
    	return date;
	}

    /**
	 * 将日期时间转换成指定格式的字符串
	 *
	 * @param date
	 *            日期时间
	 * @param format
	 *            日期时间格式
	 * @return 字符串
	 */
	public static String format(Date date, String format) {
		if (date == null) {
			return null;
		}
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date).toString();
	}


	/**
	 * 将当前时间转换成指定格式的字符串
	 *
	 * @param format
	 *            日期时间格式
	 * @return 字符串
	 */
	public static String format(String format) {
		return format(new Date(), format);
	}


	public static Object dateToString(Date value) {
		return sdfDays.format(value);
	}

	public static Date getCurrtentDateTime() {
		String curDate = format(new Date(), df_time);
		try {
			return sdfTime.parse(curDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	public static String getWeekOfDate(Date dt) {
		String[] weekDays = {"日", "一", "二", "三", "四", "五", "六"};
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}

	public static void main(String[] args){
		String dateStr = DateUtil.getAfterDayDate(1 + "");
		Date afterDay = DateUtil.fomatDate(dateStr);


		System.out.println(DateUtil.getWeekOfDate(afterDay));

		System.out.println(DateUtil.getCurrtentDateTime());
	}

}
