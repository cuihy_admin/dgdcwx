package com.ucap.util;

import com.ucap.weixin.model.WxWsbsUser;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2018/5/14.
 */
public class Conts {
    /**
     * 添加数据状态
     */
    public final static String DATA_STATE_I="I";
    /**
     * 修改数据状态
     */
    public final static String DATA_STATE_U="U";
    /**
     * 删除数据状态
     */
    public final static String DATA_STATE_D="D";
    /**
     * 申报流水号
     */
    public final static String WSBS_SBLSH="sblsh";
    /**
     * 微信用户
     */
    public final  static String WX_USER ="wxUser";
    /**
     * 获取微信用户
     * @param session
     * @return 微信用户
     */
    public static WxWsbsUser getSessionUser(HttpSession session) {
        return (WxWsbsUser)session.getAttribute(WX_USER);
    }

    public static String getWsbsSblsh(HttpSession session) {
        return (String) session.getAttribute(WSBS_SBLSH);
    }
}
