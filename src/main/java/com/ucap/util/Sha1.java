package com.ucap.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * 内包含sha1和HmacSHA1
 * @author DerrickZheng
 * @version 1.0
 *
 */
public class Sha1 {

	private static final String HMAC_SHA1 = "HmacSHA1";

	/**
	 * HmacSHA1 加密
	 * @author DerrickZheng
	 *
	 */
	public static byte[] getHashHmac_Sha1(String data, String key) throws Exception {
		Mac mac = Mac.getInstance(HMAC_SHA1);
		SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(),
				mac.getAlgorithm());
		mac.init(signingKey);

		return mac.doFinal(data.getBytes());
	}
}
