package com.ucap.util;

/**
 * Created by Administrator on 2018/5/24.
 */
public class StringFormat {
    static final String[] NATION={"汉族","壮族","满族","回族","苗族","维吾尔族","土家族","彝族","蒙古族","藏族","布依族","侗族","瑶族","朝鲜族","白族","哈尼族","哈萨克族","黎族","傣族","畲族","傈僳族","仡佬族","东乡族","高山族","拉祜族","水族","佤族","纳西族","羌族","土族","仫佬族","锡伯族","柯尔克孜族","达斡尔族","景颇族","毛南族","撒拉族","布朗族","塔吉克族","阿昌族","普米族","鄂温克族","怒族","京族","基诺族","德昂族","保安族","俄罗斯族","裕固族","乌兹别克族","门巴族","鄂伦春族","独龙族","塔塔尔族","赫哲族","珞巴族"};
    public static String parseSex(String sex) {
        if (sex != null && "1".equals(sex))
            return "男";
        else if (sex != null && "2".equals(sex))
            return "女";
        else
            return null;
    }

    public static String parseNation(String nation) {
        if(nation!=null&&!"0".equals(nation)){
            return NATION[Integer.valueOf(nation)-1];
        }
        return null;
    }

    public static String parseIdtype(String idtype) {
        if (idtype != null && "1".equals(idtype))
            return "身份证";
        else if (idtype != null && "2".equals(idtype))
            return "港澳通行证";
        else
            return null;
    }

    public static String  parseNull(Object str) {
        if(str==null){
            return"";
        }else{
            return str.toString();
        }
    }
}
