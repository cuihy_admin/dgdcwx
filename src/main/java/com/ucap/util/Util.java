package com.ucap.util;

/**
 * 工具
 * @author DerrickZheng
 * 
 */
public class Util {
		
	/**
	 * 生成Unix的时间戳 (不含毫秒)
	 * @author DerrickZheng
	 */
	public static Long getCurrentTime() {
        //毫秒时间转成分钟
        double doubleTime = (Math.floor(System.currentTimeMillis() / 60000L));
        //往下取整 1.9=> 1.0
        long floorValue = new Double(doubleTime).longValue();
        return floorValue * 60;
    }
	
	
}
