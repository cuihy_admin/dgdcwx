package com.ucap.util;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * 下载文件
 * 创建人：cuihy
 * 创建时间：2014年12月23日
 * @version
 */
public class FileDownload {

	/**
	 * @param response 
	 * @param filePath		//文件完整路径(包括文件名和扩展名)
	 * @param fileName		//下载后看到的文件名
	 * @return  文件名
	 */
	public static void fileDownload(final HttpServletResponse response, String filePath, String fileName) throws Exception{  
		   
		byte[] data = FileUtil.toByteArray2(filePath);  
	    fileName = URLEncoder.encode(fileName, "UTF-8");  
	    response.reset();  
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");  
	    response.addHeader("Content-Length", "" + data.length);  
	    response.setContentType("application/octet-stream;charset=UTF-8");  
	    OutputStream outputStream = null;
		try{
			outputStream =	new BufferedOutputStream(response.getOutputStream());
			outputStream.write(data);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if (outputStream != null){
				outputStream.flush();
				outputStream.close();
				System.out.println("outputStream流关闭成功！");
			}
		}
	    response.flushBuffer();
	}
}
