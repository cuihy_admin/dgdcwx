$(function(){

    $('select[name$="Prefix"]').change(function(){
        var $input = $(this).parents('.weui-cell_select:first').find('input[data-name]');
        if($input.val().length > 0){
            $input.blur();
        }
    })

    $('[data-name=spjyxkz]').blur(function(){
        var $input = $(this);
        var value = $input.val();
        if(value.length == 0){
            $.toptip('食品经营许可编号不能为空', 'error');
            $input.focus();
            return false;
        }
        $.showLoading();
        var prefix = $(this).parents('.weui-cell_select:first').find('select option:selected').val();
        value = prefix + value;
        $('input[name=spjyxkzh]').val(value);
        //加载回显数据
        var url = getRootPath() + '/wfw/wsbs/thirdData/getXkzInfo?classifyId=C1020&certNo=' + value;
        globalAsyncPost(url, false, function(response){
            $.hideLoading();
            if(response.success){

                if(response.data){
                    $.toast("验证通过");
                    window.initData(response.data);
                    $('[type=submit]').attr('disabled', false).removeClass('weui-btn_disabled');

                }else{

                    $.alert('没有查询到该食品经营许可编号的数据', '系统提示');
                    $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
                }

            }else{
                $.toast("程序异常请重试", "forbidden");
                $.toptip('对不起，数据加载失败，远程服务器连接超时，请稍后再试或前往食药监分局办事大厅办理，感谢您的支持与理解！', 'error');
                $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
            }
        })
    })

    $('[data-name=ypjyxkz]').blur(function(){
        var $input = $(this);
        var value = $input.val();
        if(value.length == 0){
            $.toptip('药品经营许可编号不能为空', 'error');
            $input.focus();
            return false;
        }
        $.showLoading();
        var prefix = $(this).parents('.weui-cell_select:first').find('select option:selected').val();
        value = prefix + value;
        $('input[name=xkzbh]').val(value);
        //加载回显数据
        var url = getRootPath() + '/wfw/wsbs/thirdData/getXkzInfo?classifyId=C1080&certNo=' + value;
        globalAsyncPost(url, false, function(response){
            $.hideLoading();
            if(response.success){

                if(response.data){
                    $.toast("验证通过");
                    window.initData(response.data);
                    $('[type=submit]').attr('disabled', false).removeClass('weui-btn_disabled');

                }else{

                    $.alert('没有查询到该药品经营许可编号的数据', '系统提示');
                    $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
                }

            }else{
                $.toast("程序异常请重试", "forbidden");
                $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
            }
        })
    })

    $('[data-name=gsp]').blur(function(){
        var $input = $(this);
        var value = $input.val();
        if(value.length == 0){
            $.toptip('药品经营质量管理规范认证证书编号不能为空', 'error');
            $input.focus();
            return false;
        }
        $.showLoading();
        var prefix = $(this).parents('.weui-cell_select:first').find('select option:selected').val();
        value = prefix + value;
        $('input[name=gfrzsbh]').val(value);
        //加载回显数据
        var url = getRootPath() + '/wfw/wsbs/thirdData/getXkzInfo?classifyId=GSP&certNo=' + value;
        globalAsyncPost(url, false, function(response){
            $.hideLoading();
            if(response.success){

                if(response.data){
                    $.toast("验证通过");
                    window.initData(response.data);
                    $('[type=submit]').attr('disabled', false).removeClass('weui-btn_disabled');

                }else{

                    $.alert('没有查询到该药品经营许可编号的数据', '系统提示');
                    $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
                }

            }else{
                $.toast("程序异常请重试", "forbidden");
                $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
            }
        })
    })

    $('[data-name=ylqxjyxkz]').blur(function(){
        var $input = $(this);
        var value = $input.val();
        if(value.length == 0){
            $.toptip('医疗器械经营许可证编号不能为空', 'error');
            $input.focus();
            return false;
        }else if(value.indexOf('YP') != 0){
            $.toptip('医疗器械经营许可证编号必须以大写字母“YP”开头', 'error');
            $input.focus();
            return false;
        }
        $.showLoading();
        //加载回显数据
        var url = getRootPath() + '/wfw/wsbs/thirdData/getXkzInfo?classifyId=C1151&certNo=' + value;
        globalAsyncPost(url, false, function(response){
            $.hideLoading();
            if(response.success){

                if(response.data){
                    $.toast("验证通过");
                    window.initData(response.data);
                    $('[type=submit]').attr('disabled', false).removeClass('weui-btn_disabled');

                }else{

                    $.alert('没有查询到该医疗器械经营许可证的数据', '系统提示');
                    $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
                }

            }else{
                $.toast("程序异常请重试", "forbidden");
                $('[type=submit]').attr('disabled', true).addClass('weui-btn_disabled');
            }
        })
    })
})



window.initData = function(data){

    for(var i in data){
        var properties = data[i];
        if(properties && properties.length > 0){
            data[i] = properties.replace(/\s+/g,"");
        }
    }

    $('input[name=tyshxydm]').val(data.unifiedCreditCode);
    $('input[name=fddbr]').val(data.legalName);
    $('input[name=jyzmc]').val(data.supervisePerson);
    $('input[name=ztytjtlb]').val(data.mainBusiness);
    $('input[name=jyxmjtlb]').val(data.busiitem);
    $('[name=zs]').text(data.domicile);

}