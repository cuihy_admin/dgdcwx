$(function () {

    var mailInfo = false;

    $('input[name=sfjbyj]').change(function () {
        var checked = $(this).val() == 1 ? true : false;
        if (checked) {
            $('#yzxx').show();
            $('#material').hide();

            if(!mailInfo){
                var url = getRootPath() + '/wfw/spyxmp/mailInfo/getData?_=' + Math.random();
                globalAsyncPost(url, false, function (response) {
                    if(response.code == 1){
                        var data = {};
                        if(response.data){
                            data = response.data;
                        }else{
                            data.NAME = $('input[name=lxrmc]').val();
                            data.MOBILE = $('input[name=lxrdh]').val();
                        }
                        $('input[name=yjr]').val(data.NAME);
                        $('input[name=yjrdh]').val(data.MOBILE);
                        $('[name=yjrdz]').val(data.ADDRESS).text(data.ADDRESS);
                    }else{
                        $.toptip('初始化邮寄地址失败', 'error');
                    }
                })
            }

        } else {
            $('#yzxx').hide();
            $('#material').show();
        }
    });
})