var sqbData;
function showPopup() {
    $("#about").popup();
}
var cellHtml = '<div class="weui-cell " for="CELL_FOR" style="display: block;">' +
    ' <!--<div class="weui-cell__hd"><label class="weui-label" >CELL_TITLE</label></div>-->' +
    '<div class="weui-cell_bd">' +
    '<input class="weui-input" type="text" required id="CELL_ID" name="CELL_NAME" placeholder="CELL_PLACEHOLDER" readonly="">' +
    '</div>' +
    ' </div>';
function initData(data, dataId) {
    var obj = $("#" + dataId);
    var item = data[obj.attr("id")];
    $(obj).select({
        title: item.title,
        items: item.items,
        multi: item.multi,
        radioTemplate:
            '<div class="weui-cells weui-cells_radio">\
        {{#items}}\
        <label class="weui-cell weui-check_label" for="weui-select-id-{{this.title}}">\
          <div class="weui-cell__bd weui-cell_primary">\
            <p>{{this.title}}<span class="toptip">{{this.toptip}}</span></p>\
          </div>\
          <div class="weui-cell_ft">\
            <input type="radio" class="weui-check" name="weui-select" required id="weui-select-id-{{this.title}}" value="{{this.value}}" {{#if this.checked}}checked="checked"{{/if}} data-title="{{this.title}}">\
            <span class="weui-icon-checked"></span>\
          </div>\
        </label>\
        {{/items}}\
      </div>',
        checkboxTemplate:
            '<div class="weui-cells weui-cells_checkbox">\
        {{#items}}\
        <label class="weui-cell weui-check_label" for="weui-select-id-{{this.title}}">\
          <div class="weui-cell__bd weui-cell_primary">\
            <p>{{this.title}}<span class="toptip">{{this.toptip}}</span></p>\
          </div>\
          <div class="weui-cell__ft">\
            <input type="checkbox" class="weui-check" required name="weui-select" id="weui-select-id-{{this.title}}" value="{{this.value}}" {{#if this.checked}}checked="checked"{{/if}} data-title="{{this.title}}" >\
            <span class="weui-icon-checked"></span>\
          </div>\
        </label>\
        {{/items}}\
      </div>',
        onChange: function () {
            var vals = obj.attr("data-values") == null ? null : obj.attr("data-values").split(",");
            if (vals != null && vals.length > 0) {
                var cellHtml2, ids, cellHtmlList = "";
                for (var j = 0; j < vals.length; j++) {
                    for (var i = 0; i < item.items.length; i++) {
                        if (item.items[i].name == vals[j]) {
                            cellHtml2 = cellHtml;
                            var CELL_PLACEHOLDER;
                            if(item.items[i].children==null||item.items[i].children.placeholder==null){
                                continue;
                            }else{
                                CELL_PLACEHOLDER=item.items[i].children.placeholder;
                            }
                            cellHtml2 = cellHtml2.replace("CELL_FOR", dataId).replace("CELL_ID", item.items[i].name).replace("CELL_NAME", item.items[i].name).replace("CELL_PLACEHOLDER",CELL_PLACEHOLDER);
                            cellHtmlList = cellHtmlList + cellHtml2;
                        }else{

                        }
                    }
                }
                var forDiv = $("div[for='" + dataId + "']");

                if (forDiv != null) {
                    forDiv.each(function () {

                    }).remove();
                    obj.parent().parent().after(cellHtmlList);
                } else {
                    obj.parent().parent().after(cellHtmlList);
                }
                initChildrenData(obj.attr("id"), item)
                // return cellHtmlList;
            }
        }
    });
}

function initChildrenData(id, data) {
    var item;
    $("div[for='" + id + "'] input").each(function () {
        var thin = $(this);
        console.log(data);
        for (i = 0; i < data.items.length; i++) {
            if (data.items[i].name == thin.attr("id")) {
                item = data.items[i].children;
            }
        }

        if(item!=null){
        if (item.type != null && item.type == "text") {
            thin.removeAttr("readonly");
        } else {
            thin.select("update", {
                title: item.title,
                items: item.items,
                multi: item.multi, radioTemplate:
                    '<div class="weui-cells weui-cells_radio">\
                {{#items}}\
                <label class="weui-cell weui-check_label" for="weui-select-id-{{this.title}}">\
                  <div class="weui-cell__bd weui-cell_primary">\
                    <p>{{this.title}}<span class="toptip">{{this.toptip}}</span></p>\
                  </div>\
                  <div class="weui-cell__ft">\
                    <input type="radio" class="weui-check" name="weui-select" id="weui-select-id-{{this.title}}" value="{{this.value}}" {{#if this.checked}}checked="checked"{{/if}} data-title="{{this.title}}">\
                    <span class="weui-icon-checked"></span>\
                  </div>\
                </label>\
                {{/items}}\
              </div>',
                checkboxTemplate:
                    '<div class="weui-cells weui-cells_checkbox">\
                {{#items}}\
                <label class="weui-cell weui-check_label" for="weui-select-id-{{this.title}}">\
                  <div class="weui-cell__bd weui-cell_primary">\
                    <p>{{this.title}}<span class="toptip">{{this.toptip}}</span></p>\
                  </div>\
                  <div class="weui-cell__ft">\
                    <input type="checkbox" class="weui-check" name="weui-select" id="weui-select-id-{{this.title}}" value="{{this.value}}" {{#if this.checked}}checked="checked"{{/if}} data-title="{{this.title}}" >\
                    <span class="weui-icon-checked"></span>\
                  </div>\
                </label>\
                {{/items}}\
              </div>',
                onChange: function () {
                    var vals = thin.attr("data-values") == null ? null : thin.attr("data-values").split(",");
                    if (vals != null && vals.length > 0) {
                        for (var j = 0; j < vals.length; j++) {
                            for (var i = 0; i < item.items.length; i++) {
                                if (item.items[i].name == vals[j] && item.items[i].children != null) {
                                    cellHtml.replace("CELL_FOR", "22");
                                    obj.after(cellHtml);
                                    // var childrenData = $("[for='" + dataId + "']");
                                    // childrenData.find("label").html(item.items[i].title);
                                    // childrenData.find("input").attr("id", item.items[i].name).attr("name", item.items[i].name).attr("placeholder", "");
                                    // childrenData.show();
                                    initchildrenData(item.items[i].children, $(obj).attr("data-values"))
                                }
                            }
                        }
                    }
                }
            });
        }
        }
    })

    /* */
}

$("#switchWlyx").change(function () {
    var thin = $(this);
    var id = thin.attr("id");

    if (thin.val() == "0") {
        thin.val(1);
        $("div[for='" + id + "']").show("slow");
    } else {
        thin.val(0);
        $("div[for='" + id + "']").hide("slow");
    }

});

$("#switchJystmd").change(function () {
    var thin = $(this);
    var id = thin.attr("id");
    if (thin.val() == "0") {
        thin.val(1);
    } else {
        thin.val(0);
    }

});
function initHide() {
    var thin = $("#switchWlyx");

    if(thin.val()=='0'){
        $("div[for='switchWlyx']").hide("slow");
    }else{
        $("div[for='switchWlyx']").show("slow");
    }

}
$("#city-picker").cityPicker({
    title: "请选择户籍地"
});