/**
 * WEUI 校验框架
 * Created by cuihy on 2018/5/11.
 */
;(function($){
    $.fn.weui.validator = function(options){

        var defaults = {
            //各种参数，各种属性
            'location'         : 'top',
            'background-color' : 'blue'
        }


        var options = $.extend(defaults,options);

        this.each(function(){

            var _this = $(this);
            _this.find('.tab_nav>li').click(function(){
                $(this).addClass('current').siblings().removeClass('current');

                var index = $(this).index();
                _this.find('.tab_content>div').eq(index).show().siblings().hide();

            });

        });

        return this;
    }
    var methods = {
        init : function( options ) {
            // 这
        },
        show : function( ) {
            // 很
        },
        hide : function( ) {
            // 好
        },
        update : function( content ) {
            // !!!
        }
    };
})(jQuery);