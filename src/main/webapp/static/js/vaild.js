if($.validator){
    $.validator.setDefaults({
        submitHandler: function (form) {
            var fileDoms = $(form).find('[data-role="upload"]:visible');
            var flag = false;
            fileDoms.each(function(index,dom){
                if(!$(dom).hasClass("not-required")){
                    if($(dom).find(".weui-uploader__file").length == 0){
                        if(!flag){
                            var scrollTop = $(dom).offset().top;
                            $('html,body').animate({scrollTop: scrollTop}, function(){
                                if($(dom).find('.errorTips:visible').length == 0){
                                    $(dom).find('input[type=file]').click();
                                    var $html = $('<div></div>').addClass('weui-cells__tips red errorTips').append(
                                        '<i class="weui-icon-warn" style="font-size: 0.7rem;"></i>'
                                    ).append('请上传')
                                        .append(
                                            '相关材料证件照片'//$(dom).find('.weui-cells__title').html()
                                        );
                                    $(dom).append($html);
                                }
                            });
                        }else{
                            var $html = $('<div></div>').addClass('weui-cells__tips red errorTips').append(
                                '<i class="weui-icon-warn" style="font-size: 0.7rem;"></i>'
                            ).append('请上传')
                                .append(
                                    '相关材料证件照片'//$(dom).find('.weui-cells__title').html()
                                );
                            $(dom).append($html);
                        }
                        flag = true;
                        return true;
                    }
                }
            });
            if(flag){
                return false;
            }

            if ($('[data-role="childs"]:visible').length > 0) {
                var childsflag = false;
                $('[data-role="childs"]:visible').each(function(index, dom){
                    var $dom = $(this)
                        ,title = $dom.siblings('.weui-panel__hd').text()
                        ,$childs = $dom.find('.child')
                        ;
                    if(!title){
                        title = $dom.parents('.weui-panel__bd').prev('.weui-panel__hd').text();
                    }
                    if($childs.length == 0){
                        $dom.addClass("has-error");
                        $.alert('请填写' + title)
                        childsflag = true;
                    }
                });
                if(childsflag){
                    return false;
                }
            }
            subForm($(form));
        }
    });
    $('[data-role="form"]').each(function(index, dom){
        $(dom).validate({
            errorElement: "div",
            errorClass: "weui-cells__tips red errorTips",
            showErrors:function(errorMap,errorList) {
                this.defaultShowErrors();
            },
            errorPlacement: function(error, element) {
                var $panel = element.parents('.weui-cells:first');
                if($panel.find('.weui-cell').length > 1){
                    element.parents('.weui-cell:first').after($(error));
                }else{
                    $panel.after($(error));
                }
            }
        });
    });
}