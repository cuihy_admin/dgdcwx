/**
 * Created by Administrator on 2018/5/14.
 */
/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "./jquery.validate"], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {
    (function () {
        $.validator.addMethod("cocialUnifiedCreditCode", function (value, element) {
            console.log("sss");
            var regex = /^(?:(?![IOZSV])[\dA-Z]){2}\d{6}(?:(?![IOZSV])[\dA-Z]){10}$/;

            return this.optional(element) ||  (regex.test(value));
        },"请正确填写您的邮政编码");


    })
}));
