<?xml version="1.0" encoding="utf-8"?>
<?mso-application progid="Word.Document"?>

<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex" xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex" xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex" xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex" xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex" xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex" xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex" xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex" xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink" xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:body>
                    <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00B97545">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:eastAsia="方正小标宋简体"/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:eastAsia="方正小标宋简体"/>
                                <w:sz w:val="44"/>
                                <w:szCs w:val="44"/>
                            </w:rPr>
                            <w:t>医疗器械经营许可证补发申请表</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="8986" w:type="dxa"/>
                            <w:jc w:val="center"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="8" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1333"/>
                            <w:gridCol w:w="1293"/>
                            <w:gridCol w:w="1627"/>
                            <w:gridCol w:w="1134"/>
                            <w:gridCol w:w="709"/>
                            <w:gridCol w:w="425"/>
                            <w:gridCol w:w="1134"/>
                            <w:gridCol w:w="1331"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="005B55EC">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>企业名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.QYMC}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>许可证编号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2920" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.XKZBH}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>发证日期</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00CB6A7C" w:rsidRPr="009C6D20" w:rsidRDefault="00CB6A7C" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>（许可证）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2890" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="00C3382D">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.FZRQ}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>组织机构</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>代</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2920" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.TYSHXYDM}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>有效期限</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00CB6A7C" w:rsidRPr="009C6D20" w:rsidRDefault="00CB6A7C" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>（许可证）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2890" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.YXQX}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>法定代表人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2920" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="008353B2" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008353B2">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.FDDBR}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>企业负责人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2890" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="008353B2" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008353B2">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.QYFZR}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>经营方式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                       <#if data.JYFS=="1">
    <w:sym w:font="Wingdings 2" w:char="F052" />
                                       <#else>
    <w:t>□</w:t>
                                       </#if>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>批发</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                          <#if data.JYFS=="2">
    <w:sym w:font="Wingdings 2" w:char="F052" />
                                          <#else>
    <w:t>□</w:t>
                                          </#if>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>零售</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                       <#if data.JYFS=="3">
    <w:sym w:font="Wingdings 2" w:char="F052" />
                                       <#else>
    <w:t>□</w:t>
                                       </#if>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>批零兼营</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>经营模式</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
<#if data.JYMS=="1">
    <w:sym w:font="Wingdings 2" w:char="F052" />
<#else>
    <w:t>□</w:t></#if>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>销售医疗器械</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                       <#if data.JYMS=="2">
    <w:sym w:font="Wingdings 2" w:char="F052" />
                                       <#else>
    <w:t>□</w:t>
                                       </#if>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>为其他生产经营企业提供贮存、配送服务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>住</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">    </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>所</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00D2367A" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D2367A">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.ZS}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>经营场所</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00D2367A" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00D2367A">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.JYCS}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>库房地址</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="008353B2">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.KFDZ}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="1985"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>经营范围</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.JYFW}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>联系人</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1293" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2761" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>联系电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>传真</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1331" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>电子邮件</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="567"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:bookmarkStart w:id="0" w:name="_GoBack" w:colFirst="1" w:colLast="5"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1293" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="001A57C3">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.LXRXM}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2761" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="001A57C3">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.LXRZJHM}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="001A57C3">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.LXRDH}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="001A57C3">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.LXRCZ}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1331" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00EF7B3E" w:rsidP="001A57C3">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.LXRYX}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:bookmarkEnd w:id="0"/>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00C05FDC">
                            <w:trPr>
                                <w:trHeight w:val="1229"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1333" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>补发</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7653" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>遗失、损毁原因及何年何月</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramStart"/>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>何日在何媒体</w:t>
                                    </w:r>
                                    <w:proofErr w:type="gramEnd"/>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>刊登遗失声明</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>:</w:t>
                                    </w:r>
                                    <w:r w:rsidR="00EF7B3E">
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00EF7B3E" w:rsidRPr="00EF7B3E">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>${data.REASON}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidTr="00505DB4">
                            <w:trPr>
                                <w:trHeight w:hRule="exact" w:val="2379"/>
                                <w:jc w:val="center"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8986" w:type="dxa"/>
                                    <w:gridSpan w:val="8"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="196" w:firstLine="413"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>本企业承诺所提交的全部资料真实有效，并承担一切法律责任。同时，保证按照法律法规的要求从事医疗器械经营活动。</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="196" w:firstLine="413"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w:rsidR="00B97545" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="686" w:firstLine="1446"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>法定代表人（签字）</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">           </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">           </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>（企业盖章）</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w:rsidR="00505DB4" w:rsidRDefault="00505DB4" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="686" w:firstLine="1446"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w:rsidR="00505DB4" w:rsidRDefault="00505DB4" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="686" w:firstLine="1446"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w:rsidR="00505DB4" w:rsidRPr="009C6D20" w:rsidRDefault="00505DB4" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="686" w:firstLine="1446"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w:rsidR="00B97545" w:rsidRPr="00FE1EE9" w:rsidRDefault="00B97545" w:rsidP="00C05FDC">
                                    <w:pPr>
                                        <w:spacing w:line="240" w:lineRule="exact"/>
                                        <w:ind w:leftChars="686" w:left="6079" w:hangingChars="2200" w:hanging="4638"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:eastAsia="仿宋_GB2312"/>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00505DB4">
                                        <w:rPr>
                                            <w:noProof/>
                                        </w:rPr>
                                        <w:drawing>
                                            <wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="5DAA164F" wp14:editId="6126CB16">
                                                <wp:extent cx="1170940" cy="391160"/>
                                                <wp:effectExtent l="0" t="0" r="0" b="0"/>
                                                <wp:docPr id="6" name="图片 6" descr="C:\Users\Administrator\AppData\Roaming\Tencent\Users\474399685\QQ\WinTemp\RichOle\65596WFNB%[BIE8S{1P(1D1.png"/>
                                                <wp:cNvGraphicFramePr/>
                                                <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                                    <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                        <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                            <pic:nvPicPr>
                                                                <pic:cNvPr id="6" name="图片 6" descr="C:\Users\Administrator\AppData\Roaming\Tencent\Users\474399685\QQ\WinTemp\RichOle\65596WFNB%[BIE8S{1P(1D1.png"/>
                                                                <pic:cNvPicPr/>
                                                            </pic:nvPicPr>
                                                            <pic:blipFill>
                                                                <a:blip r:embed="rId4" cstate="print">
                                                                    <a:extLst>
                                                                        <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                            <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"></a14:useLocalDpi>
                                                                        </a:ext>
                                                                    </a:extLst>
                                                                </a:blip>
                                                                <a:srcRect/>
                                                                <a:stretch>
                                                                    <a:fillRect/>
                                                                </a:stretch>
                                                            </pic:blipFill>
                                                            <pic:spPr bwMode="auto">
                                                                <a:xfrm>
                                                                    <a:off x="0" y="0"/>
                                                                    <a:ext cx="1170940" cy="391160"/>
                                                                </a:xfrm>
                                                                <a:prstGeom prst="rect">
                                                                    <a:avLst/>
                                                                </a:prstGeom>
                                                                <a:noFill/>
                                                                <a:ln>
                                                                    <a:noFill/>
                                                                </a:ln>
                                                            </pic:spPr>
                                                        </pic:pic>
                                                    </a:graphicData>
                                                </a:graphic>
                                            </wp:inline>
                                        </w:drawing>
                                    </w:r>
                                    <w:r w:rsidRPr="009C6D20">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                      </w:t>
                                    </w:r>
                                    <w:r w:rsidR="00505DB4">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${data.DATE}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w:rsidR="00B97545" w:rsidRPr="009C6D20" w:rsidRDefault="00B97545" w:rsidP="00B97545">
                        <w:pPr>
                            <w:ind w:left="1235" w:hangingChars="588" w:hanging="1235"/>
                            <w:rPr>
                                <w:bCs/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="009C6D20">
                            <w:rPr>
                                <w:bCs/>
                            </w:rPr>
                            <w:t>填表说明：本表按照实际内容填写，不涉及的可缺项。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="006E5570" w:rsidRPr="00B97545" w:rsidRDefault="006E5570" w:rsidP="00B97545"/>
                    <w:sectPr w:rsidR="006E5570" w:rsidRPr="00B97545" w:rsidSect="006E5570">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings" Target="webSettings.xml"/>
                <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/>
                <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
                <Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>
                <Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/>
                <Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image1.png"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image1.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${data.signature}</pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="1F497D"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="EEECE1"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4F81BD"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="C0504D"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="9BBB59"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="8064A2"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="4BACC6"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="F79646"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0000FF"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="800080"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="Cambria"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ ゴシック"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="Calibri"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="ＭＳ 明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="宋体"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="50000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="35000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="37000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="15000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="1"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="51000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="80000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="93000"/>
                                            <a:satMod val="130000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="94000"/>
                                            <a:satMod val="135000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="16200000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="9525" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr">
                                        <a:shade val="95000"/>
                                        <a:satMod val="105000"/>
                                    </a:schemeClr>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="25400" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                            <a:ln w="38100" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="20000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="38000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="35000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                                <a:scene3d>
                                    <a:camera prst="orthographicFront">
                                        <a:rot lat="0" lon="0" rev="0"/>
                                    </a:camera>
                                    <a:lightRig rig="threePt" dir="t">
                                        <a:rot lat="0" lon="0" rev="1200000"/>
                                    </a:lightRig>
                                </a:scene3d>
                                <a:sp3d>
                                    <a:bevelT w="63500" h="25400"/>
                                </a:sp3d>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="40000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="40000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="45000"/>
                                            <a:shade val="99000"/>
                                            <a:satMod val="350000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="20000"/>
                                            <a:satMod val="255000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="-80000" r="50000" b="180000"/>
                                </a:path>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="80000"/>
                                            <a:satMod val="300000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="30000"/>
                                            <a:satMod val="200000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:path path="circle">
                                    <a:fillToRect l="50000" t="50000" r="50000" b="50000"/>
                                </a:path>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main" mc:Ignorable="w14 w15 w16se w16cid">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word" w:val="12"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation" w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00A80F4E"/>
                    <w:rsid w:val="001A57C3"/>
                    <w:rsid w:val="004B1106"/>
                    <w:rsid w:val="00505DB4"/>
                    <w:rsid w:val="00524F16"/>
                    <w:rsid w:val="006E5570"/>
                    <w:rsid w:val="00754CD4"/>
                    <w:rsid w:val="008353B2"/>
                    <w:rsid w:val="00897506"/>
                    <w:rsid w:val="008B3AFC"/>
                    <w:rsid w:val="00A80F4E"/>
                    <w:rsid w:val="00B76703"/>
                    <w:rsid w:val="00B97545"/>
                    <w:rsid w:val="00C3382D"/>
                    <w:rsid w:val="00CB6A7C"/>
                    <w:rsid w:val="00D2367A"/>
                    <w:rsid w:val="00EF7B3E"/>
                    <w:rsid w:val="00F32C17"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1" w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5" w:accent6="accent6" w:hyperlink="hyperlink" w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="1026"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="3225F650"/>
                <w15:docId w15:val="{417F60E9-8D48-4E10-9955-F01461D07D0D}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se w16cid">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia" w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0" w:defQFormat="0" w:count="375">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:semiHidden="1" w:uiPriority="59" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1" w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:rsid w:val="00A80F4E"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
                        <w:szCs w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se w16cid">
                <w:optimizeForBrowser/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" mc:Ignorable="w14 w15 w16se w16cid">
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002AFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="方正小标宋简体">
                    <w:altName w:val="微软雅黑"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000000" w:usb1="080E0000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋_GB2312">
                    <w:altName w:val="微软雅黑"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="00000001" w:usb1="080E0000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000" w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Cambria">
                    <w:panose1 w:val="02040503050406030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E00006FF" w:usb1="420024FF" w:usb2="02000000" w:usb3="00000000" w:csb0="0000019F" w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>LZB</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>崔 汉阳</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2018-12-27T19:50:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2018-12-27T19:50:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>1</Pages>
                <Words>73</Words>
                <Characters>421</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>3</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company>DGFDA</Company>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>493</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>
